<?php
// created: 2017-06-18 00:56:18
$dictionary["CON_CONSULTORIOS"]["fields"]["accounts_con_consultorios_1"] = array (
  'name' => 'accounts_con_consultorios_1',
  'type' => 'link',
  'relationship' => 'accounts_con_consultorios_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_CON_CONSULTORIOS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_con_consultorios_1accounts_ida',
);
$dictionary["CON_CONSULTORIOS"]["fields"]["accounts_con_consultorios_1_name"] = array (
  'name' => 'accounts_con_consultorios_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_CON_CONSULTORIOS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_con_consultorios_1accounts_ida',
  'link' => 'accounts_con_consultorios_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["CON_CONSULTORIOS"]["fields"]["accounts_con_consultorios_1accounts_ida"] = array (
  'name' => 'accounts_con_consultorios_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_con_consultorios_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_CON_CONSULTORIOS_1_FROM_CON_CONSULTORIOS_TITLE',
);
