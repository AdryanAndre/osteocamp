<?php
// created: 2017-06-18 00:12:09
$dictionary["Account"]["fields"]["accounts_com_competidores_1"] = array (
  'name' => 'accounts_com_competidores_1',
  'type' => 'link',
  'relationship' => 'accounts_com_competidores_1',
  'source' => 'non-db',
  'module' => 'COM_COMPETIDORES',
  'bean_name' => 'COM_COMPETIDORES',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_COM_COMPETIDORES_1_FROM_COM_COMPETIDORES_TITLE',
);
