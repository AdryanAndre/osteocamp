<?php
// created: 2017-06-18 00:12:09
$dictionary["COM_COMPETIDORES"]["fields"]["accounts_com_competidores_1"] = array (
  'name' => 'accounts_com_competidores_1',
  'type' => 'link',
  'relationship' => 'accounts_com_competidores_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_COM_COMPETIDORES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_com_competidores_1accounts_ida',
);
$dictionary["COM_COMPETIDORES"]["fields"]["accounts_com_competidores_1_name"] = array (
  'name' => 'accounts_com_competidores_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_COM_COMPETIDORES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_com_competidores_1accounts_ida',
  'link' => 'accounts_com_competidores_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["COM_COMPETIDORES"]["fields"]["accounts_com_competidores_1accounts_ida"] = array (
  'name' => 'accounts_com_competidores_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_com_competidores_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_COM_COMPETIDORES_1_FROM_COM_COMPETIDORES_TITLE',
);
