<?php
 // created: 2017-06-18 00:16:17
$dictionary['COM_COMPETIDORES']['fields']['name']['required']=false;
$dictionary['COM_COMPETIDORES']['fields']['name']['inline_edit']=true;
$dictionary['COM_COMPETIDORES']['fields']['name']['help']='Nome da Empresa competidora';
$dictionary['COM_COMPETIDORES']['fields']['name']['duplicate_merge']='disabled';
$dictionary['COM_COMPETIDORES']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['COM_COMPETIDORES']['fields']['name']['merge_filter']='disabled';
$dictionary['COM_COMPETIDORES']['fields']['name']['unified_search']=false;

 ?>