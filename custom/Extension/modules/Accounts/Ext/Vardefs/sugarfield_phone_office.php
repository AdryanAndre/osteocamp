<?php
 // created: 2017-06-17 22:31:21
$dictionary['Account']['fields']['phone_office']['len']='16';
$dictionary['Account']['fields']['phone_office']['required']=true;
$dictionary['Account']['fields']['phone_office']['inline_edit']=true;
$dictionary['Account']['fields']['phone_office']['help']='Telefone do Médico';
$dictionary['Account']['fields']['phone_office']['merge_filter']='disabled';

 ?>