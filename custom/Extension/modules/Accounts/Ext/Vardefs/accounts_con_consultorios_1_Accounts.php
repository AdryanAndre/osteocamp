<?php
// created: 2017-06-18 00:56:18
$dictionary["Account"]["fields"]["accounts_con_consultorios_1"] = array (
  'name' => 'accounts_con_consultorios_1',
  'type' => 'link',
  'relationship' => 'accounts_con_consultorios_1',
  'source' => 'non-db',
  'module' => 'CON_CONSULTORIOS',
  'bean_name' => 'CON_CONSULTORIOS',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_CON_CONSULTORIOS_1_FROM_CON_CONSULTORIOS_TITLE',
);
