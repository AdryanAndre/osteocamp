<?php
 // created: 2017-06-18 00:12:09
$layout_defs["Accounts"]["subpanel_setup"]['accounts_com_competidores_1'] = array (
  'order' => 100,
  'module' => 'COM_COMPETIDORES',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_COM_COMPETIDORES_1_FROM_COM_COMPETIDORES_TITLE',
  'get_subpanel_data' => 'accounts_com_competidores_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
