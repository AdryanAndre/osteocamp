<?php
 // created: 2017-06-18 00:56:18
$layout_defs["Accounts"]["subpanel_setup"]['accounts_con_consultorios_1'] = array (
  'order' => 100,
  'module' => 'CON_CONSULTORIOS',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_CON_CONSULTORIOS_1_FROM_CON_CONSULTORIOS_TITLE',
  'get_subpanel_data' => 'accounts_con_consultorios_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
