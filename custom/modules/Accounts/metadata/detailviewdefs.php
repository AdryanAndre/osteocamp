<?php
$viewdefs ['Accounts'] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
          'AOS_GENLET' => 
          array (
            'customCode' => '<input type="button" class="button" onClick="showPopup();" value="{$APP.LBL_GENERATE_LETTER}">',
          ),
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'modules/Accounts/Account.js',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_ACCOUNT_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_account_information' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'form_trat_c',
            'studio' => 'visible',
            'label' => 'LBL_FORM_TRAT',
          ),
          1 => 
          array (
            'name' => 'name',
            'comment' => 'Name of the Company',
            'label' => 'LBL_NAME',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'classe_c',
            'studio' => 'visible',
            'label' => 'LBL_CLASSE',
          ),
          1 => 
          array (
            'name' => 'freq_visita_c',
            'studio' => 'visible',
            'label' => 'LBL_FREQ_VISITA',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'especialidade_c',
            'studio' => 'visible',
            'label' => 'LBL_ESPECIALIDADE',
          ),
          1 => 
          array (
            'name' => 'uf_crm_c',
            'studio' => 'visible',
            'label' => 'LBL_UF_CRM',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'crm_cro_c',
            'studio' => 'visible',
            'label' => 'LBL_CRM_CRO',
          ),
          1 => 
          array (
            'name' => 'num_crmcro_c',
            'label' => 'LBL_NUM_CRMCRO',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'phone_office',
            'comment' => 'The office phone number',
            'label' => 'LBL_PHONE_OFFICE',
          ),
          1 => 
          array (
            'name' => 'aniversario_c',
            'label' => 'LBL_ANIVERSARIO',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'email_c',
            'label' => 'LBL_EMAIL',
          ),
          1 => 
          array (
            'name' => 'status_cadastro_c',
            'studio' => 'visible',
            'label' => 'LBL_STATUS_CADASTRO',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'tipo_cliente_c',
            'studio' => 'visible',
            'label' => 'LBL_TIPO_CLIENTE',
          ),
          1 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'billing_address_street',
            'label' => 'LBL_BILLING_ADDRESS',
            'type' => 'address',
            'displayParams' => 
            array (
              'key' => 'billing',
            ),
          ),
          1 => 
          array (
            'name' => 'shipping_address_street',
            'label' => 'LBL_SHIPPING_ADDRESS',
            'type' => 'address',
            'displayParams' => 
            array (
              'key' => 'shipping',
            ),
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'quant_cirurgiais_c',
            'label' => 'LBL_QUANT_CIRURGIAIS',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'predominancia_c',
            'label' => 'LBL_PREDOMINANCIA',
          ),
          1 => 
          array (
            'name' => 'nome_convenio_c',
            'label' => 'LBL_NOME_CONVENIO',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'perc_convenio_c',
            'label' => 'LBL_PERC_CONVENIO',
          ),
          1 => 
          array (
            'name' => 'hospital_c',
            'studio' => 'visible',
            'label' => 'LBL_HOSPITAL',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'perc_hospital_c',
            'label' => 'LBL_PERC_HOSPITAL',
          ),
          1 => 
          array (
            'name' => 'pref_cirurgica_c',
            'studio' => 'visible',
            'label' => 'LBL_PREF_CIRURGICA',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'particularidade_c',
            'studio' => 'visible',
            'label' => 'LBL_PARTICULARIDADE',
          ),
          1 => 
          array (
            'name' => 'percentual_completude_c',
            'label' => 'LBL_PERCENTUAL_COMPLETUDE',
          ),
        ),
      ),
    ),
  ),
);
?>
