<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-06-20 00:30:10
$layout_defs["Accounts"]["subpanel_setup"]['accounts_am_projecttemplates_1'] = array (
  'order' => 100,
  'module' => 'AM_ProjectTemplates',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_AM_PROJECTTEMPLATES_1_FROM_AM_PROJECTTEMPLATES_TITLE',
  'get_subpanel_data' => 'accounts_am_projecttemplates_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-06-18 00:12:09
$layout_defs["Accounts"]["subpanel_setup"]['accounts_com_competidores_1'] = array (
  'order' => 100,
  'module' => 'COM_COMPETIDORES',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_COM_COMPETIDORES_1_FROM_COM_COMPETIDORES_TITLE',
  'get_subpanel_data' => 'accounts_com_competidores_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-06-18 00:56:18
$layout_defs["Accounts"]["subpanel_setup"]['accounts_con_consultorios_1'] = array (
  'order' => 100,
  'module' => 'CON_CONSULTORIOS',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_CON_CONSULTORIOS_1_FROM_CON_CONSULTORIOS_TITLE',
  'get_subpanel_data' => 'accounts_con_consultorios_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>