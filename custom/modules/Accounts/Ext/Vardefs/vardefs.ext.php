<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2017-06-20 00:30:10
$dictionary["Account"]["fields"]["accounts_am_projecttemplates_1"] = array (
  'name' => 'accounts_am_projecttemplates_1',
  'type' => 'link',
  'relationship' => 'accounts_am_projecttemplates_1',
  'source' => 'non-db',
  'module' => 'AM_ProjectTemplates',
  'bean_name' => 'AM_ProjectTemplates',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AM_PROJECTTEMPLATES_1_FROM_AM_PROJECTTEMPLATES_TITLE',
);


// created: 2017-06-18 00:12:09
$dictionary["Account"]["fields"]["accounts_com_competidores_1"] = array (
  'name' => 'accounts_com_competidores_1',
  'type' => 'link',
  'relationship' => 'accounts_com_competidores_1',
  'source' => 'non-db',
  'module' => 'COM_COMPETIDORES',
  'bean_name' => 'COM_COMPETIDORES',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_COM_COMPETIDORES_1_FROM_COM_COMPETIDORES_TITLE',
);


// created: 2017-06-18 00:56:18
$dictionary["Account"]["fields"]["accounts_con_consultorios_1"] = array (
  'name' => 'accounts_con_consultorios_1',
  'type' => 'link',
  'relationship' => 'accounts_con_consultorios_1',
  'source' => 'non-db',
  'module' => 'CON_CONSULTORIOS',
  'bean_name' => 'CON_CONSULTORIOS',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_CON_CONSULTORIOS_1_FROM_CON_CONSULTORIOS_TITLE',
);


 // created: 2017-06-17 22:33:21
$dictionary['Account']['fields']['aniversario_c']['inline_edit']='1';
$dictionary['Account']['fields']['aniversario_c']['labelValue']='Dia/Mês Aniversário:';

 

 // created: 2017-06-17 22:13:00
$dictionary['Account']['fields']['classe_c']['inline_edit']='1';
$dictionary['Account']['fields']['classe_c']['labelValue']='Classe:';

 

 // created: 2017-06-17 22:27:27
$dictionary['Account']['fields']['crm_cro_c']['inline_edit']='1';
$dictionary['Account']['fields']['crm_cro_c']['labelValue']='Tipo de Conselho:';

 

 // created: 2017-06-17 22:35:38
$dictionary['Account']['fields']['email_c']['inline_edit']='1';
$dictionary['Account']['fields']['email_c']['labelValue']='Email:';

 

 // created: 2017-06-17 22:19:55
$dictionary['Account']['fields']['especialidade_c']['inline_edit']='1';
$dictionary['Account']['fields']['especialidade_c']['labelValue']='Especialidade:';

 

 // created: 2017-06-17 22:16:48
$dictionary['Account']['fields']['form_trat_c']['inline_edit']='1';
$dictionary['Account']['fields']['form_trat_c']['labelValue']='Forma de Tratamento:';

 

 // created: 2017-06-17 22:15:52
$dictionary['Account']['fields']['freq_visita_c']['inline_edit']='1';
$dictionary['Account']['fields']['freq_visita_c']['labelValue']='Frequência de Visitas:';

 

 // created: 2017-06-18 03:46:15
$dictionary['Account']['fields']['hospital_c']['inline_edit']='1';
$dictionary['Account']['fields']['hospital_c']['labelValue']='Hospital:';

 

 // created: 2017-06-18 03:46:15
$dictionary['Account']['fields']['hos_hospital_id_c']['inline_edit']=1;

 

 // created: 2017-06-17 21:35:53
$dictionary['Account']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2017-06-17 21:35:52
$dictionary['Account']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2017-06-17 21:35:51
$dictionary['Account']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2017-06-17 21:35:51
$dictionary['Account']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2017-06-17 22:09:35
$dictionary['Account']['fields']['name']['len']='250';
$dictionary['Account']['fields']['name']['inline_edit']=true;
$dictionary['Account']['fields']['name']['comments']='Nome do Médico';
$dictionary['Account']['fields']['name']['merge_filter']='disabled';

 

 // created: 2017-06-17 22:51:58
$dictionary['Account']['fields']['nome_convenio_c']['inline_edit']='1';
$dictionary['Account']['fields']['nome_convenio_c']['labelValue']='Nome do Convênio:';

 

 // created: 2017-06-17 22:28:41
$dictionary['Account']['fields']['num_crmcro_c']['inline_edit']='1';
$dictionary['Account']['fields']['num_crmcro_c']['labelValue']='Número do Conselho:';

 

 // created: 2017-06-17 22:57:55
$dictionary['Account']['fields']['particularidade_c']['inline_edit']='1';
$dictionary['Account']['fields']['particularidade_c']['labelValue']='Particularidades:';

 

 // created: 2017-06-17 23:00:22
$dictionary['Account']['fields']['percentual_completude_c']['inline_edit']='1';
$dictionary['Account']['fields']['percentual_completude_c']['labelValue']='Percentual Completude:';

 

 // created: 2017-06-17 22:53:40
$dictionary['Account']['fields']['perc_convenio_c']['inline_edit']='1';
$dictionary['Account']['fields']['perc_convenio_c']['labelValue']='Percentual Convênio:';

 

 // created: 2017-06-17 22:55:00
$dictionary['Account']['fields']['perc_hospital_c']['inline_edit']='1';
$dictionary['Account']['fields']['perc_hospital_c']['labelValue']='Percentual Hospital:';

 

 // created: 2017-06-17 22:31:21
$dictionary['Account']['fields']['phone_office']['len']='16';
$dictionary['Account']['fields']['phone_office']['required']=true;
$dictionary['Account']['fields']['phone_office']['inline_edit']=true;
$dictionary['Account']['fields']['phone_office']['help']='Telefone do Médico';
$dictionary['Account']['fields']['phone_office']['merge_filter']='disabled';

 

 // created: 2017-06-17 22:49:28
$dictionary['Account']['fields']['predominancia_c']['inline_edit']='1';
$dictionary['Account']['fields']['predominancia_c']['labelValue']='Predominância';

 

 // created: 2017-06-17 23:31:47
$dictionary['Account']['fields']['pref_cirurgica_c']['inline_edit']='1';
$dictionary['Account']['fields']['pref_cirurgica_c']['labelValue']='Preferências Cirurgicas:';

 

 // created: 2017-06-17 22:45:10
$dictionary['Account']['fields']['quant_cirurgiais_c']['inline_edit']='1';
$dictionary['Account']['fields']['quant_cirurgiais_c']['labelValue']='Quantidade Cirurgias:';

 

 // created: 2017-06-17 22:40:48
$dictionary['Account']['fields']['status_cadastro_c']['inline_edit']='1';
$dictionary['Account']['fields']['status_cadastro_c']['labelValue']='Status do Cadastro:';

 

 // created: 2017-06-17 22:43:02
$dictionary['Account']['fields']['tipo_cliente_c']['inline_edit']='1';
$dictionary['Account']['fields']['tipo_cliente_c']['labelValue']='Tipo do Cliente:';

 

 // created: 2017-06-17 22:26:04
$dictionary['Account']['fields']['uf_crm_c']['inline_edit']='1';
$dictionary['Account']['fields']['uf_crm_c']['labelValue']='UF do CRM:';

 
?>