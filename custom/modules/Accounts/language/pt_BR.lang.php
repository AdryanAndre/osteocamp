<?php
// created: 2017-06-20 00:39:56
$mod_strings = array (
  'LBL_FORM_TRAT' => 'Forma de Tratamento:',
  'LBL_NAME' => 'Nome:',
  'LBL_CLASSE' => 'Classe:',
  'LBL_FREQ_VISITA' => 'Frequência de Visitas:',
  'LBL_ESPECIALIDADE' => 'Especialidade:',
  'LBL_UF_CRM' => 'UF do CRM:',
  'LBL_CRM_CRO' => 'Tipo de Conselho:',
  'LBL_NUM_CRMCRO' => 'Número do Conselho:',
  'LBL_PHONE_OFFICE' => 'Telefone:',
  'LBL_ANIVERSARIO' => 'Dia/Mês Aniversário:',
  'LBL_EMAIL' => 'Email:',
  'LBL_STATUS_CADASTRO' => 'Status do Cadastro:',
  'LBL_TIPO_CLIENTE' => 'Tipo do Cliente:',
  'LBL_QUANT_CIRURGIAIS' => 'Quantidade Cirurgias:',
  'LBL_PREDOMINANCIA' => 'Predominância',
  'LBL_NOME_CONVENIO' => 'Nome do Convênio:',
  'LBL_PERC_CONVENIO' => 'Percentual Convênio:',
  'LBL_PERC_HOSPITAL' => 'Percentual Hospital:',
  'LBL_PREF_CIRURGICA' => 'Preferências Cirurgicas:',
  'LBL_PARTICULARIDADE' => 'Particularidades:',
  'LBL_PERCENTUAL_COMPLETUDE' => 'Percentual Completude:',
  'LBL_EDITVIEW_PANEL1' => 'Inteligência Competitiva',
  'LBL_EDITVIEW_PANEL3' => 'Dados Principais',
  'LBL_EDITVIEW_PANEL4' => 'Inteligência Competitiva',
  'LBL_HOSPITAL_HOS_HOSPITAL_ID' => 'Hospital: (ID  relacionado)',
  'LBL_HOSPITAL' => 'Hospital:',
);