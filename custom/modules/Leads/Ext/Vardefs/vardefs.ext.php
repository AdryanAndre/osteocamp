<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-06-17 23:44:24
$dictionary['Lead']['fields']['aniversario_c']['inline_edit']='1';
$dictionary['Lead']['fields']['aniversario_c']['labelValue']='Dia/Mês Aniversário:';

 

 // created: 2017-06-17 23:41:06
$dictionary['Lead']['fields']['crm_cro_c']['inline_edit']='1';
$dictionary['Lead']['fields']['crm_cro_c']['labelValue']='Tipo de Conselho:';

 

 // created: 2017-06-17 23:45:47
$dictionary['Lead']['fields']['email_c']['inline_edit']='1';
$dictionary['Lead']['fields']['email_c']['labelValue']='Email:';

 

 // created: 2017-06-17 23:37:59
$dictionary['Lead']['fields']['especialidade_c']['inline_edit']='1';
$dictionary['Lead']['fields']['especialidade_c']['labelValue']='Especialidade:';

 

 // created: 2017-06-17 23:35:57
$dictionary['Lead']['fields']['form_trat_c']['inline_edit']='1';
$dictionary['Lead']['fields']['form_trat_c']['labelValue']='Forma de Tratamento:';

 

 // created: 2017-06-17 21:36:06
$dictionary['Lead']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2017-06-17 21:36:05
$dictionary['Lead']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2017-06-17 21:36:04
$dictionary['Lead']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2017-06-17 21:36:04
$dictionary['Lead']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2017-06-17 23:42:39
$dictionary['Lead']['fields']['nro_conselho_c']['inline_edit']='1';
$dictionary['Lead']['fields']['nro_conselho_c']['labelValue']='Número do Conselho:';

 

 // created: 2017-06-17 23:43:45
$dictionary['Lead']['fields']['phone_work']['required']=true;
$dictionary['Lead']['fields']['phone_work']['inline_edit']=true;
$dictionary['Lead']['fields']['phone_work']['merge_filter']='disabled';

 

 // created: 2017-06-17 23:39:27
$dictionary['Lead']['fields']['uf_crm_c']['inline_edit']='1';
$dictionary['Lead']['fields']['uf_crm_c']['labelValue']='UF CRM:';

 
?>