<?php
// created: 2017-06-17 23:45:47
$mod_strings = array (
  'LBL_FORM_TRAT' => 'Forma de Tratamento:',
  'LBL_ESPECIALIDADE' => 'Especialidade:',
  'LBL_UF_CRM' => 'UF CRM:',
  'LBL_CRM_CRO' => 'Tipo de Conselho:',
  'LBL_NRO_CONSELHO' => 'Número do Conselho:',
  'LBL_OFFICE_PHONE' => 'Telefone:',
  'LBL_ANIVERSARIO' => 'Dia/Mês Aniversário:',
  'LBL_EMAIL' => 'Email:',
);