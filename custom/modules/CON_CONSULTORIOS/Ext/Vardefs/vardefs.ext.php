<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2017-06-18 00:56:18
$dictionary["CON_CONSULTORIOS"]["fields"]["accounts_con_consultorios_1"] = array (
  'name' => 'accounts_con_consultorios_1',
  'type' => 'link',
  'relationship' => 'accounts_con_consultorios_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_CON_CONSULTORIOS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_con_consultorios_1accounts_ida',
);
$dictionary["CON_CONSULTORIOS"]["fields"]["accounts_con_consultorios_1_name"] = array (
  'name' => 'accounts_con_consultorios_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_CON_CONSULTORIOS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_con_consultorios_1accounts_ida',
  'link' => 'accounts_con_consultorios_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["CON_CONSULTORIOS"]["fields"]["accounts_con_consultorios_1accounts_ida"] = array (
  'name' => 'accounts_con_consultorios_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_con_consultorios_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_CON_CONSULTORIOS_1_FROM_CON_CONSULTORIOS_TITLE',
);


 // created: 2017-06-18 00:51:42
$dictionary['CON_CONSULTORIOS']['fields']['aniv_sec_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['aniv_sec_c']['labelValue']='Aniversário da Secretária:';

 

 // created: 2017-06-18 00:28:49
$dictionary['CON_CONSULTORIOS']['fields']['cep_cons_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['cep_cons_c']['labelValue']='CEP:';

 

 // created: 2017-06-18 00:26:50
$dictionary['CON_CONSULTORIOS']['fields']['cid_cons_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['cid_cons_c']['labelValue']='Cidade:';

 

 // created: 2017-06-18 00:42:04
$dictionary['CON_CONSULTORIOS']['fields']['dia_qua_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['dia_qua_c']['labelValue']='Atendimento 4º-Feira:';

 

 // created: 2017-06-18 00:43:25
$dictionary['CON_CONSULTORIOS']['fields']['dia_qui_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['dia_qui_c']['labelValue']='Atendimento 5º-Feira:';

 

 // created: 2017-06-18 00:34:04
$dictionary['CON_CONSULTORIOS']['fields']['dia_seg_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['dia_seg_c']['labelValue']='Atendimento 2º-Feira:';

 

 // created: 2017-06-18 00:44:10
$dictionary['CON_CONSULTORIOS']['fields']['dia_sex_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['dia_sex_c']['labelValue']='Atendimento 6º-Feira:';

 

 // created: 2017-06-18 00:41:13
$dictionary['CON_CONSULTORIOS']['fields']['dia_ter_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['dia_ter_c']['labelValue']='Atendimento 3º-Feira:';

 

 // created: 2017-06-18 00:50:42
$dictionary['CON_CONSULTORIOS']['fields']['email_sec_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['email_sec_c']['labelValue']='Email da Secretária:';

 

 // created: 2017-06-18 00:25:56
$dictionary['CON_CONSULTORIOS']['fields']['end_cons_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['end_cons_c']['labelValue']='Endereço:';

 

 // created: 2017-06-18 00:29:56
$dictionary['CON_CONSULTORIOS']['fields']['est_cons_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['est_cons_c']['labelValue']='Estado:';

 

 // created: 2017-06-18 00:47:17
$dictionary['CON_CONSULTORIOS']['fields']['hora_qua_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['hora_qua_c']['labelValue']='Horário 4º-Feira:';

 

 // created: 2017-06-18 00:48:22
$dictionary['CON_CONSULTORIOS']['fields']['hora_qui_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['hora_qui_c']['labelValue']='Horário 5º-Feira:';

 

 // created: 2017-06-18 00:45:02
$dictionary['CON_CONSULTORIOS']['fields']['hora_seg_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['hora_seg_c']['labelValue']='Horário 2º-Feira:';

 

 // created: 2017-06-18 00:49:07
$dictionary['CON_CONSULTORIOS']['fields']['hora_sex_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['hora_sex_c']['labelValue']='Horário 6º-Feira:';

 

 // created: 2017-06-18 00:46:22
$dictionary['CON_CONSULTORIOS']['fields']['hora_ter_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['hora_ter_c']['labelValue']='Horário 3º-Feira:';

 

 // created: 2017-06-18 00:25:23
$dictionary['CON_CONSULTORIOS']['fields']['name']['required']=false;
$dictionary['CON_CONSULTORIOS']['fields']['name']['inline_edit']=true;
$dictionary['CON_CONSULTORIOS']['fields']['name']['duplicate_merge']='disabled';
$dictionary['CON_CONSULTORIOS']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['CON_CONSULTORIOS']['fields']['name']['merge_filter']='disabled';
$dictionary['CON_CONSULTORIOS']['fields']['name']['unified_search']=false;

 

 // created: 2017-06-18 00:49:52
$dictionary['CON_CONSULTORIOS']['fields']['nome_sec_c']['inline_edit']='1';
$dictionary['CON_CONSULTORIOS']['fields']['nome_sec_c']['labelValue']='Nome da Secretária:';

 
?>