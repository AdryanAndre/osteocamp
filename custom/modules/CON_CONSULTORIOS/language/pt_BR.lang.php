<?php
// created: 2017-06-18 00:51:42
$mod_strings = array (
  'LBL_NAME' => 'Nome Consultório:',
  'LBL_END_CONS' => 'Endereço:',
  'LBL_CID_CONS' => 'Cidade:',
  'LBL_CEP_CONS' => 'CEP:',
  'LBL_EST_CONS' => 'Estado:',
  'LBL_DIA_SEG' => 'Atendimento 2º-Feira:',
  'LBL_HORA_SEG' => 'Horário 2º-Feira:',
  'LBL_DIA_TER' => 'Atendimento 3º-Feira:',
  'LBL_DIA_QUA' => 'Atendimento 4º-Feira:',
  'LBL_DIA_QUI' => 'Atendimento 5º-Feira:',
  'LBL_DIA_SEX' => 'Atendimento 6º-Feira:',
  'LBL_HORA_TER' => 'Horário 3º-Feira:',
  'LBL_HORA_QUA' => 'Horário 4º-Feira:',
  'LBL_HORA_QUI' => 'Horário 5º-Feira:',
  'LBL_HORA_SEX' => 'Horário 6º-Feira:',
  'LBL_NOME_SEC' => 'Nome da Secretária:',
  'LBL_EMAIL_SEC' => 'Email da Secretária:',
  'LBL_ANIV_SEC' => 'Aniversário da Secretária:',
);