<?php
$module_name = 'CON_CONSULTORIOS';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'end_cons_c',
            'label' => 'LBL_END_CONS',
          ),
          1 => 
          array (
            'name' => 'cid_cons_c',
            'label' => 'LBL_CID_CONS',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'cep_cons_c',
            'label' => 'LBL_CEP_CONS',
          ),
          1 => 
          array (
            'name' => 'est_cons_c',
            'studio' => 'visible',
            'label' => 'LBL_EST_CONS',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'dia_seg_c',
            'label' => 'LBL_DIA_SEG',
          ),
          1 => 
          array (
            'name' => 'hora_seg_c',
            'label' => 'LBL_HORA_SEG',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'dia_ter_c',
            'label' => 'LBL_DIA_TER',
          ),
          1 => 
          array (
            'name' => 'hora_ter_c',
            'label' => 'LBL_HORA_TER',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'dia_qua_c',
            'label' => 'LBL_DIA_QUA',
          ),
          1 => 
          array (
            'name' => 'hora_qua_c',
            'label' => 'LBL_HORA_QUA',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'dia_qui_c',
            'label' => 'LBL_DIA_QUI',
          ),
          1 => 
          array (
            'name' => 'hora_qui_c',
            'label' => 'LBL_HORA_QUI',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'dia_sex_c',
            'label' => 'LBL_DIA_SEX',
          ),
          1 => 
          array (
            'name' => 'hora_sex_c',
            'label' => 'LBL_HORA_SEX',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'nome_sec_c',
            'label' => 'LBL_NOME_SEC',
          ),
          1 => 
          array (
            'name' => 'email_sec_c',
            'label' => 'LBL_EMAIL_SEC',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'aniv_sec_c',
            'label' => 'LBL_ANIV_SEC',
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'accounts_con_consultorios_1_name',
          ),
          1 => 'assigned_user_name',
        ),
      ),
    ),
  ),
);
?>
