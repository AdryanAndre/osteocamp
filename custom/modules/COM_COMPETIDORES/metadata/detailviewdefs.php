<?php
$module_name = 'COM_COMPETIDORES';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'percentual_c',
            'label' => 'LBL_PERCENTUAL',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'accounts_com_competidores_1_name',
            'label' => 'LBL_ACCOUNTS_COM_COMPETIDORES_1_FROM_ACCOUNTS_TITLE',
          ),
          1 => 'assigned_user_name',
        ),
      ),
    ),
  ),
);
?>
