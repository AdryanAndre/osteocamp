<?php
$module_name = 'VIS_VISITA';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'data_hora_visita_c',
            'label' => 'LBL_DATA_HORA_VISITA',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'data_real_visita_c',
            'label' => 'LBL_DATA_REAL_VISITA',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'cons_visita_c',
            'studio' => 'visible',
            'label' => 'LBL_CONS_VISITA',
          ),
          1 => 
          array (
            'name' => 'medi_visita_c',
            'studio' => 'visible',
            'label' => 'LBL_MEDI_VISITA',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'status_aprovacao_c',
            'studio' => 'visible',
            'label' => 'LBL_STATUS_APROVACAO',
          ),
          1 => 
          array (
            'name' => 'status_realizacao_c',
            'studio' => 'visible',
            'label' => 'LBL_STATUS_REALIZACAO',
          ),
        ),
        4 => 
        array (
          0 => 'assigned_user_name',
          1 => '',
        ),
      ),
    ),
  ),
);
?>
