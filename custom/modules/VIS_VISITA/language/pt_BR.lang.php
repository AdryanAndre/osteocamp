<?php
// created: 2017-06-18 01:21:17
$mod_strings = array (
  'LBL_DATA_HORA_VISITA' => 'Dia/Hora Planejamento da Visita:',
  'LBL_DATA_REAL_VISITA' => 'Data/Hora Visita Realizada:',
  'LBL_CONS_VISITA_CON_CONSULTORIOS_ID' => 'Consultório a Visitar: (ID  relacionado)',
  'LBL_CONS_VISITA' => 'Consultório a Visitar:',
  'LBL_MEDI_VISITA_ACCOUNT_ID' => 'Médico a Visitar: (ID Conta relacionado)',
  'LBL_MEDI_VISITA' => 'Médico a Visitar:',
  'LBL_STATUS_APROVACAO' => 'Status Aprovação:',
  'LBL_STATUS_REALIZACAO' => 'Status Realização:',
);