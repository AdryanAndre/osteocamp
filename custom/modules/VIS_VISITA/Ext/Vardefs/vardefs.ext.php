<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2017-06-18 01:17:09
$dictionary['VIS_VISITA']['fields']['account_id_c']['inline_edit']=1;

 

 // created: 2017-06-18 01:16:07
$dictionary['VIS_VISITA']['fields']['cons_visita_c']['inline_edit']='1';
$dictionary['VIS_VISITA']['fields']['cons_visita_c']['labelValue']='Consultório a Visitar:';

 

 // created: 2017-06-18 01:15:32
$dictionary['VIS_VISITA']['fields']['con_consultorios_id_c']['inline_edit']=1;

 

 // created: 2017-06-18 01:10:36
$dictionary['VIS_VISITA']['fields']['data_hora_visita_c']['inline_edit']='1';
$dictionary['VIS_VISITA']['fields']['data_hora_visita_c']['labelValue']='Dia/Hora Planejamento da Visita:';

 

 // created: 2017-06-18 01:12:49
$dictionary['VIS_VISITA']['fields']['data_real_visita_c']['inline_edit']='1';
$dictionary['VIS_VISITA']['fields']['data_real_visita_c']['labelValue']='Data/Hora Visita Realizada:';

 

 // created: 2017-06-18 01:17:09
$dictionary['VIS_VISITA']['fields']['medi_visita_c']['inline_edit']='1';
$dictionary['VIS_VISITA']['fields']['medi_visita_c']['labelValue']='Médico a Visitar:';

 

 // created: 2017-06-18 01:19:14
$dictionary['VIS_VISITA']['fields']['status_aprovacao_c']['inline_edit']='1';
$dictionary['VIS_VISITA']['fields']['status_aprovacao_c']['labelValue']='Status Aprovação:';

 

 // created: 2017-06-18 01:21:17
$dictionary['VIS_VISITA']['fields']['status_realizacao_c']['inline_edit']='1';
$dictionary['VIS_VISITA']['fields']['status_realizacao_c']['labelValue']='Status Realização:';

 
?>