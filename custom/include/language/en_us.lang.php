<?php
$GLOBALS['app_list_strings']['form_trat_list']=array (
  'Dr' => 'Dr.',
  'Dra' => 'Dra.',
);
$GLOBALS['app_list_strings']['classe_list']=array (
  'A' => 'A',
  'B' => 'B',
  'C' => 'C',
  'D' => 'D',
);
$GLOBALS['app_list_strings']['freq_visita_list']=array (
  'Semanal' => 'Semanal',
  'Quinzenal' => 'Quinzenal',
  'Mensal' => 'Mensal',
  'Semestral' => 'Semestral',
  'Anual' => 'Anual',
);
$GLOBALS['app_list_strings']['especialidade_list']=array (
  'Coluna' => 'Coluna',
  'Cranio' => 'Crânio',
  'Maxilo_Facial' => 'Maxilo Facial',
  'Ortopedia' => 'Ortopedia',
);

$GLOBALS['app_list_strings']['brasil_uf_list']=array (
  'AC' => 'AC',
  'AL' => 'AL',
  'AM' => 'AM',
  'AP' => 'AP',
  'BA' => 'BA',
  'CE' => 'CE',
  'DF' => 'DF',
  'ES' => 'ES',
  'GO' => 'GO',
  'MA' => 'MA',
  'MG' => 'MG',
  'MS' => 'MS',
  'MT' => 'MT',
  'PA' => 'PA',
  'PB' => 'PB',
  'PE' => 'PE',
  'PI' => 'PI',
  'PR' => 'PR',
  'RJ' => 'RJ',
  'RN' => 'RN',
  'RO' => 'RO',
  'RR' => 'RR',
  'RS' => 'RS',
  'SC' => 'SC',
  'SE' => 'SE',
  'SP' => 'SP',
  'TO' => 'TO',
);
$GLOBALS['app_list_strings']['crm_cro_list']=array (
  'CRM' => 'CRM',
  'CRO' => 'CRO',
);
$GLOBALS['app_list_strings']['status_cadastro_list']=array (
  'prospeccao' => 'Prospecção',
  'cliente' => 'Cliente',
);
$GLOBALS['app_list_strings']['tipo_cliente_list']=array (
  'Ativo' => 'Ativo',
  'Inativo' => 'Inativo',
  'Exclusao' => 'Exclusão',
);
$GLOBALS['app_list_strings']['status_aprovacao_list']=array (
  'AG' => 'Agendada',
  'AU' => 'Autorizada',
);
$GLOBALS['app_list_strings']['status_realizacao_list']=array (
  'NR' => 'Não Realizada',
  'RE' => 'Realizada',
  'RA' => 'Re-Agendada',
);