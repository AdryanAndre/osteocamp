<?php
// created: 2017-06-20 00:31:13
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de Criação',
  'LBL_DATE_MODIFIED' => 'Data de Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'Criado pelo ID',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Excluído',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado pelo Usuário',
  'LBL_MODIFIED_USER' => 'Modificado pelo usuário',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_ASSIGNED_TO_ID' => 'ID do usuário atribuído',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_SECURITYGROUPS' => 'Grupos de Segurança',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Grupos de Segurança',
  'LBL_LIST_FORM_TITLE' => 'Agendamento de Visitas Lista',
  'LBL_MODULE_NAME' => 'Agendamento de Visitas',
  'LBL_MODULE_TITLE' => 'Agendamento de Visitas',
  'LBL_HOMEPAGE_TITLE' => 'Meu Agendamento de Visitas',
  'LNK_NEW_RECORD' => 'Criar Agendamento de Visitas',
  'LNK_LIST' => 'Vista Agendamento de Visitas',
  'LNK_IMPORT_VIS_VISITA' => 'Importar Agendamento de Visitas',
  'LBL_SEARCH_FORM_TITLE' => 'Filtro Agendamento de Visitas',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Atividades',
  'LBL_VIS_VISITA_SUBPANEL_TITLE' => 'Agendamento de Visitas',
  'LBL_NEW_FORM_TITLE' => 'Novo Agendamento de Visitas',
  'LBL_DATA_HORA_VISITA' => 'Dia/Hora Planejamento da Visita:',
  'LBL_DATA_REAL_VISITA' => 'Data/Hora Visita Realizada:',
  'LBL_CONS_VISITA_CON_CONSULTORIOS_ID' => 'Consultório a Visitar: (ID  relacionado)',
  'LBL_CONS_VISITA' => 'Consultório a Visitar:',
  'LBL_MEDI_VISITA_ACCOUNT_ID' => 'Médico a Visitar: (ID Conta relacionado)',
  'LBL_MEDI_VISITA' => 'Médico a Visitar:',
  'LBL_STATUS_APROVACAO' => 'Status Aprovação:',
  'LBL_STATUS_REALIZACAO' => 'Status Realização:',
);