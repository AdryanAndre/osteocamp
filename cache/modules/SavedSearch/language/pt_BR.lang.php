<?php
// created: 2017-06-20 00:30:57
$mod_strings = array (
  'LBL_MODULE_TITLE' => 'Meus Filtros Salvos',
  'LBL_SEARCH_FORM_TITLE' => 'Meus Filtros Salvos : Filtro',
  'LBL_LIST_FORM_TITLE' => 'Minha Lista de Filtros Salvos',
  'LBL_DELETE_CONFIRM' => 'Tem a certeza que deseja eliminar os filtros selecionados?',
  'LBL_UPDATE_BUTTON_TITLE' => 'Atualizar este filtro salvo',
  'LBL_DELETE_BUTTON_TITLE' => 'Excluir este filtro salvo',
  'LBL_SAVE_BUTTON_TITLE' => 'Salvar o filtro atual',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_LIST_MODULE' => 'Módulo',
  'LBL_ORDER_BY_COLUMNS' => 'Ordenar por coluna:',
  'LBL_DIRECTION' => 'Direção:',
  'LBL_SAVE_SEARCH_AS' => 'Salvar filtro como:',
  'LBL_SAVE_SEARCH_AS_HELP' => 'Esta operação salva as suas configurações de visualização e qualquer filtro no separador de filtros avançados.',
  'LBL_PREVIOUS_SAVED_SEARCH' => 'Filtros salvos anteriores:',
  'LBL_PREVIOUS_SAVED_SEARCH_HELP' => 'Editar ou excluir um filtro salvo existente.',
  'LBL_ASCENDING' => 'Ascendente',
  'LBL_DESCENDING' => 'Descendente',
  'LBL_MODIFY_CURRENT_FILTER' => 'Modificar filtro atual',
  'LBL_MODIFY_CURRENT_SEARCH' => 'Modificar filtro atual',
  'LBL_CREATED_BY' => 'Criado por:',
);