/*
SQLyog Community v11.31 (32 bit)
MySQL - 5.6.12-log : Database - suitecrm
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`suitecrm` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `suitecrm`;

/*Table structure for table `accounts` */

DROP TABLE IF EXISTS `accounts`;

CREATE TABLE `accounts` (
  `id` char(36) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `account_type` varchar(50) DEFAULT NULL,
  `industry` varchar(50) DEFAULT NULL,
  `annual_revenue` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `billing_address_street` varchar(150) DEFAULT NULL,
  `billing_address_city` varchar(100) DEFAULT NULL,
  `billing_address_state` varchar(100) DEFAULT NULL,
  `billing_address_postalcode` varchar(20) DEFAULT NULL,
  `billing_address_country` varchar(255) DEFAULT NULL,
  `rating` varchar(100) DEFAULT NULL,
  `phone_office` varchar(100) DEFAULT NULL,
  `phone_alternate` varchar(100) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `ownership` varchar(100) DEFAULT NULL,
  `employees` varchar(10) DEFAULT NULL,
  `ticker_symbol` varchar(10) DEFAULT NULL,
  `shipping_address_street` varchar(150) DEFAULT NULL,
  `shipping_address_city` varchar(100) DEFAULT NULL,
  `shipping_address_state` varchar(100) DEFAULT NULL,
  `shipping_address_postalcode` varchar(20) DEFAULT NULL,
  `shipping_address_country` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `sic_code` varchar(10) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_accnt_id_del` (`id`,`deleted`),
  KEY `idx_accnt_name_del` (`name`,`deleted`),
  KEY `idx_accnt_assigned_del` (`deleted`,`assigned_user_id`),
  KEY `idx_accnt_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `accounts` */

/*Table structure for table `accounts_am_projecttemplates_1_c` */

DROP TABLE IF EXISTS `accounts_am_projecttemplates_1_c`;

CREATE TABLE `accounts_am_projecttemplates_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `accounts_am_projecttemplates_1accounts_ida` varchar(36) DEFAULT NULL,
  `accounts_am_projecttemplates_1am_projecttemplates_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accounts_am_projecttemplates_1_ida1` (`accounts_am_projecttemplates_1accounts_ida`),
  KEY `accounts_am_projecttemplates_1_alt` (`accounts_am_projecttemplates_1am_projecttemplates_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `accounts_am_projecttemplates_1_c` */

/*Table structure for table `accounts_audit` */

DROP TABLE IF EXISTS `accounts_audit`;

CREATE TABLE `accounts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_accounts_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `accounts_audit` */

/*Table structure for table `accounts_bugs` */

DROP TABLE IF EXISTS `accounts_bugs`;

CREATE TABLE `accounts_bugs` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acc_bug_acc` (`account_id`),
  KEY `idx_acc_bug_bug` (`bug_id`),
  KEY `idx_account_bug` (`account_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `accounts_bugs` */

/*Table structure for table `accounts_cases` */

DROP TABLE IF EXISTS `accounts_cases`;

CREATE TABLE `accounts_cases` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acc_case_acc` (`account_id`),
  KEY `idx_acc_acc_case` (`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `accounts_cases` */

/*Table structure for table `accounts_com_competidores_1_c` */

DROP TABLE IF EXISTS `accounts_com_competidores_1_c`;

CREATE TABLE `accounts_com_competidores_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `accounts_com_competidores_1accounts_ida` varchar(36) DEFAULT NULL,
  `accounts_com_competidores_1com_competidores_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accounts_com_competidores_1_ida1` (`accounts_com_competidores_1accounts_ida`),
  KEY `accounts_com_competidores_1_alt` (`accounts_com_competidores_1com_competidores_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `accounts_com_competidores_1_c` */

/*Table structure for table `accounts_con_consultorios_1_c` */

DROP TABLE IF EXISTS `accounts_con_consultorios_1_c`;

CREATE TABLE `accounts_con_consultorios_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `accounts_con_consultorios_1accounts_ida` varchar(36) DEFAULT NULL,
  `accounts_con_consultorios_1con_consultorios_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accounts_con_consultorios_1_ida1` (`accounts_con_consultorios_1accounts_ida`),
  KEY `accounts_con_consultorios_1_alt` (`accounts_con_consultorios_1con_consultorios_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `accounts_con_consultorios_1_c` */

/*Table structure for table `accounts_contacts` */

DROP TABLE IF EXISTS `accounts_contacts`;

CREATE TABLE `accounts_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_account_contact` (`account_id`,`contact_id`),
  KEY `idx_contid_del_accid` (`contact_id`,`deleted`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `accounts_contacts` */

/*Table structure for table `accounts_cstm` */

DROP TABLE IF EXISTS `accounts_cstm`;

CREATE TABLE `accounts_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  `form_trat_c` varchar(100) DEFAULT NULL,
  `classe_c` varchar(100) DEFAULT NULL,
  `freq_visita_c` varchar(100) DEFAULT NULL,
  `especialidade_c` varchar(100) DEFAULT NULL,
  `uf_crm_c` varchar(100) DEFAULT NULL,
  `crm_cro_c` varchar(100) DEFAULT NULL,
  `num_crmcro_c` varchar(50) DEFAULT NULL,
  `aniversario_c` varchar(5) DEFAULT NULL,
  `email_c` varchar(150) DEFAULT NULL,
  `status_cadastro_c` varchar(100) DEFAULT NULL,
  `tipo_cliente_c` varchar(100) DEFAULT NULL,
  `quant_cirurgiais_c` varchar(3) DEFAULT NULL,
  `predominancia_c` varchar(3) DEFAULT NULL,
  `nome_convenio_c` varchar(150) DEFAULT NULL,
  `perc_convenio_c` varchar(3) DEFAULT NULL,
  `perc_hospital_c` varchar(3) DEFAULT NULL,
  `pref_cirurgica_c` text,
  `particularidade_c` text,
  `percentual_completude_c` int(255) DEFAULT NULL,
  `hos_hospital_id_c` char(36) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `accounts_cstm` */

/*Table structure for table `accounts_opportunities` */

DROP TABLE IF EXISTS `accounts_opportunities`;

CREATE TABLE `accounts_opportunities` (
  `id` varchar(36) NOT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_account_opportunity` (`account_id`,`opportunity_id`),
  KEY `idx_oppid_del_accid` (`opportunity_id`,`deleted`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `accounts_opportunities` */

/*Table structure for table `acl_actions` */

DROP TABLE IF EXISTS `acl_actions`;

CREATE TABLE `acl_actions` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `acltype` varchar(100) DEFAULT NULL,
  `aclaccess` int(3) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aclaction_id_del` (`id`,`deleted`),
  KEY `idx_category_name` (`category`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `acl_actions` */

insert  into `acl_actions`(`id`,`date_entered`,`date_modified`,`modified_user_id`,`created_by`,`name`,`category`,`acltype`,`aclaccess`,`deleted`) values ('1019bf59-dda3-15f1-a2ae-5945a06ca17e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOD_IndexEvent','module',90,0),('11bf0752-57e5-a681-ec08-5945a0aede67','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOR_Scheduled_Reports','module',90,0),('11f937c0-c698-f4a5-1fb1-59459fd82034','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Meetings','module',90,0),('12ae959a-afb7-86c5-e213-59486a558568','2017-06-20 00:19:57','2017-06-20 00:19:57','1','1','export','PME_PROCEDIMENTO_MEDICO','module',90,0),('132864ef-18d4-ab25-cac0-5945c73b9733','2017-06-18 00:22:27','2017-06-18 00:22:27','1','1','list','CON_CONSULTORIOS','module',90,0),('13500879-d5bd-447f-d346-59459f1da58c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Accounts','module',90,0),('13f0560f-e634-f0c3-9390-5945a0724d4e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOW_WorkFlow','module',90,0),('1513b165-4905-8e76-bbba-59459fb76a70','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','ProjectTask','module',90,0),('16e3f176-a415-7e8f-62b1-5945a0ca8692','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOS_Products','module',90,0),('17300297-1d22-7fc6-3119-5945a0d6943b','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Spots','module',90,0),('18c0e43e-bd40-799b-ddcc-5945a05cd9ec','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AM_TaskTemplates','module',90,0),('18cf97fa-f60b-4dba-46f5-5945a00d6b40','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOS_PDF_Templates','module',90,0),('195325bf-eb8e-4014-27e3-59459f96806c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Meetings','module',90,0),('19aa4c25-c8b1-8b6a-8ff8-5945a06e0a0a','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOD_IndexEvent','module',90,0),('19cd6572-87e2-018e-3acf-5945a0c4b17d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','SecurityGroups','module',90,0),('1a0564f2-a797-fde2-e584-5945a0200edd','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','EAPM','module',90,0),('1a5dce7b-a009-0179-f2da-59459f4f86cc','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Leads','module',90,0),('1bcd04c6-d357-cce5-7430-59459f50521d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Meetings','module',90,0),('1bd58188-b168-da5b-272c-5945a0c66220','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOW_WorkFlow','module',90,0),('1c722db2-2500-7afc-a83e-59459f6e5777','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Bugs','module',89,0),('1ca7abc6-70b1-4c18-4c3c-59459f39d832','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Tasks','module',90,0),('1cd547e6-7c3e-4d91-1649-59459ff967e1','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','ProspectLists','module',90,0),('1d2953f1-1ef2-27ab-a893-59459f8d7bfe','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','ProjectTask','module',90,0),('1e4aa652-f09e-4263-ccee-5945a04d9ef3','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOS_Products','module',90,0),('1e887302-fd1d-dcf4-3f65-5945f45aa252','2017-06-18 03:32:11','2017-06-18 03:32:11','1','1','import','PRO_PROCEDIMENTO','module',90,0),('1f0eaa98-8578-1f0f-1650-5945c720d61f','2017-06-18 00:22:27','2017-06-18 00:22:27','1','1','edit','CON_CONSULTORIOS','module',90,0),('1f3e47cc-2e04-9938-9d91-5945a08056aa','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOD_Index','module',90,0),('1f614cb0-a33c-26e1-31af-59459fe00e7f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Calls','module',90,0),('1fb407d8-a6a4-fc04-8053-5945a035e1f0','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOK_Knowledge_Base_Categories','module',90,0),('1fcfe66e-f0a2-fdf9-892f-59459f753802','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Users','module',90,0),('2062e7fe-d119-466a-204f-5945a0284709','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','FP_Event_Locations','module',90,0),('21ae0590-45fb-f051-56a4-5945a0bbd7c6','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOR_Scheduled_Reports','module',90,0),('2223c057-eeb0-27d4-cce0-59459fc1fded','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Emails','module',90,0),('223dbf0f-55bd-904d-229c-59459f61c53d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Meetings','module',90,0),('22840e27-c80b-a4e4-1822-59459f228a0b','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Prospects','module',90,0),('228fcba9-2cbc-90c1-5c5c-59459f5d0597','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Bugs','module',90,0),('22d58ba5-17e1-ed58-5655-5945a01947a2','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOK_Knowledge_Base_Categories','module',90,0),('22eaeebd-957b-8eb1-65bd-5945a0e752d6','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOD_IndexEvent','module',90,0),('239244be-c7ba-30cc-f5b9-5945a0e690fb','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOS_Product_Categories','module',89,0),('248a5550-5d8e-334b-6b75-5945a08b291d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Spots','module',90,0),('249bc0d0-416c-3f51-9bcf-5945a0b1db49','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Documents','module',90,0),('24a62759-27ad-fb32-2d91-5945a0eb0c81','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AM_TaskTemplates','module',90,0),('26a742c7-c6f1-1ac2-17c4-5945a0b15a8c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AM_ProjectTemplates','module',89,0),('26d11068-cd63-7043-fa89-5945a03e9dac','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','jjwg_Address_Cache','module',90,0),('276818e0-55d9-0273-95d7-5945a0d3cc7c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','SecurityGroups','module',90,0),('27d2a6bc-61f4-6397-c41a-59459fbf25a7','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','ProjectTask','module',90,0),('28335089-8580-6c9d-eb61-5945a0a7437e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','EAPM','module',90,0),('2908feea-9c13-f78a-b090-5945c72795e1','2017-06-18 00:22:27','2017-06-18 00:22:27','1','1','delete','CON_CONSULTORIOS','module',90,0),('293ee583-cb4b-2026-7752-59486ade27bf','2017-06-20 00:19:57','2017-06-20 00:19:57','1','1','massupdate','PME_PROCEDIMENTO_MEDICO','module',90,0),('295da370-db68-df8c-1df3-59459f3e65e0','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Calls','module',90,0),('2995143f-8e78-24de-3371-59459fd7380c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Meetings','module',90,0),('2a45df55-da06-5ff9-03bf-59459f111142','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Tasks','module',90,0),('2aae5a0e-07db-21f6-08c2-59459fe1257d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Leads','module',90,0),('2ab950b3-0f17-440b-a121-59459f1cb949','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Users','module',90,0),('2b2913b5-00a2-3dcb-8f88-5945a051b1dc','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','jjwg_Maps','module',89,0),('2becdb79-3b82-f6df-882f-59459f25b68a','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Opportunities','module',90,0),('2c18d94d-10bf-3abe-99eb-5945a076ccf7','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOS_Product_Categories','module',90,0),('2c9e380d-7f49-201f-bc8c-5945a087b199','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOW_Processed','module',90,0),('2cace6a2-7a2c-a57b-5b3c-5945a01cd737','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOD_IndexEvent','module',90,0),('2cdf439f-9020-054a-a29d-5945a01a359d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Spots','module',90,0),('2d321a34-64d4-7073-f853-59459fcacafb','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','ProspectLists','module',90,0),('301fa118-1896-6a3f-a030-59459f71ee95','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','ProjectTask','module',90,0),('302f174b-ddfc-6662-6d8d-59459f4ed7f0','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Contacts','module',89,0),('311fd801-ed2a-be53-1ebd-5945a0f04791','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOR_Reports','module',89,0),('32230590-24cf-1773-a927-5945a086bf5e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOS_PDF_Templates','module',90,0),('32495faf-2b46-fa99-3ef0-5945a06dba11','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','SecurityGroups','module',90,0),('3287b94e-479b-f150-5981-59459f8f8173','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Meetings','module',90,0),('33253ea3-36e1-017a-aa6b-5945f4d92270','2017-06-18 03:32:11','2017-06-18 03:32:11','1','1','export','PRO_PROCEDIMENTO','module',90,0),('34be1a81-0c77-c1b8-7d6d-5945a0364282','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOS_Product_Categories','module',90,0),('35eff2a8-944d-31e7-d170-5945a07d56f5','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOD_IndexEvent','module',90,0),('3602a2b6-15b2-d3a3-bb41-59459fbb3e9f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','ProspectLists','module',90,0),('3605025d-311f-24a4-dd30-59486a871457','2017-06-20 00:19:57','2017-06-20 00:19:57','1','1','import','PME_PROCEDIMENTO_MEDICO','module',90,0),('3794dab3-69d5-792c-72c6-5945a0ce2ed5','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Spots','module',90,0),('379deaff-15ed-6dd2-2278-5945a0f6c56a','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','jjwg_Maps','module',90,0),('37b19fe7-fccc-67e5-5ff3-59459f997a45','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Tasks','module',90,0),('37fdedda-1530-f8f3-35fb-5945a0623975','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Documents','module',90,0),('381208b5-81de-a7a2-fb8d-5945a080e046','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Documents','module',89,0),('382279e6-a6ac-9525-fc6b-59459f911b01','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','EmailMarketing','module',89,0),('3a9502f0-4c76-1ec4-6932-59459f220716','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Opportunities','module',90,0),('3ac6020a-572a-fc62-634d-5945a0892b3a','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOW_WorkFlow','module',90,0),('3ad18b95-dca2-db7a-874c-59459f724a47','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','ProjectTask','module',90,0),('3aec7920-3186-eadc-2903-59459f87ef8f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Leads','module',90,0),('3b6e0be0-91b6-9499-dc44-5945a0a33df2','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','FP_Event_Locations','module',90,0),('3c8c929f-902b-226c-8588-5945a02bf7ba','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','SecurityGroups','module',90,0),('3d543b35-68b7-e7fe-00e5-5945a0dac44d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOP_Case_Updates','module',89,0),('3d742aa0-05ca-f8a2-2e8c-59459f192b8f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Bugs','module',90,0),('3f641d61-33fa-44e8-c313-59459fffd42f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Tasks','module',90,0),('3fd762db-a933-cc03-d122-5945a03b0a53','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Spots','module',90,0),('4038718f-3853-198b-55a2-59459fb80ac8','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','ProspectLists','module',90,0),('405c0265-8c55-aae3-d690-5945a0f04867','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','EAPM','module',90,0),('4105ce9b-abca-921e-1a50-5945a0b0075a','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','OutboundEmailAccounts','module',89,0),('4246063a-b1d7-a07e-cd4c-5945a01ce65f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOS_Product_Categories','module',90,0),('430f5c3b-bd18-8173-dce4-59459f5db581','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','ProjectTask','module',90,0),('4500ebbb-b3ef-01e1-32fb-59459fbf8a37','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Project','module',89,0),('45171553-d220-6000-cbc9-5945a0ea6db2','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','SecurityGroups','module',90,0),('462e9c2a-5f0f-be1c-30b1-5945c7cc6e37','2017-06-18 00:22:27','2017-06-18 00:22:27','1','1','import','CON_CONSULTORIOS','module',90,0),('463aa8cf-8b96-fae1-4893-5945a0efe747','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AM_ProjectTemplates','module',90,0),('46e00dec-9182-6b0b-8003-5945a0eab9f4','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOS_Contracts','module',90,0),('46e1de13-7561-f5b2-b25c-5945a0878176','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','TemplateSectionLine','module',89,0),('475a165c-5575-83a4-35f3-5945a0a28cff','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOK_Knowledge_Base_Categories','module',89,0),('47a2bd49-c28f-1f74-0eae-5945a04c1ac1','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOW_Processed','module',90,0),('47e57e71-0a05-071c-7846-5945a0d8db7d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOP_Case_Updates','module',90,0),('47ecf012-df4e-8b8b-c40f-59459f44739f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Tasks','module',90,0),('482e04a1-3249-835e-fc11-59459f9020ad','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Opportunities','module',90,0),('48ac7d8f-2702-064c-0b86-59459f7644f1','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','EmailMarketing','module',90,0),('4aac8c8f-e507-e6fc-53d0-5945a0b07b54','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','OutboundEmailAccounts','module',90,0),('4abb45cd-1b23-cc13-0567-5945a0ddcb50','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Documents','module',90,0),('4de06742-f212-332f-61ff-59459f8ecd41','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','ProjectTask','module',90,0),('4e245d2b-1d6d-5943-c2d4-5945f533e98d','2017-06-18 03:38:56','2017-06-18 03:38:56','1','1','access','HOS_HOSPITAL','module',89,0),('4ed8b954-28c9-7cbc-71a1-5945f4c3303d','2017-06-18 03:32:11','2017-06-18 03:32:11','1','1','massupdate','PRO_PROCEDIMENTO','module',90,0),('4fa9b608-b0e0-8559-bc07-5945a07ac5cd','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','SecurityGroups','module',90,0),('4fe2a49d-a402-1afa-4510-59459f452f6f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Prospects','module',90,0),('4ff39cf4-5811-9458-d65c-5945a0fe3303','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOS_Product_Categories','module',90,0),('505956f9-3d7a-e94b-5eb3-59459f180706','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','ProspectLists','module',90,0),('514c4e3e-fdcd-f58d-96ee-5945a068b425','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOP_Case_Updates','module',90,0),('51a16aa7-002c-0049-5de3-5945a0f41593','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Calls_Reschedule','module',89,0),('51b4c7a8-18a5-8f57-0cf8-5945c7694fbf','2017-06-18 00:22:27','2017-06-18 00:22:27','1','1','export','CON_CONSULTORIOS','module',90,0),('5289ae25-986e-cd12-69c0-5945a03d1f7e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Documents','module',90,0),('5443bdb0-1bf1-32cf-728e-5945a0176127','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOP_Case_Events','module',89,0),('5494c269-1244-5442-c61b-59459f953186','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Opportunities','module',90,0),('54f2be3c-647c-141c-72fd-5945f4efb1d5','2017-06-18 03:32:11','2017-06-18 03:32:11','1','1','access','PRO_PROCEDIMENTO','module',89,0),('552a1517-038f-9935-3d64-5945a0c547d2','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','OutboundEmailAccounts','module',90,0),('555a900c-089c-ab3a-0f55-5945a0f0dfb7','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','TemplateSectionLine','module',90,0),('560ef458-690d-ad2b-9e89-5945a0a666f0','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOK_Knowledge_Base_Categories','module',90,0),('56a00a3c-dc41-cfcc-15ad-5945a0b7fa82','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','FP_Event_Locations','module',90,0),('56f0f5c8-427b-27e0-5d5f-59459f15943d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Leads','module',90,0),('577e28e0-d920-c549-36c3-59459f20cabd','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Contacts','module',90,0),('58617777-fb04-4052-17ce-5945a0dedd18','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','SecurityGroups','module',90,0),('58af905d-fcf6-9f7f-4d9f-59459f20eb8a','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Project','module',90,0),('58fc776a-cbf2-d554-52d4-5945a0d67d37','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOR_Reports','module',90,0),('593fd506-9fe9-4555-8ab3-5945a0504cd8','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOW_WorkFlow','module',89,0),('5a6b270b-e7a8-b3d6-f349-5945a085c8aa','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOK_KnowledgeBase','module',89,0),('5aca3582-4229-eaf4-9fb1-5945a0fa130d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOP_Case_Updates','module',90,0),('5ad06179-fa50-56f2-5f11-5945a04aed4d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOW_Processed','module',90,0),('5b0e9acc-e8c1-f36e-d101-5945a0d279b6','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Documents','module',90,0),('5b40a636-4594-8272-8de4-5945a0c9e11f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AM_ProjectTemplates','module',90,0),('5b51bb93-4589-052f-1247-5945a0c6f8b7','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','EAPM','module',90,0),('5b7c23f6-aea0-f1bc-a151-59459f4823bd','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','EmailMarketing','module',90,0),('5d6ca9bb-c6f8-d677-13de-5945a0a27b65','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOS_Product_Categories','module',90,0),('5d9666e0-a7ad-02ab-434b-5945a03962e4','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','OutboundEmailAccounts','module',90,0),('5e7fe75e-da02-5e87-ed48-5945c7f08da1','2017-06-18 00:22:27','2017-06-18 00:22:27','1','1','massupdate','CON_CONSULTORIOS','module',90,0),('605fd39c-8fc2-a595-c217-5945a0ec3359','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','TemplateSectionLine','module',90,0),('606f07e4-f7aa-fbed-26ec-5945a04af1b1','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOS_Quotes','module',90,0),('6089eb51-3dd6-8ca2-f253-59459feda313','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Opportunities','module',90,0),('609db74f-f02b-4f1d-c2ec-59459f693c23','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Prospects','module',90,0),('62d2374d-8f9b-c488-c9f5-5945a03be0af','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Documents','module',90,0),('6349b060-fccd-8c00-af0f-5945a06fd898','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Calls_Reschedule','module',90,0),('637c2b05-05b7-af2c-5dbe-5945a08d5b0e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','jjwg_Maps','module',90,0),('638bcc25-6379-d8dc-51d9-59459fc31a0e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Project','module',90,0),('641dd132-f849-b912-e681-59459ffba02c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','EmailTemplates','module',89,0),('6471a9a1-caf4-a40e-7eee-5945a03e78c8','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOP_Case_Updates','module',90,0),('65895462-50f0-2048-66cd-5945a0567920','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOK_KnowledgeBase','module',90,0),('65b815d1-63d4-9722-4da2-5945a078ab98','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AM_ProjectTemplates','module',90,0),('68a40930-d6d2-02e7-7f96-5945a0b05486','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOS_Products','module',90,0),('68cf6c84-cd01-6e2d-d8db-59459fb23c35','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Leads','module',90,0),('69f5a35e-73ec-d4ae-b506-5945a03f980f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOR_Reports','module',90,0),('6a97a620-6acf-d531-cbd1-5945a0a73f38','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AM_TaskTemplates','module',89,0),('6aa24ad8-109c-4bdd-465f-5945a0a1227c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOP_Case_Events','module',90,0),('6ad86645-ab3e-66ca-cb36-5945a0691228','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','TemplateSectionLine','module',90,0),('6b126b62-eddf-0be3-11df-5945a0d00010','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOS_Product_Categories','module',90,0),('6b24d3f9-7c08-95a9-9519-5945d139c0e6','2017-06-18 01:05:29','2017-06-18 01:05:29','1','1','access','VIS_VISITA','module',89,0),('6b48f302-6260-f564-b83b-5945a0c9cde3','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Documents','module',90,0),('6bbf0068-2b3a-3dcf-6ccd-5945a09a19a7','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOD_IndexEvent','module',90,0),('6c60c5fb-3663-72a8-9429-5945a097e16f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','jjwg_Markers','module',89,0),('6dbe3fa3-3b8a-5774-eb23-5945a09a1ee4','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','FP_Event_Locations','module',90,0),('6dee5da7-713c-00af-dc13-5945a0975a95','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOP_Case_Updates','module',90,0),('6e16bd9b-1d04-8cd0-54ce-5945a0d4afa1','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AM_ProjectTemplates','module',90,0),('6e41c209-710e-fee2-d715-5945a026908d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','OutboundEmailAccounts','module',90,0),('6e4d52e0-c917-e7bb-8c35-5945a0b4e8b1','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOK_KnowledgeBase','module',90,0),('6e6e5f9d-f8ea-8e52-327a-59459f0faa8f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Project','module',90,0),('6ef60942-7264-3e90-7258-59459f80a11e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Leads','module',90,0),('6f679e21-ec05-d30e-19d5-59459fcf2bc6','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Contacts','module',90,0),('6fef5bae-48b0-99bf-d43a-59459ffed1b8','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Emails','module',89,0),('70c6d6be-2309-4c5f-f8bd-59459f428c9e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Prospects','module',90,0),('70cc6f55-70d2-6e9c-565d-5945a0ce5deb','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','EAPM','module',90,0),('718b9f31-f347-6e78-0709-5945a0d1e63b','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOK_Knowledge_Base_Categories','module',90,0),('71980888-82c0-c6a8-7840-5945a0239087','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','EAPM','module',90,0),('72783b2f-81fc-56ad-d208-5945a0801693','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOS_Product_Categories','module',90,0),('7361e7ec-695f-7a4d-8da9-5945a046af8d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','TemplateSectionLine','module',90,0),('73a2161b-812b-a5df-03f8-59459fff7216','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','EmailTemplates','module',90,0),('74b127ea-0436-920a-6e5b-5945a05eb8ab','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','jjwg_Areas','module',89,0),('74b9060b-0b14-48e7-a130-59459f95aa3c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Contacts','module',90,0),('755f46d7-6101-3ab1-7945-59459fdf9841','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Accounts','module',89,0),('75b4500d-1ae2-dc4b-beb6-5945a0e77daa','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOP_Case_Events','module',90,0),('75d9dabf-e472-baac-7e09-5945a0e98bfd','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','jjwg_Markers','module',90,0),('761a67b4-1b50-a1bf-9dbe-5945a0e8b1c3','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','FP_events','module',89,0),('76797539-bb7d-a1be-dc11-59459f1d8906','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','EmailMarketing','module',90,0),('768c1624-585e-39cc-1d87-59459f7cb498','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Bugs','module',90,0),('7851494b-8e15-1c7d-2da6-59459fd1ff46','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Campaigns','module',89,0),('78b4f861-8ac8-e9b7-d700-5945a014994c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOK_KnowledgeBase','module',90,0),('78cb87bd-06f1-0ab1-7e34-5945a07d9e82','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AM_ProjectTemplates','module',90,0),('78fb56e2-f965-b0bd-f6b9-59459f7afeb4','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Tasks','module',89,0),('79012b36-bb68-ac53-f79d-5945f56a3ce0','2017-06-18 03:38:56','2017-06-18 03:38:56','1','1','view','HOS_HOSPITAL','module',90,0),('79d0787d-4759-cf4d-d034-59459f56e713','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Prospects','module',90,0),('7acb7247-b863-2105-9e5c-59459ff0fa77','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Opportunities','module',89,0),('7c03c611-16fa-4736-0448-59459f860f82','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Project','module',90,0),('7d3f596f-78b9-1616-dc98-59486a23302f','2017-06-20 00:19:57','2017-06-20 00:19:57','1','1','access','PME_PROCEDIMENTO_MEDICO','module',89,0),('7d9e8591-3923-4301-11fe-5945a08c0a0e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','jjwg_Markers','module',90,0),('7de2a99b-b511-0721-f1af-5945a02609b4','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','TemplateSectionLine','module',90,0),('7f5d4f48-7f21-cde0-5d01-5945a0a02368','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOP_Case_Events','module',90,0),('7f897cf0-f28d-03f3-9c76-5945a07eaebb','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOP_Case_Updates','module',90,0),('7f9e5d96-b76c-7ba8-ac9a-5945a0e5e0bf','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOR_Reports','module',90,0),('8057bab0-bf7d-b0b0-164d-5945a0f66efc','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOW_Processed','module',90,0),('80ec9e08-cb8f-c830-3d15-59459f2b4a98','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','EmailTemplates','module',90,0),('81192bf2-fa00-722b-1071-5945a097eefb','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AM_ProjectTemplates','module',90,0),('815af2b3-37fb-5334-cffd-5945a0a82c30','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOK_KnowledgeBase','module',90,0),('81ff723e-b74b-73c6-b689-59459f3fc8b2','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Campaigns','module',90,0),('827b010a-f541-546b-1638-5945c2d29813','2017-06-17 23:59:18','2017-06-17 23:59:18','1','1','access','COM_COMPETIDORES','module',89,0),('831d4798-4067-25a5-2bc6-5945a0dadac3','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','OutboundEmailAccounts','module',90,0),('836a0a7c-4764-4ff1-d42a-5945a08b10d2','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','SecurityGroups','module',89,0),('83ca1b47-d6b8-2df1-e517-5945a033c04a','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','EAPM','module',90,0),('85d4bc0e-e3c4-d2e8-f1f2-59459f6300a9','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Notes','module',89,0),('86716a70-33bf-ce92-4caf-59459f4cb82c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Prospects','module',90,0),('86802709-8ad6-6326-65f9-5945a0770ca4','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','TemplateSectionLine','module',90,0),('86c5166c-1a61-a27f-0014-59459fb0268c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Project','module',90,0),('87403ed9-177f-3289-a311-5945a0f369ad','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AM_TaskTemplates','module',90,0),('879797c8-901c-3be8-5975-5945d187d8d5','2017-06-18 01:05:29','2017-06-18 01:05:29','1','1','view','VIS_VISITA','module',90,0),('880f9224-6734-5229-fdf1-59459f4292d3','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Accounts','module',90,0),('884a2dbe-1868-d0ac-54a6-59459f25b907','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Calls','module',89,0),('88c40685-9481-eb86-26e1-5945a03f70b7','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOP_Case_Events','module',90,0),('88fcdae2-8928-c412-d846-5945a0062bfe','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','FP_events','module',90,0),('89c38ffa-3de0-9cef-0f5f-5945a0bbcca4','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','jjwg_Maps','module',90,0),('8a148b2e-6782-47b9-2321-5945a018af3a','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Calls_Reschedule','module',90,0),('8bbc0fe8-4dd7-902c-c9e2-5945a02253c7','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AM_ProjectTemplates','module',90,0),('8c1e1784-5f54-3012-762f-59459ff5fcc8','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Campaigns','module',90,0),('8c550635-5514-8949-57b8-5945a0634f4e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOR_Scheduled_Reports','module',89,0),('8c725369-1cb8-93bf-36dc-5945a0458359','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOK_Knowledge_Base_Categories','module',90,0),('8ceef99a-9df9-cd8f-e590-59459f3069d7','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Contacts','module',90,0),('8d4e47aa-40ac-433d-271f-5945a03ccc37','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOS_Invoices','module',89,0),('8dab3bfb-efdc-086d-91b9-59459f0cf508','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','EmailMarketing','module',90,0),('8dd0834e-3d64-dca5-2ef6-5945f51e314a','2017-06-18 03:38:56','2017-06-18 03:38:56','1','1','list','HOS_HOSPITAL','module',90,0),('8de89d28-0748-690f-73d2-5945a0915d6e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','jjwg_Markers','module',90,0),('8dffc367-27f4-38d0-8009-59459feb7d33','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Emails','module',90,0),('8e7d77f6-e452-8432-f28f-59459f171cc2','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Bugs','module',90,0),('8eb4ab63-0eeb-8be3-10c2-5945a06671b4','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOK_KnowledgeBase','module',90,0),('8ebf24bf-8256-6a8c-ab8d-59459fbca6bc','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','EmailTemplates','module',90,0),('8ec812c4-4b9d-5988-29fe-59459f23b70b','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Opportunities','module',90,0),('8ef6bdbb-ed59-a68f-58fe-59459f25f44c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Project','module',90,0),('8f869016-9366-f962-5ca8-59459f7c55ae','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Prospects','module',90,0),('8fa8d0bb-1026-5f98-612c-5945a0f5b1a0','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOP_Case_Updates','module',90,0),('90170a83-41fc-8933-9a5f-59459f93d6cc','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Meetings','module',90,0),('90d20bf4-e6e2-7db2-1f0a-5945a0a4c165','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','TemplateSectionLine','module',90,0),('918227ce-aed9-27a6-7520-5945a095761e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','jjwg_Areas','module',90,0),('91a10662-2dfe-7379-5361-5945c7422894','2017-06-18 00:22:27','2017-06-18 00:22:27','1','1','view','CON_CONSULTORIOS','module',90,0),('92138feb-438a-2fac-52a0-5945a03b804c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','FP_events','module',90,0),('9242b7cb-2eb2-043e-9fba-5945a0e7bfd6','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOP_Case_Events','module',90,0),('93602cb3-c4b1-36ae-7fb4-5945a00d9c77','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','jjwg_Maps','module',90,0),('94bb9a16-3ce4-9441-8af7-5945d15c10dc','2017-06-18 01:05:29','2017-06-18 01:05:29','1','1','list','VIS_VISITA','module',90,0),('95047bed-8fb5-bb1e-0181-59459fbf9549','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Campaigns','module',90,0),('954e0ed8-14ff-748e-12ae-5945a092930a','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOS_Quotes','module',89,0),('963dcc66-6901-9d9c-502e-5945a0fa0669','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','jjwg_Markers','module',90,0),('9663a8d9-4397-bdc1-f9f1-5945a0d2a712','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOR_Scheduled_Reports','module',90,0),('96843bd5-cb4f-1d87-4c0c-5945a0ea8af7','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Calls_Reschedule','module',90,0),('96c57051-15e9-a9b9-680e-59459fc32737','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Notes','module',90,0),('96cb02e2-1283-0724-707c-59459f3cc50a','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','ProjectTask','module',89,0),('96fbac64-7250-b4ca-f7aa-59459fa185ab','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Calls','module',90,0),('9915ffc0-d73e-5a9c-5974-5945a002e255','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','OutboundEmailAccounts','module',90,0),('992a00bb-dc2c-420d-2bba-5945a091126e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','FP_Event_Locations','module',90,0),('992f4ba4-ab64-e331-af34-59459ff151e3','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Tasks','module',90,0),('997380a3-732e-1e09-c39b-5945a0caf70b','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOD_Index','module',89,0),('99bf38e9-8f43-639e-a6ed-59459fc7bf89','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Project','module',90,0),('9b581fe5-fa2a-41ea-4f5c-5945f5574cab','2017-06-18 03:38:56','2017-06-18 03:38:56','1','1','edit','HOS_HOSPITAL','module',90,0),('9b6be8e5-c363-26c8-a7e2-59459f26b14f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','EmailTemplates','module',90,0),('9ba65936-aeeb-b118-43ee-5945a0b41e5d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','jjwg_Maps','module',90,0),('9bb2979c-6f85-46bb-d0c7-59459f553332','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Cases','module',89,0),('9c1943ee-419b-aeec-2bc9-5945a0eea1c7','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','FP_events','module',90,0),('9cc25622-0099-425b-b707-5945a0d66379','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOR_Reports','module',90,0),('9dee8cd4-deb2-3bee-4459-59459ff8f89f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Opportunities','module',90,0),('9e4d7e1a-25a1-2a25-8fd5-5945a0928f84','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','jjwg_Markers','module',90,0),('9e79064b-552e-e783-0684-59459f00e2c0','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Accounts','module',90,0),('9f0fce5c-d04f-081a-cded-59459ff84fa0','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Campaigns','module',90,0),('9fbced75-4a64-30d7-a01f-5945d1777d5d','2017-06-18 01:05:29','2017-06-18 01:05:29','1','1','edit','VIS_VISITA','module',90,0),('a05e5bba-d93a-f678-b9dc-5945a04ba3a5','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Alerts','module',89,0),('a1ab81a7-fd57-0d92-8e3c-5945a0419573','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','jjwg_Areas','module',90,0),('a1c5370a-8d32-5486-1f36-5945a0d3b122','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Calls_Reschedule','module',90,0),('a1d7408e-0b1e-c90c-9ed1-59459f40ed98','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Bugs','module',90,0),('a1e44e58-5594-5ecc-0115-59459fcd42e2','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','EmailMarketing','module',90,0),('a2169e7a-f157-3751-d25b-5945a04854f4','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOK_KnowledgeBase','module',90,0),('a267784b-e24e-c196-04a6-5945a06fc004','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AM_TaskTemplates','module',90,0),('a3330f6e-59cb-88b3-d9d3-5945a0723b84','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOW_Processed','module',90,0),('a388abb8-8ce6-88eb-1506-5945a0dc24e4','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','jjwg_Maps','module',90,0),('a3f8a7d8-b410-7580-a41c-5945a0897cbb','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOD_Index','module',90,0),('a40219b4-c4ab-b202-e4a9-5945a02b0de9','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOP_Case_Events','module',90,0),('a41bb42c-87ac-bdd9-eefa-5945a05ed91d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOR_Scheduled_Reports','module',90,0),('a43945bd-9c81-fef3-2211-5945c2e7aeda','2017-06-17 23:59:18','2017-06-17 23:59:18','1','1','view','COM_COMPETIDORES','module',90,0),('a684a956-1113-63b5-3b55-5945a0a2ecaf','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','jjwg_Markers','module',90,0),('a69f6410-e897-1942-7d43-5945a0f7a43a','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOS_Invoices','module',90,0),('a6f79aa6-9677-ad49-4078-59459fee0b1e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Calls','module',90,0),('a7071e5a-830f-3074-415d-59459f46aa15','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Emails','module',90,0),('a732cde5-2c7d-03e1-09ed-59459fba649b','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Notes','module',90,0),('a7dbe0de-733f-6d63-c604-5945a00526a7','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOK_Knowledge_Base_Categories','module',90,0),('a7e63c7f-8b4a-a295-212b-59459f5421e8','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Campaigns','module',90,0),('a9237766-ae0b-18a8-9d3d-5945a00db132','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','OutboundEmailAccounts','module',90,0),('a9a17309-a28c-a00a-a4d3-5945a073a13a','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Alerts','module',90,0),('aa7688d6-df9c-fee0-deb6-5945a00a56da','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','FP_events','module',90,0),('aafdbbb3-977c-009d-107c-5945a0d37c93','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOS_Contracts','module',89,0),('abf9dbdb-dbcc-5143-507e-5945a02a3859','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','jjwg_Maps','module',90,0),('ace568e4-1961-52a0-b2f3-59459ffb0c2f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','EmailTemplates','module',90,0),('acf877ed-d8c4-f057-ad61-5945d171d374','2017-06-18 01:05:29','2017-06-18 01:05:29','1','1','delete','VIS_VISITA','module',90,0),('ad642086-f111-e203-80c6-5945a01a9512','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOD_Index','module',90,0),('ade7c543-c829-6ad5-afb7-59459f7d1131','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Accounts','module',90,0),('ae376254-3c27-0955-dba4-5945f5c4c175','2017-06-18 03:38:56','2017-06-18 03:38:56','1','1','delete','HOS_HOSPITAL','module',90,0),('ae65aa59-7545-e442-3d58-5945a0f53344','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOS_Quotes','module',90,0),('ae8501e9-1528-22cf-d939-5945f4df77f5','2017-06-18 03:32:11','2017-06-18 03:32:11','1','1','view','PRO_PROCEDIMENTO','module',90,0),('aeaad485-3f17-b686-adcb-5945a0d8ad3e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','jjwg_Markers','module',90,0),('aec209f7-ada4-fd37-48e7-5945c20a982a','2017-06-17 23:59:18','2017-06-17 23:59:18','1','1','list','COM_COMPETIDORES','module',90,0),('af1ba915-a1f9-b90a-8817-5945a0c66344','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Calls_Reschedule','module',90,0),('af83b2d8-d427-d834-9045-59459ffa9aaa','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Cases','module',90,0),('afa6bd4e-11c6-2e80-162b-59459f9f2b0f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Bugs','module',90,0),('b035dfa9-abf5-a3eb-a955-5945a087ea16','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOP_Case_Events','module',90,0),('b06abeee-ea4a-88aa-70ab-59486afc9ec2','2017-06-20 00:19:57','2017-06-20 00:19:57','1','1','view','PME_PROCEDIMENTO_MEDICO','module',90,0),('b074798e-71c4-c20c-42d6-5945a0e213a6','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','jjwg_Address_Cache','module',89,0),('b09ea351-5872-bcac-3ba1-5945a011132c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOR_Scheduled_Reports','module',90,0),('b157109a-c283-0e1b-2571-5945a0103675','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOW_Processed','module',90,0),('b159eb96-e858-aec6-aedc-5945a05850c0','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','jjwg_Areas','module',90,0),('b1734051-0e59-17e9-5f83-5945a0f68f22','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Alerts','module',90,0),('b21016ba-986c-4bfb-7b19-59459fa1d5f3','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Campaigns','module',90,0),('b24e3f6e-41af-b757-fd48-59459fae54cc','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Contacts','module',90,0),('b447aaae-0304-f2d3-81a6-5945a0e33104','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','FP_events','module',90,0),('b4a7ad3b-4ba9-3822-c4c2-59459fe391e4','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Calls','module',90,0),('b4c37441-fc00-656f-54f6-59459fd88690','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','EmailMarketing','module',90,0),('b61bb4b5-7e5a-bf5e-f77f-5945a095ad02','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOS_Contracts','module',90,0),('b734667b-721c-78a5-5f2a-5945a0643035','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','FP_Event_Locations','module',89,0),('b7422824-1c04-1495-0b72-59459fd8eaf9','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Notes','module',90,0),('b74c6453-8bf9-e58b-2ede-59459f75836a','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Emails','module',90,0),('b79d0a71-446e-bb53-db3f-5945a0a72543','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOW_WorkFlow','module',90,0),('b803f047-bd8c-d0b7-07a9-5945d1f96f1a','2017-06-18 01:05:29','2017-06-18 01:05:29','1','1','import','VIS_VISITA','module',90,0),('b9c562f1-c9b8-594d-7070-5945a04437f0','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOS_Invoices','module',90,0),('b9d18012-e2db-852f-1e1d-5945a0c0f7ff','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Alerts','module',90,0),('b9da9251-2612-d25b-e8b3-5945c2611d26','2017-06-17 23:59:18','2017-06-17 23:59:18','1','1','edit','COM_COMPETIDORES','module',90,0),('b9dd58f1-72e8-366d-fe5b-5945a07ad2d1','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','jjwg_Areas','module',90,0),('ba2c2e39-e9de-68e3-6c68-59459f2baf74','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Cases','module',90,0),('ba5ebec0-bd44-948b-df08-5945a0f58a94','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Calls_Reschedule','module',90,0),('ba9ad840-e229-4188-09c7-5945a0ecc7cb','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOR_Reports','module',90,0),('badaffe9-723e-4d21-a40d-59459ff0c5d7','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Campaigns','module',90,0),('bafa2897-0f9f-d5bf-1be8-5945a080e7be','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOD_Index','module',90,0),('bbbcd144-5b23-25b7-7ccc-59459f208992','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','EmailTemplates','module',90,0),('bc58a2ca-0d43-5943-0648-5945a0026b6b','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOK_KnowledgeBase','module',90,0),('bced649c-8f8a-e030-413c-5945a09f13ca','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','jjwg_Address_Cache','module',90,0),('bd6fd0b2-433f-66a1-c851-5945a09d9ae2','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','FP_events','module',90,0),('bd761f45-044f-301b-6c65-5945a045d6ae','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AM_TaskTemplates','module',90,0),('bee56158-c07e-b420-0aa2-5945a019aa8e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOS_Contracts','module',90,0),('bf53fd14-2042-452e-8279-59459fe44b20','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Leads','module',89,0),('bfb0019d-be4d-c4e3-51ae-59459fae3516','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Accounts','module',90,0),('c0270659-a39b-6247-d8db-5945a0394b1a','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOW_Processed','module',90,0),('c126af1d-99bc-7373-f3f2-59486a2493e0','2017-06-20 00:19:57','2017-06-20 00:19:57','1','1','list','PME_PROCEDIMENTO_MEDICO','module',90,0),('c12b02b0-472e-2a24-4f3f-59459f7ccaad','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Notes','module',90,0),('c147d0be-58db-c4b0-995b-5945a0b69953','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOS_Quotes','module',90,0),('c19431cb-5734-96bd-3d97-5945f5c9bf71','2017-06-18 03:38:56','2017-06-18 03:38:56','1','1','import','HOS_HOSPITAL','module',90,0),('c1b6d814-bdbb-50cf-a636-59459fdb5a29','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Users','module',89,0),('c1c77274-1bdb-a7de-85d1-5945a0b75e92','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Alerts','module',90,0),('c1fe0c7c-fca6-4e31-e7cb-5945a0ad3714','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOS_PDF_Templates','module',90,0),('c20b594f-94cd-496a-a90b-5945a0e87208','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','FP_Event_Locations','module',90,0),('c4507c56-14da-2c71-5fd8-5945a03bc10f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','jjwg_Areas','module',90,0),('c4647dff-8e02-2842-2c36-5945c21df553','2017-06-17 23:59:18','2017-06-17 23:59:18','1','1','delete','COM_COMPETIDORES','module',90,0),('c583faf8-4af8-de59-c873-5945d1ea53db','2017-06-18 01:05:29','2017-06-18 01:05:29','1','1','export','VIS_VISITA','module',90,0),('c65f09a7-d106-5615-9d36-59459fced20b','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Emails','module',90,0),('c6731906-ad81-a700-1530-5945a0d64d4c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Spots','module',89,0),('c707731e-57eb-e526-8721-5945a0560f48','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOS_Invoices','module',90,0),('c7358c76-d3c6-8ebc-df65-59459f19e981','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Calls','module',90,0),('c758903b-f964-884f-c11a-5945a0fff55c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','FP_events','module',90,0),('c7684258-d3fd-28b4-c749-5945a0f730cd','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','jjwg_Address_Cache','module',90,0),('c7a51ab3-df1e-f236-5985-59459fdc361e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','EmailTemplates','module',90,0),('c7b3aae1-cc86-48bf-a40f-59459fb7f00e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','EmailMarketing','module',90,0),('c7c56a7c-828d-dbd9-9ae3-5945a0a9218e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Calls_Reschedule','module',90,0),('c7e1c051-ee0f-a824-94d1-59459fdc5773','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Cases','module',90,0),('c8270e0d-984c-2fa2-71dc-5945a0bf7ff1','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOR_Scheduled_Reports','module',90,0),('c8f7fb81-c98f-fb54-3400-5945a0a078e1','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOS_Contracts','module',90,0),('ca1130d6-0598-b90b-2b30-5945a01c7005','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Alerts','module',90,0),('ca54ad13-98c9-784c-1089-59459f3f1ca9','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Notes','module',90,0),('ca990c2a-6625-1649-c958-59459fc8e926','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','ProspectLists','module',90,0),('caba4b3c-32d0-fa17-dd0e-59459f018dd3','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Users','module',90,0),('cc3d9ead-dc71-ea89-57b5-59459fd00036','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Accounts','module',90,0),('ce5615ce-812e-2572-5367-59459fa74763','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Contacts','module',90,0),('ce71ede2-8112-59c4-0acb-5945a088f6c4','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOS_PDF_Templates','module',89,0),('cf7c46c0-519e-e09d-be78-5945c2f784e3','2017-06-17 23:59:18','2017-06-17 23:59:18','1','1','import','COM_COMPETIDORES','module',90,0),('cfd60aec-1e6b-1707-829e-59459fcc76d0','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Emails','module',90,0),('cfe41733-9df7-57ca-03d6-59459f92bfd9','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Cases','module',90,0),('d005b47e-2711-c2fd-3e45-5945a0a04481','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOK_Knowledge_Base_Categories','module',90,0),('d0695c58-378b-4740-0d01-5945d14ada00','2017-06-18 01:05:29','2017-06-18 01:05:29','1','1','massupdate','VIS_VISITA','module',90,0),('d0703d85-a866-af4b-637c-5945a0d25aec','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOD_Index','module',90,0),('d12e698a-9fda-fe7f-8f32-5945a07289bc','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOS_Invoices','module',90,0),('d20290d7-ddb3-16cf-cfb8-5945a0111676','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AM_TaskTemplates','module',90,0),('d2074826-ecd6-c315-e9ec-5945a020a31d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOS_Contracts','module',90,0),('d21d0a85-1821-cbc8-87d3-5945a0b3ac7d','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Alerts','module',90,0),('d26f56c5-cef3-89a8-2354-59486adcf7ab','2017-06-20 00:19:57','2017-06-20 00:19:57','1','1','edit','PME_PROCEDIMENTO_MEDICO','module',90,0),('d27b320f-497c-6956-8025-5945a0ea91d2','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','jjwg_Address_Cache','module',90,0),('d2cb5e06-1e66-5cbd-6507-59459f52aee4','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Users','module',90,0),('d3ed0c67-8323-e892-3b03-5945a0668dca','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Spots','module',90,0),('d41480ed-3ec2-605c-78cd-5945f44f0819','2017-06-18 03:32:11','2017-06-18 03:32:11','1','1','list','PRO_PROCEDIMENTO','module',90,0),('d5d277cf-b0b7-d933-8370-5945a02e6aa2','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOR_Reports','module',90,0),('d6e0e125-b755-e7d7-c563-5945a00ebba1','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOS_PDF_Templates','module',90,0),('d7651bde-55fa-9160-b19e-59459ffbbd93','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','ProspectLists','module',89,0),('d932937a-8286-04ca-fd82-5945a02e0a18','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOS_Products','module',89,0),('d9ea2c85-54dc-2073-59b0-5945f5c43884','2017-06-18 03:38:56','2017-06-18 03:38:56','1','1','export','HOS_HOSPITAL','module',90,0),('d9fe2091-3c3e-38a9-007b-5945a08cb00a','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOS_Invoices','module',90,0),('dafe0270-3bd2-61b2-7953-59459f662ac9','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Notes','module',90,0),('dce71624-cc99-2221-ae5a-5945a014d906','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOS_Quotes','module',90,0),('dd0c594a-4c82-25d9-6cbb-5945a0d76557','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Alerts','module',90,0),('dd392d24-2fdb-d7a9-96c8-5945a04917ef','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','jjwg_Address_Cache','module',90,0),('ddb18cd3-26a1-b5d9-0aeb-59459f9bb116','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Cases','module',90,0),('de6db51a-4c56-024e-e6a9-5945a045b749','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOW_WorkFlow','module',90,0),('deba50c0-f5ae-8375-f9d8-5945c77ebaf5','2017-06-18 00:22:27','2017-06-18 00:22:27','1','1','access','CON_CONSULTORIOS','module',89,0),('dfa80854-31f2-bfa6-95e2-5945f49b492e','2017-06-18 03:32:11','2017-06-18 03:32:11','1','1','delete','PRO_PROCEDIMENTO','module',90,0),('dfbc2106-f96a-19bb-3195-5945a022ba90','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOS_PDF_Templates','module',90,0),('dfd8e264-301c-bedc-eb52-5945a006e1b0','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','jjwg_Areas','module',90,0),('e0482b07-773a-0265-b981-59459fa54db7','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','Users','module',90,0),('e096e951-4e83-42be-f75c-5945a099aeea','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOR_Scheduled_Reports','module',90,0),('e0ee0bcb-3cdb-9169-0a91-5945a0d9d5fd','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOS_Products','module',90,0),('e1649c10-4fba-4283-9f23-5945a0a8f123','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOS_Contracts','module',90,0),('e197ccb2-6849-431e-0954-59486abe852a','2017-06-20 00:19:57','2017-06-20 00:19:57','1','1','delete','PME_PROCEDIMENTO_MEDICO','module',90,0),('e1bb6a68-5b8f-c950-6fe5-5945a0a0a484','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOS_Products','module',90,0),('e2a5537d-169a-053c-f1a5-5945a0417fe7','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','FP_Event_Locations','module',90,0),('e2ab6593-f9f5-6d5d-1079-5945a04477d4','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Spots','module',90,0),('e3a65faf-fbcf-799f-2e04-5945a03ee7fc','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOD_Index','module',90,0),('e4333841-d2f4-3524-ad9a-5945a01fb69b','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOS_Invoices','module',90,0),('e4c24bab-6cc9-a796-5a95-59459f0e2316','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Meetings','module',89,0),('e5249c7b-4241-a687-07dc-5945c2b9cc23','2017-06-17 23:59:18','2017-06-17 23:59:18','1','1','export','COM_COMPETIDORES','module',90,0),('e5919a07-3eff-0902-b719-59459fb3ab89','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Cases','module',90,0),('e69e9d85-601d-09e1-908b-5945a0dbfe73','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOR_Reports','module',90,0),('e6b047cb-c3e4-df5b-fbb4-59459f58caa2','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','Leads','module',90,0),('e6ee7094-0239-6dd0-c922-5945a0de47f5','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOD_IndexEvent','module',89,0),('e6fe96a3-d902-d7de-ae91-5945a01fba19','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOS_PDF_Templates','module',90,0),('e77406e1-4561-e032-9fcd-59459f97cfff','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Emails','module',90,0),('e781ab89-11bc-bb72-0436-5945a082b49c','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOW_WorkFlow','module',90,0),('e7d258a9-2297-f3ac-6832-5945a0e145fd','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','jjwg_Address_Cache','module',90,0),('e7ffd103-54ea-b826-e915-59459fdbc90b','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Notes','module',90,0),('e864147c-e9f2-deac-cf5e-59459fde1926','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','Users','module',90,0),('e88030b2-c566-0f0b-b264-59459f398762','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','Contacts','module',90,0),('e9b18f81-0621-c022-8b85-5945a07cd4ef','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','EAPM','module',89,0),('e9e78e84-d752-26a3-a612-5945a0f41f3b','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOS_Quotes','module',90,0),('ea56284d-1ec6-d1ca-80a0-5945a04e6de6','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOS_Contracts','module',90,0),('ea673159-e926-fbea-c8a7-5945a0f6a214','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','AOS_Products','module',90,0),('ebae759f-f9fd-13b0-fc59-5945a06823e6','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AM_TaskTemplates','module',90,0),('ebdfe4a4-b036-326b-6863-5945f493d54e','2017-06-18 03:32:11','2017-06-18 03:32:11','1','1','edit','PRO_PROCEDIMENTO','module',90,0),('ecc55dbc-4418-f19a-e39f-5945f56cd065','2017-06-18 03:38:56','2017-06-18 03:38:56','1','1','massupdate','HOS_HOSPITAL','module',90,0),('ed0c0718-3593-88d1-a98e-5945a0d8e4ab','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOS_Invoices','module',90,0),('ed9201ac-4ef5-1b2e-f229-5945a0e625f1','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','AOS_Quotes','module',90,0),('edf24674-c8e4-ec89-01e8-59459f31121e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','Cases','module',90,0),('ef629eec-fe22-17c3-0823-5945a072e0ba','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOW_WorkFlow','module',90,0),('efeb036b-d527-575e-b20f-5945a0a32cb7','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','delete','AOS_PDF_Templates','module',90,0),('f016acbf-6c77-83f6-ca46-5945a0fa0d5b','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','jjwg_Address_Cache','module',90,0),('f0893020-46aa-1cfd-3506-5945c2477012','2017-06-17 23:59:18','2017-06-17 23:59:18','1','1','massupdate','COM_COMPETIDORES','module',90,0),('f0baafe6-dec2-00e0-0653-59459f76943f','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','ProspectLists','module',90,0),('f0cab60b-073a-91b2-1399-59459fce0cb8','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Users','module',90,0),('f148cb83-eb66-6398-29fe-5945a07afa39','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','view','AOD_IndexEvent','module',90,0),('f193bc4d-9129-8aa4-40e8-5945a09d325e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','AOW_Processed','module',89,0),('f1bc0a82-b8f1-812f-af6b-59459f9404b1','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','list','Tasks','module',90,0),('f1ca28d2-f98d-1c4d-8f99-59459f185442','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Accounts','module',90,0),('f1e2d111-549d-f75b-0698-5945a02fe402','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','edit','AOS_Products','module',90,0),('f29f5997-0229-8c71-ac8d-5945a0cb0048','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','AOS_Quotes','module',90,0),('f2eddd3b-f347-326b-1d74-5945a04a2de0','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','massupdate','jjwg_Areas','module',90,0),('f348f334-dc00-4e02-3077-59459fd59135','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','access','Prospects','module',89,0),('f3f126da-b45b-0475-786d-5945a0c4f2b0','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','export','AOD_Index','module',90,0),('f48003bd-e13e-67da-4627-59459f84d9a2','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Calls','module',90,0),('f69d074e-69e6-51aa-0688-59459f924a34','2017-06-17 21:31:32','2017-06-17 21:31:32','1','','import','Bugs','module',90,0);

/*Table structure for table `acl_roles` */

DROP TABLE IF EXISTS `acl_roles`;

CREATE TABLE `acl_roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aclrole_id_del` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `acl_roles` */

/*Table structure for table `acl_roles_actions` */

DROP TABLE IF EXISTS `acl_roles_actions`;

CREATE TABLE `acl_roles_actions` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `action_id` varchar(36) DEFAULT NULL,
  `access_override` int(3) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_acl_role_id` (`role_id`),
  KEY `idx_acl_action_id` (`action_id`),
  KEY `idx_aclrole_action` (`role_id`,`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `acl_roles_actions` */

/*Table structure for table `acl_roles_users` */

DROP TABLE IF EXISTS `acl_roles_users`;

CREATE TABLE `acl_roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_aclrole_id` (`role_id`),
  KEY `idx_acluser_id` (`user_id`),
  KEY `idx_aclrole_user` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `acl_roles_users` */

/*Table structure for table `address_book` */

DROP TABLE IF EXISTS `address_book`;

CREATE TABLE `address_book` (
  `assigned_user_id` char(36) NOT NULL,
  `bean` varchar(50) DEFAULT NULL,
  `bean_id` char(36) NOT NULL,
  KEY `ab_user_bean_idx` (`assigned_user_id`,`bean`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `address_book` */

/*Table structure for table `alerts` */

DROP TABLE IF EXISTS `alerts`;

CREATE TABLE `alerts` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `target_module` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `url_redirect` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `alerts` */

/*Table structure for table `am_projecttemplates` */

DROP TABLE IF EXISTS `am_projecttemplates`;

CREATE TABLE `am_projecttemplates` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Draft',
  `priority` varchar(100) DEFAULT 'High',
  `override_business_hours` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `am_projecttemplates` */

/*Table structure for table `am_projecttemplates_audit` */

DROP TABLE IF EXISTS `am_projecttemplates_audit`;

CREATE TABLE `am_projecttemplates_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_am_projecttemplates_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `am_projecttemplates_audit` */

/*Table structure for table `am_projecttemplates_contacts_1_c` */

DROP TABLE IF EXISTS `am_projecttemplates_contacts_1_c`;

CREATE TABLE `am_projecttemplates_contacts_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `am_projecttemplates_ida` varchar(36) DEFAULT NULL,
  `contacts_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `am_projecttemplates_contacts_1_alt` (`am_projecttemplates_ida`,`contacts_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `am_projecttemplates_contacts_1_c` */

/*Table structure for table `am_projecttemplates_project_1_c` */

DROP TABLE IF EXISTS `am_projecttemplates_project_1_c`;

CREATE TABLE `am_projecttemplates_project_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `am_projecttemplates_project_1am_projecttemplates_ida` varchar(36) DEFAULT NULL,
  `am_projecttemplates_project_1project_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `am_projecttemplates_project_1_ida1` (`am_projecttemplates_project_1am_projecttemplates_ida`),
  KEY `am_projecttemplates_project_1_alt` (`am_projecttemplates_project_1project_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `am_projecttemplates_project_1_c` */

/*Table structure for table `am_projecttemplates_users_1_c` */

DROP TABLE IF EXISTS `am_projecttemplates_users_1_c`;

CREATE TABLE `am_projecttemplates_users_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `am_projecttemplates_ida` varchar(36) DEFAULT NULL,
  `users_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `am_projecttemplates_users_1_alt` (`am_projecttemplates_ida`,`users_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `am_projecttemplates_users_1_c` */

/*Table structure for table `am_tasktemplates` */

DROP TABLE IF EXISTS `am_tasktemplates`;

CREATE TABLE `am_tasktemplates` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Not Started',
  `priority` varchar(100) DEFAULT 'High',
  `percent_complete` int(255) DEFAULT '0',
  `predecessors` int(255) DEFAULT NULL,
  `milestone_flag` tinyint(1) DEFAULT '0',
  `relationship_type` varchar(100) DEFAULT 'FS',
  `task_number` int(255) DEFAULT NULL,
  `order_number` int(255) DEFAULT NULL,
  `estimated_effort` int(255) DEFAULT NULL,
  `utilization` varchar(100) DEFAULT '0',
  `duration` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `am_tasktemplates` */

/*Table structure for table `am_tasktemplates_am_projecttemplates_c` */

DROP TABLE IF EXISTS `am_tasktemplates_am_projecttemplates_c`;

CREATE TABLE `am_tasktemplates_am_projecttemplates_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `am_tasktemplates_am_projecttemplatesam_projecttemplates_ida` varchar(36) DEFAULT NULL,
  `am_tasktemplates_am_projecttemplatesam_tasktemplates_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `am_tasktemplates_am_projecttemplates_ida1` (`am_tasktemplates_am_projecttemplatesam_projecttemplates_ida`),
  KEY `am_tasktemplates_am_projecttemplates_alt` (`am_tasktemplates_am_projecttemplatesam_tasktemplates_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `am_tasktemplates_am_projecttemplates_c` */

/*Table structure for table `am_tasktemplates_audit` */

DROP TABLE IF EXISTS `am_tasktemplates_audit`;

CREATE TABLE `am_tasktemplates_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_am_tasktemplates_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `am_tasktemplates_audit` */

/*Table structure for table `aobh_businesshours` */

DROP TABLE IF EXISTS `aobh_businesshours`;

CREATE TABLE `aobh_businesshours` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `opening_hours` varchar(100) DEFAULT '1',
  `closing_hours` varchar(100) DEFAULT '1',
  `open` tinyint(1) DEFAULT NULL,
  `day` varchar(100) DEFAULT 'monday',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aobh_businesshours` */

/*Table structure for table `aod_index` */

DROP TABLE IF EXISTS `aod_index`;

CREATE TABLE `aod_index` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `last_optimised` datetime DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aod_index` */

insert  into `aod_index`(`id`,`name`,`date_entered`,`date_modified`,`modified_user_id`,`created_by`,`description`,`deleted`,`assigned_user_id`,`last_optimised`,`location`) values ('1','Index','2017-06-17 21:37:28','2017-06-17 21:37:28','1','1',NULL,0,NULL,NULL,'modules/AOD_Index/Index/Index');

/*Table structure for table `aod_index_audit` */

DROP TABLE IF EXISTS `aod_index_audit`;

CREATE TABLE `aod_index_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aod_index_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aod_index_audit` */

/*Table structure for table `aod_indexevent` */

DROP TABLE IF EXISTS `aod_indexevent`;

CREATE TABLE `aod_indexevent` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `error` varchar(255) DEFAULT NULL,
  `success` tinyint(1) DEFAULT '0',
  `record_id` char(36) DEFAULT NULL,
  `record_module` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_record_module` (`record_module`),
  KEY `idx_record_id` (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aod_indexevent` */

insert  into `aod_indexevent`(`id`,`name`,`date_entered`,`date_modified`,`modified_user_id`,`created_by`,`description`,`deleted`,`assigned_user_id`,`error`,`success`,`record_id`,`record_module`) values ('90e1dba7-0c05-f03c-cd88-594866467a2f','Procedimeto 01','2017-06-20 00:04:08','2017-06-20 00:04:08','1','1',NULL,0,NULL,NULL,1,'ae006f80-86e8-89a4-550d-594866f314ff','PRO_PROCEDIMENTO'),('da01b856-f028-f4b2-9471-59486d589f8e','','2017-06-20 00:34:46','2017-06-20 00:34:46','1','1',NULL,0,NULL,NULL,1,'47a49096-8403-136c-19a6-59486d94a1eb','PME_PROCEDIMENTO_MEDICO');

/*Table structure for table `aod_indexevent_audit` */

DROP TABLE IF EXISTS `aod_indexevent_audit`;

CREATE TABLE `aod_indexevent_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aod_indexevent_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aod_indexevent_audit` */

/*Table structure for table `aok_knowledge_base_categories` */

DROP TABLE IF EXISTS `aok_knowledge_base_categories`;

CREATE TABLE `aok_knowledge_base_categories` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aok_knowledge_base_categories` */

/*Table structure for table `aok_knowledge_base_categories_audit` */

DROP TABLE IF EXISTS `aok_knowledge_base_categories_audit`;

CREATE TABLE `aok_knowledge_base_categories_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aok_knowledge_base_categories_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aok_knowledge_base_categories_audit` */

/*Table structure for table `aok_knowledgebase` */

DROP TABLE IF EXISTS `aok_knowledgebase`;

CREATE TABLE `aok_knowledgebase` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Draft',
  `revision` varchar(255) DEFAULT NULL,
  `additional_info` text,
  `user_id_c` char(36) DEFAULT NULL,
  `user_id1_c` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aok_knowledgebase` */

/*Table structure for table `aok_knowledgebase_audit` */

DROP TABLE IF EXISTS `aok_knowledgebase_audit`;

CREATE TABLE `aok_knowledgebase_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aok_knowledgebase_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aok_knowledgebase_audit` */

/*Table structure for table `aok_knowledgebase_categories` */

DROP TABLE IF EXISTS `aok_knowledgebase_categories`;

CREATE TABLE `aok_knowledgebase_categories` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aok_knowledgebase_id` varchar(36) DEFAULT NULL,
  `aok_knowledge_base_categories_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aok_knowledgebase_categories_alt` (`aok_knowledgebase_id`,`aok_knowledge_base_categories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aok_knowledgebase_categories` */

/*Table structure for table `aop_case_events` */

DROP TABLE IF EXISTS `aop_case_events`;

CREATE TABLE `aop_case_events` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `case_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aop_case_events` */

/*Table structure for table `aop_case_events_audit` */

DROP TABLE IF EXISTS `aop_case_events_audit`;

CREATE TABLE `aop_case_events_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aop_case_events_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aop_case_events_audit` */

/*Table structure for table `aop_case_updates` */

DROP TABLE IF EXISTS `aop_case_updates`;

CREATE TABLE `aop_case_updates` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `case_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `internal` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aop_case_updates` */

/*Table structure for table `aop_case_updates_audit` */

DROP TABLE IF EXISTS `aop_case_updates_audit`;

CREATE TABLE `aop_case_updates_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aop_case_updates_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aop_case_updates_audit` */

/*Table structure for table `aor_charts` */

DROP TABLE IF EXISTS `aor_charts`;

CREATE TABLE `aor_charts` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aor_report_id` char(36) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `x_field` int(11) DEFAULT NULL,
  `y_field` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aor_charts` */

/*Table structure for table `aor_conditions` */

DROP TABLE IF EXISTS `aor_conditions`;

CREATE TABLE `aor_conditions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aor_report_id` char(36) DEFAULT NULL,
  `condition_order` int(255) DEFAULT NULL,
  `logic_op` varchar(255) DEFAULT NULL,
  `parenthesis` varchar(255) DEFAULT NULL,
  `module_path` longtext,
  `field` varchar(100) DEFAULT NULL,
  `operator` varchar(100) DEFAULT NULL,
  `value_type` varchar(100) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `parameter` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aor_conditions_index_report_id` (`aor_report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aor_conditions` */

/*Table structure for table `aor_fields` */

DROP TABLE IF EXISTS `aor_fields`;

CREATE TABLE `aor_fields` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aor_report_id` char(36) DEFAULT NULL,
  `field_order` int(255) DEFAULT NULL,
  `module_path` longtext,
  `field` varchar(100) DEFAULT NULL,
  `display` tinyint(1) DEFAULT NULL,
  `link` tinyint(1) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `field_function` varchar(100) DEFAULT NULL,
  `sort_by` varchar(100) DEFAULT NULL,
  `format` varchar(100) DEFAULT NULL,
  `total` varchar(100) DEFAULT NULL,
  `sort_order` varchar(100) DEFAULT NULL,
  `group_by` tinyint(1) DEFAULT NULL,
  `group_order` varchar(100) DEFAULT NULL,
  `group_display` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aor_fields_index_report_id` (`aor_report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aor_fields` */

/*Table structure for table `aor_reports` */

DROP TABLE IF EXISTS `aor_reports`;

CREATE TABLE `aor_reports` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `report_module` varchar(100) DEFAULT NULL,
  `graphs_per_row` int(11) DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aor_reports` */

/*Table structure for table `aor_reports_audit` */

DROP TABLE IF EXISTS `aor_reports_audit`;

CREATE TABLE `aor_reports_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aor_reports_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aor_reports_audit` */

/*Table structure for table `aor_scheduled_reports` */

DROP TABLE IF EXISTS `aor_scheduled_reports`;

CREATE TABLE `aor_scheduled_reports` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `schedule` varchar(100) DEFAULT NULL,
  `last_run` datetime DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `email_recipients` longtext,
  `aor_report_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aor_scheduled_reports` */

/*Table structure for table `aos_contracts` */

DROP TABLE IF EXISTS `aos_contracts`;

CREATE TABLE `aos_contracts` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `reference_code` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `total_contract_value` decimal(26,6) DEFAULT NULL,
  `total_contract_value_usdollar` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Not Started',
  `customer_signed_date` date DEFAULT NULL,
  `company_signed_date` date DEFAULT NULL,
  `renewal_reminder_date` datetime DEFAULT NULL,
  `contract_type` varchar(100) DEFAULT 'Type',
  `contract_account_id` char(36) DEFAULT NULL,
  `opportunity_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `call_id` char(36) DEFAULT NULL,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `total_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `discount_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_amount` decimal(26,6) DEFAULT NULL,
  `shipping_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_tax` varchar(100) DEFAULT NULL,
  `shipping_tax_amt` decimal(26,6) DEFAULT NULL,
  `shipping_tax_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  `total_amount_usdollar` decimal(26,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_contracts` */

/*Table structure for table `aos_contracts_audit` */

DROP TABLE IF EXISTS `aos_contracts_audit`;

CREATE TABLE `aos_contracts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_contracts_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_contracts_audit` */

/*Table structure for table `aos_contracts_documents` */

DROP TABLE IF EXISTS `aos_contracts_documents`;

CREATE TABLE `aos_contracts_documents` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_contracts_id` varchar(36) DEFAULT NULL,
  `documents_id` varchar(36) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_contracts_documents_alt` (`aos_contracts_id`,`documents_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_contracts_documents` */

/*Table structure for table `aos_invoices` */

DROP TABLE IF EXISTS `aos_invoices`;

CREATE TABLE `aos_invoices` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `billing_account_id` char(36) DEFAULT NULL,
  `billing_contact_id` char(36) DEFAULT NULL,
  `billing_address_street` varchar(150) DEFAULT NULL,
  `billing_address_city` varchar(100) DEFAULT NULL,
  `billing_address_state` varchar(100) DEFAULT NULL,
  `billing_address_postalcode` varchar(20) DEFAULT NULL,
  `billing_address_country` varchar(255) DEFAULT NULL,
  `shipping_address_street` varchar(150) DEFAULT NULL,
  `shipping_address_city` varchar(100) DEFAULT NULL,
  `shipping_address_state` varchar(100) DEFAULT NULL,
  `shipping_address_postalcode` varchar(20) DEFAULT NULL,
  `shipping_address_country` varchar(255) DEFAULT NULL,
  `number` int(11) NOT NULL,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `total_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `discount_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_amount` decimal(26,6) DEFAULT NULL,
  `shipping_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_tax` varchar(100) DEFAULT NULL,
  `shipping_tax_amt` decimal(26,6) DEFAULT NULL,
  `shipping_tax_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  `total_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `quote_number` int(11) DEFAULT NULL,
  `quote_date` date DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `template_ddown_c` text,
  `subtotal_tax_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_invoices` */

/*Table structure for table `aos_invoices_audit` */

DROP TABLE IF EXISTS `aos_invoices_audit`;

CREATE TABLE `aos_invoices_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_invoices_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_invoices_audit` */

/*Table structure for table `aos_line_item_groups` */

DROP TABLE IF EXISTS `aos_line_item_groups`;

CREATE TABLE `aos_line_item_groups` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `total_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `discount_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `subtotal_tax_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  `total_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_line_item_groups` */

/*Table structure for table `aos_line_item_groups_audit` */

DROP TABLE IF EXISTS `aos_line_item_groups_audit`;

CREATE TABLE `aos_line_item_groups_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_line_item_groups_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_line_item_groups_audit` */

/*Table structure for table `aos_pdf_templates` */

DROP TABLE IF EXISTS `aos_pdf_templates`;

CREATE TABLE `aos_pdf_templates` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `type` varchar(100) DEFAULT NULL,
  `pdfheader` text,
  `pdffooter` text,
  `margin_left` int(255) DEFAULT '15',
  `margin_right` int(255) DEFAULT '15',
  `margin_top` int(255) DEFAULT '16',
  `margin_bottom` int(255) DEFAULT '16',
  `margin_header` int(255) DEFAULT '9',
  `margin_footer` int(255) DEFAULT '9',
  `page_size` varchar(100) DEFAULT NULL,
  `orientation` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_pdf_templates` */

/*Table structure for table `aos_pdf_templates_audit` */

DROP TABLE IF EXISTS `aos_pdf_templates_audit`;

CREATE TABLE `aos_pdf_templates_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_pdf_templates_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_pdf_templates_audit` */

/*Table structure for table `aos_product_categories` */

DROP TABLE IF EXISTS `aos_product_categories`;

CREATE TABLE `aos_product_categories` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `is_parent` tinyint(1) DEFAULT '0',
  `parent_category_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_product_categories` */

/*Table structure for table `aos_product_categories_audit` */

DROP TABLE IF EXISTS `aos_product_categories_audit`;

CREATE TABLE `aos_product_categories_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_product_categories_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_product_categories_audit` */

/*Table structure for table `aos_products` */

DROP TABLE IF EXISTS `aos_products`;

CREATE TABLE `aos_products` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `maincode` varchar(100) DEFAULT 'XXXX',
  `part_number` varchar(25) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT 'Good',
  `cost` decimal(26,6) DEFAULT NULL,
  `cost_usdollar` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `price` decimal(26,6) DEFAULT NULL,
  `price_usdollar` decimal(26,6) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `product_image` varchar(255) DEFAULT NULL,
  `aos_product_category_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_products` */

/*Table structure for table `aos_products_audit` */

DROP TABLE IF EXISTS `aos_products_audit`;

CREATE TABLE `aos_products_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_products_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_products_audit` */

/*Table structure for table `aos_products_quotes` */

DROP TABLE IF EXISTS `aos_products_quotes`;

CREATE TABLE `aos_products_quotes` (
  `id` char(36) NOT NULL,
  `name` text,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `part_number` varchar(255) DEFAULT NULL,
  `item_description` text,
  `number` int(11) DEFAULT NULL,
  `product_qty` decimal(18,4) DEFAULT NULL,
  `product_cost_price` decimal(26,6) DEFAULT NULL,
  `product_cost_price_usdollar` decimal(26,6) DEFAULT NULL,
  `product_list_price` decimal(26,6) DEFAULT NULL,
  `product_list_price_usdollar` decimal(26,6) DEFAULT NULL,
  `product_discount` decimal(26,6) DEFAULT NULL,
  `product_discount_usdollar` decimal(26,6) DEFAULT NULL,
  `product_discount_amount` decimal(26,6) DEFAULT NULL,
  `product_discount_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `discount` varchar(255) DEFAULT 'Percentage',
  `product_unit_price` decimal(26,6) DEFAULT NULL,
  `product_unit_price_usdollar` decimal(26,6) DEFAULT NULL,
  `vat_amt` decimal(26,6) DEFAULT NULL,
  `vat_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `product_total_price` decimal(26,6) DEFAULT NULL,
  `product_total_price_usdollar` decimal(26,6) DEFAULT NULL,
  `vat` varchar(100) DEFAULT '5.0',
  `parent_type` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `product_id` char(36) DEFAULT NULL,
  `group_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aospq_par_del` (`parent_id`,`parent_type`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_products_quotes` */

/*Table structure for table `aos_products_quotes_audit` */

DROP TABLE IF EXISTS `aos_products_quotes_audit`;

CREATE TABLE `aos_products_quotes_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_products_quotes_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_products_quotes_audit` */

/*Table structure for table `aos_quotes` */

DROP TABLE IF EXISTS `aos_quotes`;

CREATE TABLE `aos_quotes` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `approval_issue` text,
  `billing_account_id` char(36) DEFAULT NULL,
  `billing_contact_id` char(36) DEFAULT NULL,
  `billing_address_street` varchar(150) DEFAULT NULL,
  `billing_address_city` varchar(100) DEFAULT NULL,
  `billing_address_state` varchar(100) DEFAULT NULL,
  `billing_address_postalcode` varchar(20) DEFAULT NULL,
  `billing_address_country` varchar(255) DEFAULT NULL,
  `shipping_address_street` varchar(150) DEFAULT NULL,
  `shipping_address_city` varchar(100) DEFAULT NULL,
  `shipping_address_state` varchar(100) DEFAULT NULL,
  `shipping_address_postalcode` varchar(20) DEFAULT NULL,
  `shipping_address_country` varchar(255) DEFAULT NULL,
  `expiration` date DEFAULT NULL,
  `number` int(11) NOT NULL,
  `opportunity_id` char(36) DEFAULT NULL,
  `template_ddown_c` text,
  `total_amt` decimal(26,6) DEFAULT NULL,
  `total_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `subtotal_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `discount_amount` decimal(26,6) DEFAULT NULL,
  `discount_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `tax_amount` decimal(26,6) DEFAULT NULL,
  `tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_amount` decimal(26,6) DEFAULT NULL,
  `shipping_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `shipping_tax` varchar(100) DEFAULT NULL,
  `shipping_tax_amt` decimal(26,6) DEFAULT NULL,
  `shipping_tax_amt_usdollar` decimal(26,6) DEFAULT NULL,
  `total_amount` decimal(26,6) DEFAULT NULL,
  `total_amount_usdollar` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `stage` varchar(100) DEFAULT 'Draft',
  `term` varchar(100) DEFAULT NULL,
  `terms_c` text,
  `approval_status` varchar(100) DEFAULT NULL,
  `invoice_status` varchar(100) DEFAULT 'Not Invoiced',
  `subtotal_tax_amount` decimal(26,6) DEFAULT NULL,
  `subtotal_tax_amount_usdollar` decimal(26,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_quotes` */

/*Table structure for table `aos_quotes_aos_invoices_c` */

DROP TABLE IF EXISTS `aos_quotes_aos_invoices_c`;

CREATE TABLE `aos_quotes_aos_invoices_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_quotes77d9_quotes_ida` varchar(36) DEFAULT NULL,
  `aos_quotes6b83nvoices_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_quotes_aos_invoices_alt` (`aos_quotes77d9_quotes_ida`,`aos_quotes6b83nvoices_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_quotes_aos_invoices_c` */

/*Table structure for table `aos_quotes_audit` */

DROP TABLE IF EXISTS `aos_quotes_audit`;

CREATE TABLE `aos_quotes_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aos_quotes_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_quotes_audit` */

/*Table structure for table `aos_quotes_os_contracts_c` */

DROP TABLE IF EXISTS `aos_quotes_os_contracts_c`;

CREATE TABLE `aos_quotes_os_contracts_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_quotese81e_quotes_ida` varchar(36) DEFAULT NULL,
  `aos_quotes4dc0ntracts_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_quotes_aos_contracts_alt` (`aos_quotese81e_quotes_ida`,`aos_quotes4dc0ntracts_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_quotes_os_contracts_c` */

/*Table structure for table `aos_quotes_project_c` */

DROP TABLE IF EXISTS `aos_quotes_project_c`;

CREATE TABLE `aos_quotes_project_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `aos_quotes1112_quotes_ida` varchar(36) DEFAULT NULL,
  `aos_quotes7207project_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aos_quotes_project_alt` (`aos_quotes1112_quotes_ida`,`aos_quotes7207project_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aos_quotes_project_c` */

/*Table structure for table `aow_actions` */

DROP TABLE IF EXISTS `aow_actions`;

CREATE TABLE `aow_actions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aow_workflow_id` char(36) DEFAULT NULL,
  `action_order` int(255) DEFAULT NULL,
  `action` varchar(100) DEFAULT NULL,
  `parameters` longtext,
  PRIMARY KEY (`id`),
  KEY `aow_action_index_workflow_id` (`aow_workflow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aow_actions` */

/*Table structure for table `aow_conditions` */

DROP TABLE IF EXISTS `aow_conditions`;

CREATE TABLE `aow_conditions` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aow_workflow_id` char(36) DEFAULT NULL,
  `condition_order` int(255) DEFAULT NULL,
  `module_path` longtext,
  `field` varchar(100) DEFAULT NULL,
  `operator` varchar(100) DEFAULT NULL,
  `value_type` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aow_conditions_index_workflow_id` (`aow_workflow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aow_conditions` */

/*Table structure for table `aow_processed` */

DROP TABLE IF EXISTS `aow_processed`;

CREATE TABLE `aow_processed` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `aow_workflow_id` char(36) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Pending',
  PRIMARY KEY (`id`),
  KEY `aow_processed_index_workflow` (`aow_workflow_id`,`status`,`parent_id`,`deleted`),
  KEY `aow_processed_index_status` (`status`),
  KEY `aow_processed_index_workflow_id` (`aow_workflow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aow_processed` */

/*Table structure for table `aow_processed_aow_actions` */

DROP TABLE IF EXISTS `aow_processed_aow_actions`;

CREATE TABLE `aow_processed_aow_actions` (
  `id` varchar(36) NOT NULL,
  `aow_processed_id` varchar(36) DEFAULT NULL,
  `aow_action_id` varchar(36) DEFAULT NULL,
  `status` varchar(36) DEFAULT 'Pending',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_aow_processed_aow_actions` (`aow_processed_id`,`aow_action_id`),
  KEY `idx_actid_del_freid` (`aow_action_id`,`deleted`,`aow_processed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aow_processed_aow_actions` */

/*Table structure for table `aow_workflow` */

DROP TABLE IF EXISTS `aow_workflow`;

CREATE TABLE `aow_workflow` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `flow_module` varchar(100) DEFAULT NULL,
  `flow_run_on` varchar(100) DEFAULT '0',
  `status` varchar(100) DEFAULT 'Active',
  `run_when` varchar(100) DEFAULT 'Always',
  `multiple_runs` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `aow_workflow_index_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aow_workflow` */

/*Table structure for table `aow_workflow_audit` */

DROP TABLE IF EXISTS `aow_workflow_audit`;

CREATE TABLE `aow_workflow_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_aow_workflow_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `aow_workflow_audit` */

/*Table structure for table `bugs` */

DROP TABLE IF EXISTS `bugs`;

CREATE TABLE `bugs` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `bug_number` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  `resolution` varchar(255) DEFAULT NULL,
  `work_log` text,
  `found_in_release` varchar(255) DEFAULT NULL,
  `fixed_in_release` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `product_category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bugsnumk` (`bug_number`),
  KEY `bug_number` (`bug_number`),
  KEY `idx_bug_name` (`name`),
  KEY `idx_bugs_assigned_user` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `bugs` */

/*Table structure for table `bugs_audit` */

DROP TABLE IF EXISTS `bugs_audit`;

CREATE TABLE `bugs_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_bugs_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `bugs_audit` */

/*Table structure for table `calls` */

DROP TABLE IF EXISTS `calls`;

CREATE TABLE `calls` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `duration_hours` int(2) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Planned',
  `direction` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `reminder_time` int(11) DEFAULT '-1',
  `email_reminder_time` int(11) DEFAULT '-1',
  `email_reminder_sent` tinyint(1) DEFAULT '0',
  `outlook_id` varchar(255) DEFAULT NULL,
  `repeat_type` varchar(36) DEFAULT NULL,
  `repeat_interval` int(3) DEFAULT '1',
  `repeat_dow` varchar(7) DEFAULT NULL,
  `repeat_until` date DEFAULT NULL,
  `repeat_count` int(7) DEFAULT NULL,
  `repeat_parent_id` char(36) DEFAULT NULL,
  `recurring_source` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_call_name` (`name`),
  KEY `idx_status` (`status`),
  KEY `idx_calls_date_start` (`date_start`),
  KEY `idx_calls_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_calls_assigned_del` (`deleted`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `calls` */

/*Table structure for table `calls_contacts` */

DROP TABLE IF EXISTS `calls_contacts`;

CREATE TABLE `calls_contacts` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_call_call` (`call_id`),
  KEY `idx_con_call_con` (`contact_id`),
  KEY `idx_call_contact` (`call_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `calls_contacts` */

/*Table structure for table `calls_leads` */

DROP TABLE IF EXISTS `calls_leads`;

CREATE TABLE `calls_leads` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `lead_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_lead_call_call` (`call_id`),
  KEY `idx_lead_call_lead` (`lead_id`),
  KEY `idx_call_lead` (`call_id`,`lead_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `calls_leads` */

/*Table structure for table `calls_reschedule` */

DROP TABLE IF EXISTS `calls_reschedule`;

CREATE TABLE `calls_reschedule` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `reason` varchar(100) DEFAULT NULL,
  `call_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `calls_reschedule` */

/*Table structure for table `calls_reschedule_audit` */

DROP TABLE IF EXISTS `calls_reschedule_audit`;

CREATE TABLE `calls_reschedule_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_calls_reschedule_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `calls_reschedule_audit` */

/*Table structure for table `calls_users` */

DROP TABLE IF EXISTS `calls_users`;

CREATE TABLE `calls_users` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_usr_call_call` (`call_id`),
  KEY `idx_usr_call_usr` (`user_id`),
  KEY `idx_call_users` (`call_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `calls_users` */

/*Table structure for table `campaign_log` */

DROP TABLE IF EXISTS `campaign_log`;

CREATE TABLE `campaign_log` (
  `id` char(36) NOT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `target_tracker_key` varchar(36) DEFAULT NULL,
  `target_id` varchar(36) DEFAULT NULL,
  `target_type` varchar(100) DEFAULT NULL,
  `activity_type` varchar(100) DEFAULT NULL,
  `activity_date` datetime DEFAULT NULL,
  `related_id` varchar(36) DEFAULT NULL,
  `related_type` varchar(100) DEFAULT NULL,
  `archived` tinyint(1) DEFAULT '0',
  `hits` int(11) DEFAULT '0',
  `list_id` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `more_information` varchar(100) DEFAULT NULL,
  `marketing_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_camp_tracker` (`target_tracker_key`),
  KEY `idx_camp_campaign_id` (`campaign_id`),
  KEY `idx_camp_more_info` (`more_information`),
  KEY `idx_target_id` (`target_id`),
  KEY `idx_target_id_deleted` (`target_id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `campaign_log` */

/*Table structure for table `campaign_trkrs` */

DROP TABLE IF EXISTS `campaign_trkrs`;

CREATE TABLE `campaign_trkrs` (
  `id` char(36) NOT NULL,
  `tracker_name` varchar(30) DEFAULT NULL,
  `tracker_url` varchar(255) DEFAULT 'http://',
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` char(36) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `is_optout` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `campaign_tracker_key_idx` (`tracker_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `campaign_trkrs` */

/*Table structure for table `campaigns` */

DROP TABLE IF EXISTS `campaigns`;

CREATE TABLE `campaigns` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `tracker_count` int(11) DEFAULT '0',
  `refer_url` varchar(255) DEFAULT 'http://',
  `tracker_text` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `impressions` int(11) DEFAULT '0',
  `currency_id` char(36) DEFAULT NULL,
  `budget` double DEFAULT NULL,
  `expected_cost` double DEFAULT NULL,
  `actual_cost` double DEFAULT NULL,
  `expected_revenue` double DEFAULT NULL,
  `campaign_type` varchar(100) DEFAULT NULL,
  `objective` text,
  `content` text,
  `frequency` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `camp_auto_tracker_key` (`tracker_key`),
  KEY `idx_campaign_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `campaigns` */

/*Table structure for table `campaigns_audit` */

DROP TABLE IF EXISTS `campaigns_audit`;

CREATE TABLE `campaigns_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_campaigns_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `campaigns_audit` */

/*Table structure for table `cases` */

DROP TABLE IF EXISTS `cases`;

CREATE TABLE `cases` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `case_number` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  `resolution` text,
  `work_log` text,
  `account_id` char(36) DEFAULT NULL,
  `state` varchar(100) DEFAULT 'Open',
  `contact_created_by_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `casesnumk` (`case_number`),
  KEY `case_number` (`case_number`),
  KEY `idx_case_name` (`name`),
  KEY `idx_account_id` (`account_id`),
  KEY `idx_cases_stat_del` (`assigned_user_id`,`status`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cases` */

/*Table structure for table `cases_audit` */

DROP TABLE IF EXISTS `cases_audit`;

CREATE TABLE `cases_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_cases_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cases_audit` */

/*Table structure for table `cases_bugs` */

DROP TABLE IF EXISTS `cases_bugs`;

CREATE TABLE `cases_bugs` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_cas_bug_cas` (`case_id`),
  KEY `idx_cas_bug_bug` (`bug_id`),
  KEY `idx_case_bug` (`case_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cases_bugs` */

/*Table structure for table `cases_cstm` */

DROP TABLE IF EXISTS `cases_cstm`;

CREATE TABLE `cases_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cases_cstm` */

/*Table structure for table `com_competidores` */

DROP TABLE IF EXISTS `com_competidores`;

CREATE TABLE `com_competidores` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `com_competidores` */

/*Table structure for table `com_competidores_audit` */

DROP TABLE IF EXISTS `com_competidores_audit`;

CREATE TABLE `com_competidores_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_com_competidores_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `com_competidores_audit` */

/*Table structure for table `com_competidores_cstm` */

DROP TABLE IF EXISTS `com_competidores_cstm`;

CREATE TABLE `com_competidores_cstm` (
  `id_c` char(36) NOT NULL,
  `percentual_c` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `com_competidores_cstm` */

/*Table structure for table `con_consultorios` */

DROP TABLE IF EXISTS `con_consultorios`;

CREATE TABLE `con_consultorios` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `con_consultorios` */

/*Table structure for table `con_consultorios_audit` */

DROP TABLE IF EXISTS `con_consultorios_audit`;

CREATE TABLE `con_consultorios_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_con_consultorios_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `con_consultorios_audit` */

/*Table structure for table `con_consultorios_cstm` */

DROP TABLE IF EXISTS `con_consultorios_cstm`;

CREATE TABLE `con_consultorios_cstm` (
  `id_c` char(36) NOT NULL,
  `end_cons_c` varchar(255) DEFAULT NULL,
  `cid_cons_c` varchar(255) DEFAULT NULL,
  `cep_cons_c` varchar(255) DEFAULT NULL,
  `est_cons_c` varchar(100) DEFAULT NULL,
  `dia_seg_c` tinyint(1) DEFAULT '0',
  `hora_seg_c` varchar(255) DEFAULT NULL,
  `dia_ter_c` tinyint(1) DEFAULT '0',
  `dia_qua_c` tinyint(1) DEFAULT '0',
  `dia_qui_c` tinyint(1) DEFAULT '0',
  `dia_sex_c` tinyint(1) DEFAULT '0',
  `hora_ter_c` varchar(255) DEFAULT NULL,
  `hora_qua_c` varchar(255) DEFAULT NULL,
  `hora_qui_c` varchar(255) DEFAULT NULL,
  `hora_sex_c` varchar(255) DEFAULT NULL,
  `nome_sec_c` varchar(255) DEFAULT NULL,
  `email_sec_c` varchar(255) DEFAULT NULL,
  `aniv_sec_c` date DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `con_consultorios_cstm` */

/*Table structure for table `config` */

DROP TABLE IF EXISTS `config`;

CREATE TABLE `config` (
  `category` varchar(32) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `value` text,
  KEY `idx_config_cat` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `config` */

insert  into `config`(`category`,`name`,`value`) values ('notify','fromaddress','do_not_reply@example.com'),('notify','fromname','SuiteCRM'),('notify','send_by_default','1'),('notify','on','1'),('notify','send_from_assigning_user','0'),('info','sugar_version','6.5.24'),('MySettings','tab','YTo0MTp7czo0OiJIb21lIjtzOjQ6IkhvbWUiO3M6ODoiQWNjb3VudHMiO3M6ODoiQWNjb3VudHMiO3M6ODoiQ29udGFjdHMiO3M6ODoiQ29udGFjdHMiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6NToiTGVhZHMiO3M6NToiTGVhZHMiO3M6MTA6IkFPU19RdW90ZXMiO3M6MTA6IkFPU19RdW90ZXMiO3M6ODoiQ2FsZW5kYXIiO3M6ODoiQ2FsZW5kYXIiO3M6OToiRG9jdW1lbnRzIjtzOjk6IkRvY3VtZW50cyI7czo2OiJFbWFpbHMiO3M6NjoiRW1haWxzIjtzOjU6IlNwb3RzIjtzOjU6IlNwb3RzIjtzOjk6IkNhbXBhaWducyI7czo5OiJDYW1wYWlnbnMiO3M6NToiQ2FsbHMiO3M6NToiQ2FsbHMiO3M6ODoiTWVldGluZ3MiO3M6ODoiTWVldGluZ3MiO3M6NToiVGFza3MiO3M6NToiVGFza3MiO3M6NToiTm90ZXMiO3M6NToiTm90ZXMiO3M6MTI6IkFPU19JbnZvaWNlcyI7czoxMjoiQU9TX0ludm9pY2VzIjtzOjEzOiJBT1NfQ29udHJhY3RzIjtzOjEzOiJBT1NfQ29udHJhY3RzIjtzOjU6IkNhc2VzIjtzOjU6IkNhc2VzIjtzOjk6IlByb3NwZWN0cyI7czo5OiJQcm9zcGVjdHMiO3M6MTM6IlByb3NwZWN0TGlzdHMiO3M6MTM6IlByb3NwZWN0TGlzdHMiO3M6NzoiUHJvamVjdCI7czo3OiJQcm9qZWN0IjtzOjE5OiJBTV9Qcm9qZWN0VGVtcGxhdGVzIjtzOjE5OiJBTV9Qcm9qZWN0VGVtcGxhdGVzIjtzOjk6IkZQX2V2ZW50cyI7czo5OiJGUF9ldmVudHMiO3M6MTg6IkZQX0V2ZW50X0xvY2F0aW9ucyI7czoxODoiRlBfRXZlbnRfTG9jYXRpb25zIjtzOjEyOiJBT1NfUHJvZHVjdHMiO3M6MTI6IkFPU19Qcm9kdWN0cyI7czoyMjoiQU9TX1Byb2R1Y3RfQ2F0ZWdvcmllcyI7czoyMjoiQU9TX1Byb2R1Y3RfQ2F0ZWdvcmllcyI7czoxNzoiQU9TX1BERl9UZW1wbGF0ZXMiO3M6MTc6IkFPU19QREZfVGVtcGxhdGVzIjtzOjk6Impqd2dfTWFwcyI7czo5OiJqandnX01hcHMiO3M6MTI6Impqd2dfTWFya2VycyI7czoxMjoiamp3Z19NYXJrZXJzIjtzOjEwOiJqandnX0FyZWFzIjtzOjEwOiJqandnX0FyZWFzIjtzOjE4OiJqandnX0FkZHJlc3NfQ2FjaGUiO3M6MTg6Impqd2dfQWRkcmVzc19DYWNoZSI7czoxMToiQU9SX1JlcG9ydHMiO3M6MTE6IkFPUl9SZXBvcnRzIjtzOjEyOiJBT1dfV29ya0Zsb3ciO3M6MTI6IkFPV19Xb3JrRmxvdyI7czoxNzoiQU9LX0tub3dsZWRnZUJhc2UiO3M6MTc6IkFPS19Lbm93bGVkZ2VCYXNlIjtzOjI5OiJBT0tfS25vd2xlZGdlX0Jhc2VfQ2F0ZWdvcmllcyI7czoyOToiQU9LX0tub3dsZWRnZV9CYXNlX0NhdGVnb3JpZXMiO3M6MTY6IkNPTV9DT01QRVRJRE9SRVMiO3M6MTY6IkNPTV9DT01QRVRJRE9SRVMiO3M6MTY6IkNPTl9DT05TVUxUT1JJT1MiO3M6MTY6IkNPTl9DT05TVUxUT1JJT1MiO3M6MTA6IlZJU19WSVNJVEEiO3M6MTA6IlZJU19WSVNJVEEiO3M6MTY6IlBST19QUk9DRURJTUVOVE8iO3M6MTY6IlBST19QUk9DRURJTUVOVE8iO3M6MTI6IkhPU19IT1NQSVRBTCI7czoxMjoiSE9TX0hPU1BJVEFMIjtzOjIzOiJQTUVfUFJPQ0VESU1FTlRPX01FRElDTyI7czoyMzoiUE1FX1BST0NFRElNRU5UT19NRURJQ08iO30='),('portal','on','0'),('tracker','Tracker','1'),('system','skypeout_on','1'),('sugarfeed','enabled','1'),('sugarfeed','module_UserFeed','1'),('sugarfeed','module_Cases','1'),('sugarfeed','module_Contacts','1'),('sugarfeed','module_Leads','1'),('sugarfeed','module_Opportunities','1'),('Update','CheckUpdates','manual'),('system','name','SuiteCRM'),('system','adminwizard','1'),('notify','allow_default_outbound','0');

/*Table structure for table `contacts` */

DROP TABLE IF EXISTS `contacts`;

CREATE TABLE `contacts` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `lead_source` varchar(255) DEFAULT NULL,
  `reports_to_id` char(36) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `joomla_account_id` varchar(255) DEFAULT NULL,
  `portal_account_disabled` tinyint(1) DEFAULT NULL,
  `portal_user_type` varchar(100) DEFAULT 'Single',
  PRIMARY KEY (`id`),
  KEY `idx_cont_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_contacts_del_last` (`deleted`,`last_name`),
  KEY `idx_cont_del_reports` (`deleted`,`reports_to_id`,`last_name`),
  KEY `idx_reports_to_id` (`reports_to_id`),
  KEY `idx_del_id_user` (`deleted`,`id`,`assigned_user_id`),
  KEY `idx_cont_assigned` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `contacts` */

/*Table structure for table `contacts_audit` */

DROP TABLE IF EXISTS `contacts_audit`;

CREATE TABLE `contacts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_contacts_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `contacts_audit` */

/*Table structure for table `contacts_bugs` */

DROP TABLE IF EXISTS `contacts_bugs`;

CREATE TABLE `contacts_bugs` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_bug_con` (`contact_id`),
  KEY `idx_con_bug_bug` (`bug_id`),
  KEY `idx_contact_bug` (`contact_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `contacts_bugs` */

/*Table structure for table `contacts_cases` */

DROP TABLE IF EXISTS `contacts_cases`;

CREATE TABLE `contacts_cases` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_case_con` (`contact_id`),
  KEY `idx_con_case_case` (`case_id`),
  KEY `idx_contacts_cases` (`contact_id`,`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `contacts_cases` */

/*Table structure for table `contacts_cstm` */

DROP TABLE IF EXISTS `contacts_cstm`;

CREATE TABLE `contacts_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `contacts_cstm` */

/*Table structure for table `contacts_users` */

DROP TABLE IF EXISTS `contacts_users`;

CREATE TABLE `contacts_users` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_users_con` (`contact_id`),
  KEY `idx_con_users_user` (`user_id`),
  KEY `idx_contacts_users` (`contact_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `contacts_users` */

/*Table structure for table `cron_remove_documents` */

DROP TABLE IF EXISTS `cron_remove_documents`;

CREATE TABLE `cron_remove_documents` (
  `id` varchar(36) NOT NULL,
  `bean_id` varchar(36) DEFAULT NULL,
  `module` varchar(25) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cron_remove_document_bean_id` (`bean_id`),
  KEY `idx_cron_remove_document_stamp` (`date_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cron_remove_documents` */

/*Table structure for table `currencies` */

DROP TABLE IF EXISTS `currencies`;

CREATE TABLE `currencies` (
  `id` char(36) NOT NULL,
  `name` varchar(36) DEFAULT NULL,
  `symbol` varchar(36) DEFAULT NULL,
  `iso4217` varchar(3) DEFAULT NULL,
  `conversion_rate` double DEFAULT '0',
  `status` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_currency_name` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `currencies` */

/*Table structure for table `custom_fields` */

DROP TABLE IF EXISTS `custom_fields`;

CREATE TABLE `custom_fields` (
  `bean_id` varchar(36) DEFAULT NULL,
  `set_num` int(11) DEFAULT '0',
  `field0` varchar(255) DEFAULT NULL,
  `field1` varchar(255) DEFAULT NULL,
  `field2` varchar(255) DEFAULT NULL,
  `field3` varchar(255) DEFAULT NULL,
  `field4` varchar(255) DEFAULT NULL,
  `field5` varchar(255) DEFAULT NULL,
  `field6` varchar(255) DEFAULT NULL,
  `field7` varchar(255) DEFAULT NULL,
  `field8` varchar(255) DEFAULT NULL,
  `field9` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  KEY `idx_beanid_set_num` (`bean_id`,`set_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `custom_fields` */

/*Table structure for table `document_revisions` */

DROP TABLE IF EXISTS `document_revisions`;

CREATE TABLE `document_revisions` (
  `id` varchar(36) NOT NULL,
  `change_log` varchar(255) DEFAULT NULL,
  `document_id` varchar(36) DEFAULT NULL,
  `doc_id` varchar(100) DEFAULT NULL,
  `doc_type` varchar(100) DEFAULT NULL,
  `doc_url` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `file_ext` varchar(100) DEFAULT NULL,
  `file_mime_type` varchar(100) DEFAULT NULL,
  `revision` varchar(100) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documentrevision_mimetype` (`file_mime_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `document_revisions` */

/*Table structure for table `documents` */

DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `document_name` varchar(255) DEFAULT NULL,
  `doc_id` varchar(100) DEFAULT NULL,
  `doc_type` varchar(100) DEFAULT 'Sugar',
  `doc_url` varchar(255) DEFAULT NULL,
  `active_date` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `category_id` varchar(100) DEFAULT NULL,
  `subcategory_id` varchar(100) DEFAULT NULL,
  `status_id` varchar(100) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  `related_doc_id` char(36) DEFAULT NULL,
  `related_doc_rev_id` char(36) DEFAULT NULL,
  `is_template` tinyint(1) DEFAULT '0',
  `template_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_doc_cat` (`category_id`,`subcategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `documents` */

/*Table structure for table `documents_accounts` */

DROP TABLE IF EXISTS `documents_accounts`;

CREATE TABLE `documents_accounts` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_accounts_account_id` (`account_id`,`document_id`),
  KEY `documents_accounts_document_id` (`document_id`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `documents_accounts` */

/*Table structure for table `documents_bugs` */

DROP TABLE IF EXISTS `documents_bugs`;

CREATE TABLE `documents_bugs` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_bugs_bug_id` (`bug_id`,`document_id`),
  KEY `documents_bugs_document_id` (`document_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `documents_bugs` */

/*Table structure for table `documents_cases` */

DROP TABLE IF EXISTS `documents_cases`;

CREATE TABLE `documents_cases` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_cases_case_id` (`case_id`,`document_id`),
  KEY `documents_cases_document_id` (`document_id`,`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `documents_cases` */

/*Table structure for table `documents_contacts` */

DROP TABLE IF EXISTS `documents_contacts`;

CREATE TABLE `documents_contacts` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_contacts_contact_id` (`contact_id`,`document_id`),
  KEY `documents_contacts_document_id` (`document_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `documents_contacts` */

/*Table structure for table `documents_opportunities` */

DROP TABLE IF EXISTS `documents_opportunities`;

CREATE TABLE `documents_opportunities` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `document_id` varchar(36) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_docu_opps_oppo_id` (`opportunity_id`,`document_id`),
  KEY `idx_docu_oppo_docu_id` (`document_id`,`opportunity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `documents_opportunities` */

/*Table structure for table `eapm` */

DROP TABLE IF EXISTS `eapm`;

CREATE TABLE `eapm` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `application` varchar(100) DEFAULT 'webex',
  `api_data` text,
  `consumer_key` varchar(255) DEFAULT NULL,
  `consumer_secret` varchar(255) DEFAULT NULL,
  `oauth_token` varchar(255) DEFAULT NULL,
  `oauth_secret` varchar(255) DEFAULT NULL,
  `validated` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_app_active` (`assigned_user_id`,`application`,`validated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `eapm` */

/*Table structure for table `email_addr_bean_rel` */

DROP TABLE IF EXISTS `email_addr_bean_rel`;

CREATE TABLE `email_addr_bean_rel` (
  `id` char(36) NOT NULL,
  `email_address_id` char(36) NOT NULL,
  `bean_id` char(36) NOT NULL,
  `bean_module` varchar(100) DEFAULT NULL,
  `primary_address` tinyint(1) DEFAULT '0',
  `reply_to_address` tinyint(1) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_email_address_id` (`email_address_id`),
  KEY `idx_bean_id` (`bean_id`,`bean_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `email_addr_bean_rel` */

insert  into `email_addr_bean_rel`(`id`,`email_address_id`,`bean_id`,`bean_module`,`primary_address`,`reply_to_address`,`date_created`,`date_modified`,`deleted`) values ('5f79a328-73e1-451c-d7f5-5945a0d118c8','5f99ea0b-fdc7-f8d5-205d-5945a056a087','1','Users',1,0,'2017-06-17 21:31:32','2017-06-17 21:31:32',0);

/*Table structure for table `email_addresses` */

DROP TABLE IF EXISTS `email_addresses`;

CREATE TABLE `email_addresses` (
  `id` char(36) NOT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `email_address_caps` varchar(255) DEFAULT NULL,
  `invalid_email` tinyint(1) DEFAULT '0',
  `opt_out` tinyint(1) DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_ea_caps_opt_out_invalid` (`email_address_caps`,`opt_out`,`invalid_email`),
  KEY `idx_ea_opt_out_invalid` (`email_address`,`opt_out`,`invalid_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `email_addresses` */

insert  into `email_addresses`(`id`,`email_address`,`email_address_caps`,`invalid_email`,`opt_out`,`date_created`,`date_modified`,`deleted`) values ('5f99ea0b-fdc7-f8d5-205d-5945a056a087','avitis@avitis.com.br','AVITIS@AVITIS.COM.BR',0,0,'2017-06-17 21:31:32','2017-06-17 21:31:32',0);

/*Table structure for table `email_cache` */

DROP TABLE IF EXISTS `email_cache`;

CREATE TABLE `email_cache` (
  `ie_id` char(36) DEFAULT NULL,
  `mbox` varchar(60) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `fromaddr` varchar(100) DEFAULT NULL,
  `toaddr` varchar(255) DEFAULT NULL,
  `senddate` datetime DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `mailsize` int(10) unsigned DEFAULT NULL,
  `imap_uid` int(10) unsigned DEFAULT NULL,
  `msgno` int(10) unsigned DEFAULT NULL,
  `recent` tinyint(4) DEFAULT NULL,
  `flagged` tinyint(4) DEFAULT NULL,
  `answered` tinyint(4) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `seen` tinyint(4) DEFAULT NULL,
  `draft` tinyint(4) DEFAULT NULL,
  KEY `idx_ie_id` (`ie_id`),
  KEY `idx_mail_date` (`ie_id`,`mbox`,`senddate`),
  KEY `idx_mail_from` (`ie_id`,`mbox`,`fromaddr`),
  KEY `idx_mail_subj` (`subject`),
  KEY `idx_mail_to` (`toaddr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `email_cache` */

/*Table structure for table `email_marketing` */

DROP TABLE IF EXISTS `email_marketing`;

CREATE TABLE `email_marketing` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `from_name` varchar(100) DEFAULT NULL,
  `from_addr` varchar(100) DEFAULT NULL,
  `reply_to_name` varchar(100) DEFAULT NULL,
  `reply_to_addr` varchar(100) DEFAULT NULL,
  `inbound_email_id` varchar(36) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `template_id` char(36) NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `outbound_email_id` char(36) DEFAULT NULL,
  `all_prospect_lists` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_emmkt_name` (`name`),
  KEY `idx_emmkit_del` (`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `email_marketing` */

/*Table structure for table `email_marketing_prospect_lists` */

DROP TABLE IF EXISTS `email_marketing_prospect_lists`;

CREATE TABLE `email_marketing_prospect_lists` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `email_marketing_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email_mp_prospects` (`email_marketing_id`,`prospect_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `email_marketing_prospect_lists` */

/*Table structure for table `email_templates` */

DROP TABLE IF EXISTS `email_templates`;

CREATE TABLE `email_templates` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `published` varchar(3) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `body_html` text,
  `deleted` tinyint(1) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `text_only` tinyint(1) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_email_template_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `email_templates` */

insert  into `email_templates`(`id`,`date_entered`,`date_modified`,`modified_user_id`,`created_by`,`published`,`name`,`description`,`subject`,`body`,`body_html`,`deleted`,`assigned_user_id`,`text_only`,`type`) values ('1d525146-36b8-aa79-def9-5945a0c647a1','2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','off','Joomla Account Creation','Template used when informing a contact that they\'ve been given an account on the joomla portal.','Support Portal Account Created','Hi $contact_name,\r\n					   An account has been created for you at $portal_address.\r\n					   You may login using this email address and the password $joomla_pass','<p>Hi $contact_name,</p>\r\n					    <p>An account has been created for you at <a href=\"$portal_address\">$portal_address</a>.</p>\r\n					    <p>You may login using this email address and the password $joomla_pass</p>',0,NULL,NULL,NULL),('2df10cde-c1f5-77be-4286-5945a0446e72','2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','off','Case Creation','Template to send to a contact when a case is received from them.','$acase_name [CASE:$acase_case_number]','Hi $contact_first_name $contact_last_name,\r\n\r\n					   We\'ve received your case $acase_name (# $acase_case_number) on $acase_date_entered\r\n					   Status:		$acase_status\r\n					   Reference:	$acase_case_number\r\n					   Description:	$acase_description','<p> Hi $contact_first_name $contact_last_name,</p>\r\n					    <p>We\'ve received your case $acase_name (# $acase_case_number) on $acase_date_entered</p>\r\n					    <table border=\"0\"><tbody><tr><td>Status</td><td>$acase_status</td></tr><tr><td>Reference</td><td>$acase_case_number</td></tr><tr><td>Description</td><td>$acase_description</td></tr></tbody></table>',0,NULL,NULL,NULL),('403c91ec-d40a-16fc-c6d8-5945a065aced','2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','off','Contact Case Update','Template to send to a contact when their case is updated.','$acase_name update [CASE:$acase_case_number]','Hi $user_first_name $user_last_name,\r\n\r\n					   You\'ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:\r\n					       $contact_first_name $contact_last_name, said:\r\n					               $aop_case_updates_description','<p>Hi $contact_first_name $contact_last_name,</p>\r\n					    <p> </p>\r\n					    <p>You\'ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:</p>\r\n					    <p><strong>$user_first_name $user_last_name said:</strong></p>\r\n					    <p style=\"padding-left:30px;\">$aop_case_updates_description</p>',0,NULL,NULL,NULL),('583eeb9e-95dc-0734-7741-5945a08258fd','2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','off','User Case Update','Email template to send to a Sugar user when their case is updated.','$acase_name (# $acase_case_number) update','Hi $user_first_name $user_last_name,\r\n\r\n					   You\'ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:\r\n					       $contact_first_name $contact_last_name, said:\r\n					               $aop_case_updates_description\r\n                        You may review this Case at:\r\n                            $sugarurl/index.php?module=Cases&action=DetailView&record=$acase_id;','<p>Hi $user_first_name $user_last_name,</p>\r\n					   <p> </p>\r\n					   <p>You\'ve had an update to your case $acase_name (# $acase_case_number) on $aop_case_updates_date_entered:</p>\r\n					   <p><strong>$contact_first_name $contact_last_name, said:</strong></p>\r\n					   <p style=\"padding-left:30px;\">$aop_case_updates_description</p>\r\n					   <p>You may review this Case at: $sugarurl/index.php?module=Cases&action=DetailView&record=$acase_id;</p>\r\n					   ',0,NULL,NULL,NULL),('7e330fba-b301-a664-c6f0-5945a08dfeba','2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','off','System-generated password email','This template is used when the System Administrator sends a new password to a user.','New account information','\nHere is your account username and temporary password:\nUsername : $contact_user_user_name\nPassword : $contact_user_user_hash\n\n$config_site_url\n\nAfter you log in using the above password, you may be required to reset the password to one of your own choice.','<div><table width=\"550\"><tbody><tr><td><p>Here is your account username and temporary password:</p><p>Username : $contact_user_user_name </p><p>Password : $contact_user_user_hash </p><br /><p>$config_site_url</p><br /><p>After you log in using the above password, you may be required to reset the password to one of your own choice.</p>   </td>         </tr><tr><td></td>         </tr></tbody></table></div>',0,NULL,0,NULL),('8c4cf271-8377-4fb1-3d2c-5945a00443f2','2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','off','Forgot Password email','This template is used to send a user a link to click to reset the user\'s account password.','Reset your account password','\nYou recently requested on $contact_user_pwd_last_changed to be able to reset your account password.\n\nClick on the link below to reset your password:\n\n$contact_user_link_guid','<div><table width=\"550\"><tbody><tr><td><p>You recently requested on $contact_user_pwd_last_changed to be able to reset your account password. </p><p>Click on the link below to reset your password:</p><p> $contact_user_link_guid </p>  </td>         </tr><tr><td></td>         </tr></tbody></table></div>',0,NULL,0,NULL),('ac44730c-e57f-e8c3-ccd3-5945a0503e35','2013-05-24 14:31:45','2017-06-17 21:31:32','1','1','off','Event Invite Template','Default event invite template.','You have been invited to $fp_events_name','Dear $contact_name,\r\nYou have been invited to $fp_events_name on $fp_events_date_start to $fp_events_date_end\r\n$fp_events_description\r\nYours Sincerely,\r\n','\r\n<p>Dear $contact_name,</p>\r\n<p>You have been invited to $fp_events_name on $fp_events_date_start to $fp_events_date_end</p>\r\n<p>$fp_events_description</p>\r\n<p>If you would like to accept this invititation please click accept.</p>\r\n<p> $fp_events_link or $fp_events_link_declined</p>\r\n<p>Yours Sincerely,</p>\r\n',0,NULL,NULL,'email'),('f3c10113-c660-c4ed-532d-5945a0c1536e','2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','off','Case Closure','Template for informing a contact that their case has been closed.','$acase_name [CASE:$acase_case_number] closed','Hi $contact_first_name $contact_last_name,\r\n\r\n					   Your case $acase_name (# $acase_case_number) has been closed on $acase_date_entered\r\n					   Status:				$acase_status\r\n					   Reference:			$acase_case_number\r\n					   Resolution:			$acase_resolution','<p> Hi $contact_first_name $contact_last_name,</p>\r\n					    <p>Your case $acase_name (# $acase_case_number) has been closed on $acase_date_entered</p>\r\n					    <table border=\"0\"><tbody><tr><td>Status</td><td>$acase_status</td></tr><tr><td>Reference</td><td>$acase_case_number</td></tr><tr><td>Resolution</td><td>$acase_resolution</td></tr></tbody></table>',0,NULL,NULL,NULL);

/*Table structure for table `emailman` */

DROP TABLE IF EXISTS `emailman`;

CREATE TABLE `emailman` (
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` char(36) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` char(36) DEFAULT NULL,
  `marketing_id` char(36) DEFAULT NULL,
  `list_id` char(36) DEFAULT NULL,
  `send_date_time` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `in_queue` tinyint(1) DEFAULT '0',
  `in_queue_date` datetime DEFAULT NULL,
  `send_attempts` int(11) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `related_id` char(36) DEFAULT NULL,
  `related_type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_eman_list` (`list_id`,`user_id`,`deleted`),
  KEY `idx_eman_campaign_id` (`campaign_id`),
  KEY `idx_eman_relid_reltype_id` (`related_id`,`related_type`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `emailman` */

/*Table structure for table `emails` */

DROP TABLE IF EXISTS `emails`;

CREATE TABLE `emails` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `flagged` tinyint(1) DEFAULT NULL,
  `reply_to_status` tinyint(1) DEFAULT NULL,
  `intent` varchar(100) DEFAULT 'pick',
  `mailbox_id` char(36) DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_email_name` (`name`),
  KEY `idx_message_id` (`message_id`),
  KEY `idx_email_parent_id` (`parent_id`),
  KEY `idx_email_assigned` (`assigned_user_id`,`type`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `emails` */

/*Table structure for table `emails_beans` */

DROP TABLE IF EXISTS `emails_beans`;

CREATE TABLE `emails_beans` (
  `id` char(36) NOT NULL,
  `email_id` char(36) DEFAULT NULL,
  `bean_id` char(36) DEFAULT NULL,
  `bean_module` varchar(100) DEFAULT NULL,
  `campaign_data` text,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_emails_beans_bean_id` (`bean_id`),
  KEY `idx_emails_beans_email_bean` (`email_id`,`bean_id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `emails_beans` */

/*Table structure for table `emails_email_addr_rel` */

DROP TABLE IF EXISTS `emails_email_addr_rel`;

CREATE TABLE `emails_email_addr_rel` (
  `id` char(36) NOT NULL,
  `email_id` char(36) NOT NULL,
  `address_type` varchar(4) DEFAULT NULL,
  `email_address_id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_eearl_email_id` (`email_id`,`address_type`),
  KEY `idx_eearl_address_id` (`email_address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `emails_email_addr_rel` */

/*Table structure for table `emails_text` */

DROP TABLE IF EXISTS `emails_text`;

CREATE TABLE `emails_text` (
  `email_id` char(36) NOT NULL,
  `from_addr` varchar(255) DEFAULT NULL,
  `reply_to_addr` varchar(255) DEFAULT NULL,
  `to_addrs` text,
  `cc_addrs` text,
  `bcc_addrs` text,
  `description` longtext,
  `description_html` longtext,
  `raw_source` longtext,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`email_id`),
  KEY `emails_textfromaddr` (`from_addr`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `emails_text` */

/*Table structure for table `favorites` */

DROP TABLE IF EXISTS `favorites`;

CREATE TABLE `favorites` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `favorites` */

/*Table structure for table `fields_meta_data` */

DROP TABLE IF EXISTS `fields_meta_data`;

CREATE TABLE `fields_meta_data` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `vname` varchar(255) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `help` varchar(255) DEFAULT NULL,
  `custom_module` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `len` int(11) DEFAULT NULL,
  `required` tinyint(1) DEFAULT '0',
  `default_value` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `audited` tinyint(1) DEFAULT '0',
  `massupdate` tinyint(1) DEFAULT '0',
  `duplicate_merge` smallint(6) DEFAULT '0',
  `reportable` tinyint(1) DEFAULT '1',
  `importable` varchar(255) DEFAULT NULL,
  `ext1` varchar(255) DEFAULT NULL,
  `ext2` varchar(255) DEFAULT NULL,
  `ext3` varchar(255) DEFAULT NULL,
  `ext4` text,
  PRIMARY KEY (`id`),
  KEY `idx_meta_id_del` (`id`,`deleted`),
  KEY `idx_meta_cm_del` (`custom_module`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `fields_meta_data` */

insert  into `fields_meta_data`(`id`,`name`,`vname`,`comments`,`help`,`custom_module`,`type`,`len`,`required`,`default_value`,`date_modified`,`deleted`,`audited`,`massupdate`,`duplicate_merge`,`reportable`,`importable`,`ext1`,`ext2`,`ext3`,`ext4`) values ('Accountsaniversario_c','aniversario_c','LBL_ANIVERSARIO','','Dia e Mês do Aniversário','Accounts','varchar',5,0,'','2017-06-17 22:33:20',0,0,0,0,1,'true','','','',''),('Accountsclasse_c','classe_c','LBL_CLASSE','','Campo que define a classificação interna do médico','Accounts','dynamicenum',100,0,NULL,'2017-06-17 22:12:59',0,0,0,0,1,'true','classe_list','','',''),('Accountscrm_cro_c','crm_cro_c','LBL_CRM_CRO','','Tipo de Conselho','Accounts','dynamicenum',100,1,NULL,'2017-06-17 22:27:26',0,0,0,0,1,'true','crm_cro_list','','',''),('Accountsemail_c','email_c','LBL_EMAIL','','Email do Médico','Accounts','varchar',150,1,'','2017-06-17 22:35:36',0,0,0,0,1,'true','','','',''),('Accountsespecialidade_c','especialidade_c','LBL_ESPECIALIDADE','','Especialidade do Médico','Accounts','dynamicenum',100,0,NULL,'2017-06-17 22:19:54',0,0,0,0,1,'true','especialidade_list','','',''),('Accountsform_trat_c','form_trat_c','LBL_FORM_TRAT',NULL,'Forma de Tratamento','Accounts','enum',100,1,NULL,'2017-06-17 22:16:48',0,0,0,0,1,'true','form_trat_list',NULL,NULL,NULL),('Accountsfreq_visita_c','freq_visita_c','LBL_FREQ_VISITA','','Frequência de Visitas','Accounts','dynamicenum',100,0,NULL,'2017-06-17 22:15:51',0,0,0,0,1,'true','freq_visita_list','','',''),('Accountshospital_c','hospital_c','LBL_HOSPITAL','','','Accounts','relate',255,0,NULL,'2017-06-18 03:46:11',0,0,0,0,1,'true','','HOS_HOSPITAL','hos_hospital_id_c',''),('Accountshos_hospital_id_c','hos_hospital_id_c','LBL_HOSPITAL_HOS_HOSPITAL_ID','','','Accounts','id',36,0,NULL,'2017-06-18 03:46:11',0,0,0,0,0,'true','','','',''),('Accountsjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Accounts','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Accountsjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Accounts','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Accountsjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Accounts','float',10,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('Accountsjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Accounts','float',11,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('Accountsnome_convenio_c','nome_convenio_c','LBL_NOME_CONVENIO','','Nome do Convênio','Accounts','varchar',150,0,'','2017-06-17 22:51:57',0,0,0,0,1,'true','','','',''),('Accountsnum_crmcro_c','num_crmcro_c','LBL_NUM_CRMCRO','','Número do Registro no Conselho de Classe do Médico','Accounts','varchar',50,1,'','2017-06-17 22:28:40',0,0,0,0,1,'true','','','',''),('Accountsparticularidade_c','particularidade_c','LBL_PARTICULARIDADE','','Particularidades do Médico','Accounts','text',NULL,0,'','2017-06-17 22:57:54',0,0,0,0,1,'true','','4','20',''),('Accountspercentual_completude_c','percentual_completude_c','LBL_PERCENTUAL_COMPLETUDE','','Percentual de Completudo de Cadastro','Accounts','int',255,1,'','2017-06-17 23:00:21',0,0,0,0,1,'true','','','',''),('Accountsperc_convenio_c','perc_convenio_c','LBL_PERC_CONVENIO','','Percentual da Predominância do Convênio no total da cirurgia','Accounts','varchar',3,0,'','2017-06-17 22:53:39',0,0,0,0,1,'true','','','',''),('Accountsperc_hospital_c','perc_hospital_c','LBL_PERC_HOSPITAL','','Percentual da Predominância do Hospital no total da cirurgia','Accounts','varchar',3,0,'','2017-06-17 22:54:59',0,0,0,0,1,'true','','','',''),('Accountspredominancia_c','predominancia_c','LBL_PREDOMINANCIA','','Percentual da Predominância do Procedimento no total da cirurgia','Accounts','varchar',3,0,'','2017-06-17 22:49:27',0,0,0,0,1,'true','','','',''),('Accountspref_cirurgica_c','pref_cirurgica_c','LBL_PREF_CIRURGICA',NULL,'Preferências Cirurgicas do Médico','Accounts','text',NULL,0,NULL,'2017-06-17 23:31:46',0,0,0,0,1,'true',NULL,'4','20',NULL),('Accountsquant_cirurgiais_c','quant_cirurgiais_c','LBL_QUANT_CIRURGIAIS','','Quantidade/Volume de Cirurgias no mês que o médico realiza','Accounts','varchar',3,0,'','2017-06-17 22:45:08',0,0,0,0,1,'true','','','',''),('Accountsstatus_cadastro_c','status_cadastro_c','LBL_STATUS_CADASTRO','','Status do Cadastro do Médico','Accounts','dynamicenum',100,0,NULL,'2017-06-17 22:40:47',0,0,0,0,1,'true','status_cadastro_list','','',''),('Accountstipo_cliente_c','tipo_cliente_c','LBL_TIPO_CLIENTE','','Tipo do Cliente','Accounts','dynamicenum',100,0,NULL,'2017-06-17 22:43:01',0,0,0,0,1,'true','tipo_cliente_list','','',''),('Accountsuf_crm_c','uf_crm_c','LBL_UF_CRM','','UF do CRM','Accounts','dynamicenum',100,1,NULL,'2017-06-17 22:26:03',0,0,0,0,1,'true','brasil_uf_list','','',''),('Casesjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Cases','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Casesjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Cases','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Casesjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Cases','float',10,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('Casesjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Cases','float',11,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('COM_COMPETIDORESpercentual_c','percentual_c','LBL_PERCENTUAL','','Percentual que o médico opera com a empresa competidora','COM_COMPETIDORES','varchar',3,0,'','2017-06-18 00:17:14',0,0,0,0,1,'true','','','',''),('Contactsjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Contacts','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Contactsjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Contacts','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Contactsjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Contacts','float',10,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('Contactsjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Contacts','float',11,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('CON_CONSULTORIOSaniv_sec_c','aniv_sec_c','LBL_ANIV_SEC','','','CON_CONSULTORIOS','date',NULL,0,'','2017-06-18 00:51:41',0,0,0,0,1,'true','','','',''),('CON_CONSULTORIOScep_cons_c','cep_cons_c','LBL_CEP_CONS','','','CON_CONSULTORIOS','varchar',255,0,'','2017-06-18 00:28:48',0,0,0,0,1,'true','','','',''),('CON_CONSULTORIOScid_cons_c','cid_cons_c','LBL_CID_CONS','','','CON_CONSULTORIOS','varchar',255,0,'','2017-06-18 00:26:48',0,0,0,0,1,'true','','','',''),('CON_CONSULTORIOSdia_qua_c','dia_qua_c','LBL_DIA_QUA','','','CON_CONSULTORIOS','bool',255,0,'0','2017-06-18 00:42:03',0,0,0,0,1,'true','','','',''),('CON_CONSULTORIOSdia_qui_c','dia_qui_c','LBL_DIA_QUI','','','CON_CONSULTORIOS','bool',255,0,'0','2017-06-18 00:43:24',0,0,0,0,1,'true','','','',''),('CON_CONSULTORIOSdia_seg_c','dia_seg_c','LBL_DIA_SEG','','','CON_CONSULTORIOS','bool',255,0,'0','2017-06-18 00:34:03',0,0,0,0,1,'true','','','',''),('CON_CONSULTORIOSdia_sex_c','dia_sex_c','LBL_DIA_SEX','','','CON_CONSULTORIOS','bool',255,0,'0','2017-06-18 00:44:09',0,0,0,0,1,'true','','','',''),('CON_CONSULTORIOSdia_ter_c','dia_ter_c','LBL_DIA_TER','','','CON_CONSULTORIOS','bool',255,0,'0','2017-06-18 00:41:11',0,0,0,0,1,'true','','','',''),('CON_CONSULTORIOSemail_sec_c','email_sec_c','LBL_EMAIL_SEC','','','CON_CONSULTORIOS','varchar',255,0,'','2017-06-18 00:50:41',0,0,0,0,1,'true','','','',''),('CON_CONSULTORIOSend_cons_c','end_cons_c','LBL_END_CONS','','','CON_CONSULTORIOS','varchar',255,0,'','2017-06-18 00:25:55',0,0,0,0,1,'true','','','',''),('CON_CONSULTORIOSest_cons_c','est_cons_c','LBL_EST_CONS','','','CON_CONSULTORIOS','enum',100,0,NULL,'2017-06-18 00:29:55',0,0,0,0,1,'true','brasil_uf_list','','',''),('CON_CONSULTORIOShora_qua_c','hora_qua_c','LBL_HORA_QUA','','','CON_CONSULTORIOS','varchar',255,0,'','2017-06-18 00:47:16',0,0,0,0,1,'true','','','',''),('CON_CONSULTORIOShora_qui_c','hora_qui_c','LBL_HORA_QUI','','','CON_CONSULTORIOS','varchar',255,0,'','2017-06-18 00:48:21',0,0,0,0,1,'true','','','',''),('CON_CONSULTORIOShora_seg_c','hora_seg_c','LBL_HORA_SEG',NULL,NULL,'CON_CONSULTORIOS','varchar',255,0,NULL,'2017-06-18 00:45:01',0,0,0,0,1,'true',NULL,NULL,NULL,NULL),('CON_CONSULTORIOShora_sex_c','hora_sex_c','LBL_HORA_SEX','','','CON_CONSULTORIOS','varchar',255,0,'','2017-06-18 00:49:06',0,0,0,0,1,'true','','','',''),('CON_CONSULTORIOShora_ter_c','hora_ter_c','LBL_HORA_TER','','','CON_CONSULTORIOS','varchar',255,0,'','2017-06-18 00:46:21',0,0,0,0,1,'true','','','',''),('CON_CONSULTORIOSnome_sec_c','nome_sec_c','LBL_NOME_SEC','','','CON_CONSULTORIOS','varchar',255,0,'','2017-06-18 00:49:51',0,0,0,0,1,'true','','','',''),('Leadsaniversario_c','aniversario_c','LBL_ANIVERSARIO','','','Leads','varchar',255,0,'','2017-06-17 23:44:23',0,0,0,0,1,'true','','','',''),('Leadscrm_cro_c','crm_cro_c','LBL_CRM_CRO','','','Leads','dynamicenum',100,1,NULL,'2017-06-17 23:41:05',0,0,0,0,1,'true','crm_cro_list','','',''),('Leadsemail_c','email_c','LBL_EMAIL','','','Leads','varchar',255,1,'','2017-06-17 23:45:46',0,0,0,0,1,'true','','','',''),('Leadsespecialidade_c','especialidade_c','LBL_ESPECIALIDADE','','','Leads','dynamicenum',100,0,NULL,'2017-06-17 23:37:58',0,0,0,0,1,'true','especialidade_list','','',''),('Leadsform_trat_c','form_trat_c','LBL_FORM_TRAT','','','Leads','enum',100,1,NULL,'2017-06-17 23:35:56',0,0,0,0,1,'true','form_trat_list','','',''),('Leadsjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Leads','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Leadsjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Leads','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Leadsjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Leads','float',10,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('Leadsjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Leads','float',11,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('Leadsnro_conselho_c','nro_conselho_c','LBL_NRO_CONSELHO','','','Leads','varchar',255,1,'','2017-06-17 23:42:38',0,0,0,0,1,'true','','','',''),('Leadsuf_crm_c','uf_crm_c','LBL_UF_CRM','','','Leads','dynamicenum',100,1,NULL,'2017-06-17 23:39:26',0,0,0,0,1,'true','brasil_uf_list','','',''),('Meetingsjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Meetings','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Meetingsjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Meetings','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Meetingsjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Meetings','float',10,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('Meetingsjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Meetings','float',11,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('Opportunitiesjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Opportunities','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Opportunitiesjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Opportunities','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Opportunitiesjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Opportunities','float',10,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('Opportunitiesjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Opportunities','float',11,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('PME_PROCEDIMENTO_MEDICOporcentagem_c','porcentagem_c','LBL_PORCENTAGEM','','porcentagem que o médico atua','PME_PROCEDIMENTO_MEDICO','varchar',255,0,'','2017-06-20 00:26:47',0,0,0,0,1,'true','','','',''),('PME_PROCEDIMENTO_MEDICOprocedimento_c','procedimento_c','LBL_PROCEDIMENTO','','','PME_PROCEDIMENTO_MEDICO','relate',255,1,NULL,'2017-06-20 00:24:35',0,0,0,0,1,'true','','PRO_PROCEDIMENTO','pro_procedimento_id_c',''),('PME_PROCEDIMENTO_MEDICOpro_procedimento_id_c','pro_procedimento_id_c','LBL_PROCEDIMENTO_PRO_PROCEDIMENTO_ID','','','PME_PROCEDIMENTO_MEDICO','id',36,0,NULL,'2017-06-20 00:24:35',0,0,0,0,0,'true','','','',''),('Projectjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Project','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Projectjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Project','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Projectjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Project','float',10,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('Projectjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Project','float',11,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('Prospectsjjwg_maps_address_c','jjwg_maps_address_c','LBL_JJWG_MAPS_ADDRESS','Address','Address','Prospects','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Prospectsjjwg_maps_geocode_status_c','jjwg_maps_geocode_status_c','LBL_JJWG_MAPS_GEOCODE_STATUS','Geocode Status','Geocode Status','Prospects','varchar',255,0,NULL,'2017-06-17 21:31:32',0,0,0,0,1,'true',NULL,'','',''),('Prospectsjjwg_maps_lat_c','jjwg_maps_lat_c','LBL_JJWG_MAPS_LAT','','Latitude','Prospects','float',10,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('Prospectsjjwg_maps_lng_c','jjwg_maps_lng_c','LBL_JJWG_MAPS_LNG','','Longitude','Prospects','float',11,0,'0.00000000','2017-06-17 21:31:32',0,0,0,0,1,'true','8','','',''),('VIS_VISITAaccount_id_c','account_id_c','LBL_MEDI_VISITA_ACCOUNT_ID','','','VIS_VISITA','id',36,0,NULL,'2017-06-18 01:17:08',0,0,0,0,0,'true','','','',''),('VIS_VISITAcons_visita_c','cons_visita_c','LBL_CONS_VISITA',NULL,NULL,'VIS_VISITA','relate',255,1,NULL,'2017-06-18 01:16:07',0,0,0,0,1,'true',NULL,'CON_CONSULTORIOS','con_consultorios_id_c',NULL),('VIS_VISITAcon_consultorios_id_c','con_consultorios_id_c','LBL_CONS_VISITA_CON_CONSULTORIOS_ID','','','VIS_VISITA','id',36,0,NULL,'2017-06-18 01:15:31',0,0,0,0,0,'true','','','',''),('VIS_VISITAdata_hora_visita_c','data_hora_visita_c','LBL_DATA_HORA_VISITA',NULL,'Data e Hora planejadas da Visita','VIS_VISITA','datetimecombo',NULL,1,NULL,'2017-06-18 01:10:36',0,0,0,0,1,'true',NULL,NULL,NULL,NULL),('VIS_VISITAdata_real_visita_c','data_real_visita_c','LBL_DATA_REAL_VISITA','','Data e Hora em que a visita foi efetivamente realizada','VIS_VISITA','datetimecombo',NULL,0,'','2017-06-18 01:12:48',0,0,0,0,1,'true','','','',''),('VIS_VISITAmedi_visita_c','medi_visita_c','LBL_MEDI_VISITA','','Médico que será visitado','VIS_VISITA','relate',255,1,NULL,'2017-06-18 01:17:08',0,0,0,0,1,'true','','Accounts','account_id_c',''),('VIS_VISITAstatus_aprovacao_c','status_aprovacao_c','LBL_STATUS_APROVACAO','','Status da Aprovação do Agendamento pelo Gestor','VIS_VISITA','dynamicenum',100,0,NULL,'2017-06-18 01:19:13',0,0,0,0,1,'true','status_aprovacao_list','','',''),('VIS_VISITAstatus_realizacao_c','status_realizacao_c','LBL_STATUS_REALIZACAO','','Status da Realização da Visita','VIS_VISITA','dynamicenum',100,0,NULL,'2017-06-18 01:21:16',0,0,0,0,1,'true','status_realizacao_list','','','');

/*Table structure for table `folders` */

DROP TABLE IF EXISTS `folders`;

CREATE TABLE `folders` (
  `id` char(36) NOT NULL,
  `name` varchar(25) DEFAULT NULL,
  `folder_type` varchar(25) DEFAULT NULL,
  `parent_folder` char(36) DEFAULT NULL,
  `has_child` tinyint(1) DEFAULT '0',
  `is_group` tinyint(1) DEFAULT '0',
  `is_dynamic` tinyint(1) DEFAULT '0',
  `dynamic_query` text,
  `assign_to_id` char(36) DEFAULT NULL,
  `created_by` char(36) NOT NULL,
  `modified_by` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_parent_folder` (`parent_folder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `folders` */

/*Table structure for table `folders_rel` */

DROP TABLE IF EXISTS `folders_rel`;

CREATE TABLE `folders_rel` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `polymorphic_module` varchar(25) DEFAULT NULL,
  `polymorphic_id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_poly_module_poly_id` (`polymorphic_module`,`polymorphic_id`),
  KEY `idx_fr_id_deleted_poly` (`folder_id`,`deleted`,`polymorphic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `folders_rel` */

/*Table structure for table `folders_subscriptions` */

DROP TABLE IF EXISTS `folders_subscriptions`;

CREATE TABLE `folders_subscriptions` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `assigned_user_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_folder_id_assigned_user_id` (`folder_id`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `folders_subscriptions` */

/*Table structure for table `fp_event_locations` */

DROP TABLE IF EXISTS `fp_event_locations`;

CREATE TABLE `fp_event_locations` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address_city` varchar(100) DEFAULT NULL,
  `address_country` varchar(100) DEFAULT NULL,
  `address_postalcode` varchar(20) DEFAULT NULL,
  `address_state` varchar(100) DEFAULT NULL,
  `capacity` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `fp_event_locations` */

/*Table structure for table `fp_event_locations_audit` */

DROP TABLE IF EXISTS `fp_event_locations_audit`;

CREATE TABLE `fp_event_locations_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_fp_event_locations_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `fp_event_locations_audit` */

/*Table structure for table `fp_event_locations_fp_events_1_c` */

DROP TABLE IF EXISTS `fp_event_locations_fp_events_1_c`;

CREATE TABLE `fp_event_locations_fp_events_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_event_locations_fp_events_1fp_event_locations_ida` varchar(36) DEFAULT NULL,
  `fp_event_locations_fp_events_1fp_events_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fp_event_locations_fp_events_1_ida1` (`fp_event_locations_fp_events_1fp_event_locations_ida`),
  KEY `fp_event_locations_fp_events_1_alt` (`fp_event_locations_fp_events_1fp_events_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `fp_event_locations_fp_events_1_c` */

/*Table structure for table `fp_events` */

DROP TABLE IF EXISTS `fp_events`;

CREATE TABLE `fp_events` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `duration_hours` int(3) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `budget` decimal(26,6) DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `invite_templates` varchar(100) DEFAULT NULL,
  `accept_redirect` varchar(255) DEFAULT NULL,
  `decline_redirect` varchar(255) DEFAULT NULL,
  `activity_status_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `fp_events` */

/*Table structure for table `fp_events_audit` */

DROP TABLE IF EXISTS `fp_events_audit`;

CREATE TABLE `fp_events_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_fp_events_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `fp_events_audit` */

/*Table structure for table `fp_events_contacts_c` */

DROP TABLE IF EXISTS `fp_events_contacts_c`;

CREATE TABLE `fp_events_contacts_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_contactsfp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_contactscontacts_idb` varchar(36) DEFAULT NULL,
  `invite_status` varchar(25) DEFAULT 'Not Invited',
  `accept_status` varchar(25) DEFAULT 'No Response',
  `email_responded` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fp_events_contacts_alt` (`fp_events_contactsfp_events_ida`,`fp_events_contactscontacts_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `fp_events_contacts_c` */

/*Table structure for table `fp_events_fp_event_delegates_1_c` */

DROP TABLE IF EXISTS `fp_events_fp_event_delegates_1_c`;

CREATE TABLE `fp_events_fp_event_delegates_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_fp_event_delegates_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_fp_event_delegates_1fp_event_delegates_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fp_events_fp_event_delegates_1_ida1` (`fp_events_fp_event_delegates_1fp_events_ida`),
  KEY `fp_events_fp_event_delegates_1_alt` (`fp_events_fp_event_delegates_1fp_event_delegates_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `fp_events_fp_event_delegates_1_c` */

/*Table structure for table `fp_events_fp_event_locations_1_c` */

DROP TABLE IF EXISTS `fp_events_fp_event_locations_1_c`;

CREATE TABLE `fp_events_fp_event_locations_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_fp_event_locations_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_fp_event_locations_1fp_event_locations_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fp_events_fp_event_locations_1_alt` (`fp_events_fp_event_locations_1fp_events_ida`,`fp_events_fp_event_locations_1fp_event_locations_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `fp_events_fp_event_locations_1_c` */

/*Table structure for table `fp_events_leads_1_c` */

DROP TABLE IF EXISTS `fp_events_leads_1_c`;

CREATE TABLE `fp_events_leads_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_leads_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_leads_1leads_idb` varchar(36) DEFAULT NULL,
  `invite_status` varchar(25) DEFAULT 'Not Invited',
  `accept_status` varchar(25) DEFAULT 'No Response',
  `email_responded` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fp_events_leads_1_alt` (`fp_events_leads_1fp_events_ida`,`fp_events_leads_1leads_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `fp_events_leads_1_c` */

/*Table structure for table `fp_events_prospects_1_c` */

DROP TABLE IF EXISTS `fp_events_prospects_1_c`;

CREATE TABLE `fp_events_prospects_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `fp_events_prospects_1fp_events_ida` varchar(36) DEFAULT NULL,
  `fp_events_prospects_1prospects_idb` varchar(36) DEFAULT NULL,
  `invite_status` varchar(25) DEFAULT 'Not Invited',
  `accept_status` varchar(25) DEFAULT 'No Response',
  `email_responded` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fp_events_prospects_1_alt` (`fp_events_prospects_1fp_events_ida`,`fp_events_prospects_1prospects_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `fp_events_prospects_1_c` */

/*Table structure for table `hos_hospital` */

DROP TABLE IF EXISTS `hos_hospital`;

CREATE TABLE `hos_hospital` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `hos_hospital` */

/*Table structure for table `hos_hospital_audit` */

DROP TABLE IF EXISTS `hos_hospital_audit`;

CREATE TABLE `hos_hospital_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_hos_hospital_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `hos_hospital_audit` */

/*Table structure for table `import_maps` */

DROP TABLE IF EXISTS `import_maps`;

CREATE TABLE `import_maps` (
  `id` char(36) NOT NULL,
  `name` varchar(254) DEFAULT NULL,
  `source` varchar(36) DEFAULT NULL,
  `enclosure` varchar(1) DEFAULT ' ',
  `delimiter` varchar(1) DEFAULT ',',
  `module` varchar(36) DEFAULT NULL,
  `content` text,
  `default_values` text,
  `has_header` tinyint(1) DEFAULT '1',
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `is_published` varchar(3) DEFAULT 'no',
  PRIMARY KEY (`id`),
  KEY `idx_owner_module_name` (`assigned_user_id`,`module`,`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `import_maps` */

/*Table structure for table `inbound_email` */

DROP TABLE IF EXISTS `inbound_email`;

CREATE TABLE `inbound_email` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Active',
  `server_url` varchar(100) DEFAULT NULL,
  `email_user` varchar(100) DEFAULT NULL,
  `email_password` varchar(100) DEFAULT NULL,
  `port` int(5) DEFAULT NULL,
  `service` varchar(50) DEFAULT NULL,
  `mailbox` text,
  `delete_seen` tinyint(1) DEFAULT '0',
  `mailbox_type` varchar(10) DEFAULT NULL,
  `template_id` char(36) DEFAULT NULL,
  `stored_options` text,
  `group_id` char(36) DEFAULT NULL,
  `is_personal` tinyint(1) DEFAULT '0',
  `groupfolder_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `inbound_email` */

/*Table structure for table `inbound_email_autoreply` */

DROP TABLE IF EXISTS `inbound_email_autoreply`;

CREATE TABLE `inbound_email_autoreply` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `autoreplied_to` varchar(100) DEFAULT NULL,
  `ie_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ie_autoreplied_to` (`autoreplied_to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `inbound_email_autoreply` */

/*Table structure for table `inbound_email_cache_ts` */

DROP TABLE IF EXISTS `inbound_email_cache_ts`;

CREATE TABLE `inbound_email_cache_ts` (
  `id` varchar(255) NOT NULL,
  `ie_timestamp` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `inbound_email_cache_ts` */

/*Table structure for table `jjwg_address_cache` */

DROP TABLE IF EXISTS `jjwg_address_cache`;

CREATE TABLE `jjwg_address_cache` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `lat` float(10,8) DEFAULT NULL,
  `lng` float(11,8) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jjwg_address_cache` */

/*Table structure for table `jjwg_address_cache_audit` */

DROP TABLE IF EXISTS `jjwg_address_cache_audit`;

CREATE TABLE `jjwg_address_cache_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_address_cache_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jjwg_address_cache_audit` */

/*Table structure for table `jjwg_areas` */

DROP TABLE IF EXISTS `jjwg_areas`;

CREATE TABLE `jjwg_areas` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `coordinates` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jjwg_areas` */

/*Table structure for table `jjwg_areas_audit` */

DROP TABLE IF EXISTS `jjwg_areas_audit`;

CREATE TABLE `jjwg_areas_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_areas_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jjwg_areas_audit` */

/*Table structure for table `jjwg_maps` */

DROP TABLE IF EXISTS `jjwg_maps`;

CREATE TABLE `jjwg_maps` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `distance` float(9,4) DEFAULT NULL,
  `unit_type` varchar(100) DEFAULT 'mi',
  `module_type` varchar(100) DEFAULT 'Accounts',
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jjwg_maps` */

/*Table structure for table `jjwg_maps_audit` */

DROP TABLE IF EXISTS `jjwg_maps_audit`;

CREATE TABLE `jjwg_maps_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_maps_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jjwg_maps_audit` */

/*Table structure for table `jjwg_maps_jjwg_areas_c` */

DROP TABLE IF EXISTS `jjwg_maps_jjwg_areas_c`;

CREATE TABLE `jjwg_maps_jjwg_areas_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `jjwg_maps_5304wg_maps_ida` varchar(36) DEFAULT NULL,
  `jjwg_maps_41f2g_areas_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jjwg_maps_jjwg_areas_alt` (`jjwg_maps_5304wg_maps_ida`,`jjwg_maps_41f2g_areas_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jjwg_maps_jjwg_areas_c` */

/*Table structure for table `jjwg_maps_jjwg_markers_c` */

DROP TABLE IF EXISTS `jjwg_maps_jjwg_markers_c`;

CREATE TABLE `jjwg_maps_jjwg_markers_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `jjwg_maps_b229wg_maps_ida` varchar(36) DEFAULT NULL,
  `jjwg_maps_2e31markers_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jjwg_maps_jjwg_markers_alt` (`jjwg_maps_b229wg_maps_ida`,`jjwg_maps_2e31markers_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jjwg_maps_jjwg_markers_c` */

/*Table structure for table `jjwg_markers` */

DROP TABLE IF EXISTS `jjwg_markers`;

CREATE TABLE `jjwg_markers` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `jjwg_maps_lat` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_lng` float(11,8) DEFAULT '0.00000000',
  `marker_image` varchar(100) DEFAULT 'company',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jjwg_markers` */

/*Table structure for table `jjwg_markers_audit` */

DROP TABLE IF EXISTS `jjwg_markers_audit`;

CREATE TABLE `jjwg_markers_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_jjwg_markers_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `jjwg_markers_audit` */

/*Table structure for table `job_queue` */

DROP TABLE IF EXISTS `job_queue`;

CREATE TABLE `job_queue` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `scheduler_id` char(36) DEFAULT NULL,
  `execute_time` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `resolution` varchar(20) DEFAULT NULL,
  `message` text,
  `target` varchar(255) DEFAULT NULL,
  `data` text,
  `requeue` tinyint(1) DEFAULT '0',
  `retry_count` tinyint(4) DEFAULT NULL,
  `failure_count` tinyint(4) DEFAULT NULL,
  `job_delay` int(11) DEFAULT NULL,
  `client` varchar(255) DEFAULT NULL,
  `percent_complete` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_status_scheduler` (`status`,`scheduler_id`),
  KEY `idx_status_time` (`status`,`execute_time`,`date_entered`),
  KEY `idx_status_entered` (`status`,`date_entered`),
  KEY `idx_status_modified` (`status`,`date_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `job_queue` */

/*Table structure for table `leads` */

DROP TABLE IF EXISTS `leads`;

CREATE TABLE `leads` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `converted` tinyint(1) DEFAULT '0',
  `refered_by` varchar(100) DEFAULT NULL,
  `lead_source` varchar(100) DEFAULT NULL,
  `lead_source_description` text,
  `status` varchar(100) DEFAULT NULL,
  `status_description` text,
  `reports_to_id` char(36) DEFAULT NULL,
  `account_name` varchar(255) DEFAULT NULL,
  `account_description` text,
  `contact_id` char(36) DEFAULT NULL,
  `account_id` char(36) DEFAULT NULL,
  `opportunity_id` char(36) DEFAULT NULL,
  `opportunity_name` varchar(255) DEFAULT NULL,
  `opportunity_amount` varchar(50) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `portal_name` varchar(255) DEFAULT NULL,
  `portal_app` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_lead_acct_name_first` (`account_name`,`deleted`),
  KEY `idx_lead_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_lead_del_stat` (`last_name`,`status`,`deleted`,`first_name`),
  KEY `idx_lead_opp_del` (`opportunity_id`,`deleted`),
  KEY `idx_leads_acct_del` (`account_id`,`deleted`),
  KEY `idx_del_user` (`deleted`,`assigned_user_id`),
  KEY `idx_lead_assigned` (`assigned_user_id`),
  KEY `idx_lead_contact` (`contact_id`),
  KEY `idx_reports_to` (`reports_to_id`),
  KEY `idx_lead_phone_work` (`phone_work`),
  KEY `idx_leads_id_del` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `leads` */

/*Table structure for table `leads_audit` */

DROP TABLE IF EXISTS `leads_audit`;

CREATE TABLE `leads_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_leads_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `leads_audit` */

/*Table structure for table `leads_cstm` */

DROP TABLE IF EXISTS `leads_cstm`;

CREATE TABLE `leads_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  `form_trat_c` varchar(100) DEFAULT NULL,
  `especialidade_c` varchar(100) DEFAULT NULL,
  `uf_crm_c` varchar(100) DEFAULT NULL,
  `crm_cro_c` varchar(100) DEFAULT NULL,
  `nro_conselho_c` varchar(255) DEFAULT NULL,
  `aniversario_c` varchar(255) DEFAULT NULL,
  `email_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `leads_cstm` */

/*Table structure for table `linked_documents` */

DROP TABLE IF EXISTS `linked_documents`;

CREATE TABLE `linked_documents` (
  `id` varchar(36) NOT NULL,
  `parent_id` varchar(36) DEFAULT NULL,
  `parent_type` varchar(25) DEFAULT NULL,
  `document_id` varchar(36) DEFAULT NULL,
  `document_revision_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_parent_document` (`parent_type`,`parent_id`,`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `linked_documents` */

/*Table structure for table `meetings` */

DROP TABLE IF EXISTS `meetings`;

CREATE TABLE `meetings` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `join_url` varchar(200) DEFAULT NULL,
  `host_url` varchar(400) DEFAULT NULL,
  `displayed_url` varchar(400) DEFAULT NULL,
  `creator` varchar(50) DEFAULT NULL,
  `external_id` varchar(50) DEFAULT NULL,
  `duration_hours` int(3) DEFAULT NULL,
  `duration_minutes` int(2) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `parent_type` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Planned',
  `type` varchar(255) DEFAULT 'Sugar',
  `parent_id` char(36) DEFAULT NULL,
  `reminder_time` int(11) DEFAULT '-1',
  `email_reminder_time` int(11) DEFAULT '-1',
  `email_reminder_sent` tinyint(1) DEFAULT '0',
  `outlook_id` varchar(255) DEFAULT NULL,
  `sequence` int(11) DEFAULT '0',
  `repeat_type` varchar(36) DEFAULT NULL,
  `repeat_interval` int(3) DEFAULT '1',
  `repeat_dow` varchar(7) DEFAULT NULL,
  `repeat_until` date DEFAULT NULL,
  `repeat_count` int(7) DEFAULT NULL,
  `repeat_parent_id` char(36) DEFAULT NULL,
  `recurring_source` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_mtg_name` (`name`),
  KEY `idx_meet_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_meet_stat_del` (`assigned_user_id`,`status`,`deleted`),
  KEY `idx_meet_date_start` (`date_start`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `meetings` */

/*Table structure for table `meetings_contacts` */

DROP TABLE IF EXISTS `meetings_contacts`;

CREATE TABLE `meetings_contacts` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_mtg_mtg` (`meeting_id`),
  KEY `idx_con_mtg_con` (`contact_id`),
  KEY `idx_meeting_contact` (`meeting_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `meetings_contacts` */

/*Table structure for table `meetings_cstm` */

DROP TABLE IF EXISTS `meetings_cstm`;

CREATE TABLE `meetings_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `meetings_cstm` */

/*Table structure for table `meetings_leads` */

DROP TABLE IF EXISTS `meetings_leads`;

CREATE TABLE `meetings_leads` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `lead_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_lead_meeting_meeting` (`meeting_id`),
  KEY `idx_lead_meeting_lead` (`lead_id`),
  KEY `idx_meeting_lead` (`meeting_id`,`lead_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `meetings_leads` */

/*Table structure for table `meetings_users` */

DROP TABLE IF EXISTS `meetings_users`;

CREATE TABLE `meetings_users` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `required` varchar(1) DEFAULT '1',
  `accept_status` varchar(25) DEFAULT 'none',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_usr_mtg_mtg` (`meeting_id`),
  KEY `idx_usr_mtg_usr` (`user_id`),
  KEY `idx_meeting_users` (`meeting_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `meetings_users` */

/*Table structure for table `notes` */

DROP TABLE IF EXISTS `notes`;

CREATE TABLE `notes` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file_mime_type` varchar(100) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `portal_flag` tinyint(1) DEFAULT NULL,
  `embed_flag` tinyint(1) DEFAULT '0',
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_note_name` (`name`),
  KEY `idx_notes_parent` (`parent_id`,`parent_type`),
  KEY `idx_note_contact` (`contact_id`),
  KEY `idx_notes_assigned_del` (`deleted`,`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `notes` */

/*Table structure for table `oauth_consumer` */

DROP TABLE IF EXISTS `oauth_consumer`;

CREATE TABLE `oauth_consumer` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `c_key` varchar(255) DEFAULT NULL,
  `c_secret` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ckey` (`c_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `oauth_consumer` */

/*Table structure for table `oauth_nonce` */

DROP TABLE IF EXISTS `oauth_nonce`;

CREATE TABLE `oauth_nonce` (
  `conskey` varchar(32) NOT NULL,
  `nonce` varchar(32) NOT NULL,
  `nonce_ts` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`conskey`,`nonce`),
  KEY `oauth_nonce_keyts` (`conskey`,`nonce_ts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `oauth_nonce` */

/*Table structure for table `oauth_tokens` */

DROP TABLE IF EXISTS `oauth_tokens`;

CREATE TABLE `oauth_tokens` (
  `id` char(36) NOT NULL,
  `secret` varchar(32) DEFAULT NULL,
  `tstate` varchar(1) DEFAULT NULL,
  `consumer` char(36) NOT NULL,
  `token_ts` bigint(20) DEFAULT NULL,
  `verify` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `callback_url` varchar(255) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`,`deleted`),
  KEY `oauth_state_ts` (`tstate`,`token_ts`),
  KEY `constoken_key` (`consumer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `oauth_tokens` */

/*Table structure for table `opportunities` */

DROP TABLE IF EXISTS `opportunities`;

CREATE TABLE `opportunities` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `opportunity_type` varchar(255) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  `lead_source` varchar(50) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `amount_usdollar` double DEFAULT NULL,
  `currency_id` char(36) DEFAULT NULL,
  `date_closed` date DEFAULT NULL,
  `next_step` varchar(100) DEFAULT NULL,
  `sales_stage` varchar(255) DEFAULT NULL,
  `probability` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_opp_name` (`name`),
  KEY `idx_opp_assigned` (`assigned_user_id`),
  KEY `idx_opp_id_deleted` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `opportunities` */

/*Table structure for table `opportunities_audit` */

DROP TABLE IF EXISTS `opportunities_audit`;

CREATE TABLE `opportunities_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_opportunities_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `opportunities_audit` */

/*Table structure for table `opportunities_contacts` */

DROP TABLE IF EXISTS `opportunities_contacts`;

CREATE TABLE `opportunities_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `contact_role` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_con_opp_con` (`contact_id`),
  KEY `idx_con_opp_opp` (`opportunity_id`),
  KEY `idx_opportunities_contacts` (`opportunity_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `opportunities_contacts` */

/*Table structure for table `opportunities_cstm` */

DROP TABLE IF EXISTS `opportunities_cstm`;

CREATE TABLE `opportunities_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `opportunities_cstm` */

/*Table structure for table `outbound_email` */

DROP TABLE IF EXISTS `outbound_email`;

CREATE TABLE `outbound_email` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(15) DEFAULT 'user',
  `user_id` char(36) NOT NULL,
  `mail_sendtype` varchar(8) DEFAULT 'smtp',
  `mail_smtptype` varchar(20) DEFAULT 'other',
  `mail_smtpserver` varchar(100) DEFAULT NULL,
  `mail_smtpport` int(5) DEFAULT '0',
  `mail_smtpuser` varchar(100) DEFAULT NULL,
  `mail_smtppass` varchar(100) DEFAULT NULL,
  `mail_smtpauth_req` tinyint(1) DEFAULT '0',
  `mail_smtpssl` varchar(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `outbound_email` */

insert  into `outbound_email`(`id`,`name`,`type`,`user_id`,`mail_sendtype`,`mail_smtptype`,`mail_smtpserver`,`mail_smtpport`,`mail_smtpuser`,`mail_smtppass`,`mail_smtpauth_req`,`mail_smtpssl`,`date_entered`,`date_modified`,`modified_user_id`,`created_by`,`deleted`,`assigned_user_id`) values ('e04423a7-e9ad-708a-40ab-5945a0b0772c','system','system','1','SMTP','other','',25,'','',1,'0',NULL,NULL,NULL,NULL,0,NULL);

/*Table structure for table `outbound_email_audit` */

DROP TABLE IF EXISTS `outbound_email_audit`;

CREATE TABLE `outbound_email_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_outbound_email_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `outbound_email_audit` */

/*Table structure for table `pme_procedimento_medico` */

DROP TABLE IF EXISTS `pme_procedimento_medico`;

CREATE TABLE `pme_procedimento_medico` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pme_procedimento_medico` */

insert  into `pme_procedimento_medico`(`id`,`name`,`date_entered`,`date_modified`,`modified_user_id`,`created_by`,`description`,`deleted`,`assigned_user_id`) values ('47a49096-8403-136c-19a6-59486d94a1eb',NULL,'2017-06-20 00:34:45','2017-06-20 00:34:45','1','1',NULL,0,'1');

/*Table structure for table `pme_procedimento_medico_audit` */

DROP TABLE IF EXISTS `pme_procedimento_medico_audit`;

CREATE TABLE `pme_procedimento_medico_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_pme_procedimento_medico_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pme_procedimento_medico_audit` */

/*Table structure for table `pme_procedimento_medico_cstm` */

DROP TABLE IF EXISTS `pme_procedimento_medico_cstm`;

CREATE TABLE `pme_procedimento_medico_cstm` (
  `id_c` char(36) NOT NULL,
  `pro_procedimento_id_c` char(36) DEFAULT NULL,
  `porcentagem_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pme_procedimento_medico_cstm` */

insert  into `pme_procedimento_medico_cstm`(`id_c`,`pro_procedimento_id_c`,`porcentagem_c`) values ('47a49096-8403-136c-19a6-59486d94a1eb','ae006f80-86e8-89a4-550d-594866f314ff','20%');

/*Table structure for table `pro_procedimento` */

DROP TABLE IF EXISTS `pro_procedimento`;

CREATE TABLE `pro_procedimento` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pro_procedimento` */

insert  into `pro_procedimento`(`id`,`name`,`date_entered`,`date_modified`,`modified_user_id`,`created_by`,`description`,`deleted`,`assigned_user_id`) values ('ae006f80-86e8-89a4-550d-594866f314ff','Procedimeto 01','2017-06-20 00:04:07','2017-06-20 00:04:07','1','1','',0,'1');

/*Table structure for table `pro_procedimento_audit` */

DROP TABLE IF EXISTS `pro_procedimento_audit`;

CREATE TABLE `pro_procedimento_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_pro_procedimento_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pro_procedimento_audit` */

/*Table structure for table `project` */

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `estimated_start_date` date DEFAULT NULL,
  `estimated_end_date` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `override_business_hours` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `project` */

/*Table structure for table `project_contacts_1_c` */

DROP TABLE IF EXISTS `project_contacts_1_c`;

CREATE TABLE `project_contacts_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `project_contacts_1project_ida` varchar(36) DEFAULT NULL,
  `project_contacts_1contacts_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_contacts_1_alt` (`project_contacts_1project_ida`,`project_contacts_1contacts_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `project_contacts_1_c` */

/*Table structure for table `project_cstm` */

DROP TABLE IF EXISTS `project_cstm`;

CREATE TABLE `project_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `project_cstm` */

/*Table structure for table `project_task` */

DROP TABLE IF EXISTS `project_task`;

CREATE TABLE `project_task` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `project_id` char(36) NOT NULL,
  `project_task_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `relationship_type` varchar(255) DEFAULT NULL,
  `description` text,
  `predecessors` text,
  `date_start` date DEFAULT NULL,
  `time_start` int(11) DEFAULT NULL,
  `time_finish` int(11) DEFAULT NULL,
  `date_finish` date DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `duration_unit` text,
  `actual_duration` int(11) DEFAULT NULL,
  `percent_complete` int(11) DEFAULT NULL,
  `date_due` date DEFAULT NULL,
  `time_due` time DEFAULT NULL,
  `parent_task_id` int(11) DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `milestone_flag` tinyint(1) DEFAULT NULL,
  `order_number` int(11) DEFAULT '1',
  `task_number` int(11) DEFAULT NULL,
  `estimated_effort` int(11) DEFAULT NULL,
  `actual_effort` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `utilization` int(11) DEFAULT '100',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `project_task` */

/*Table structure for table `project_task_audit` */

DROP TABLE IF EXISTS `project_task_audit`;

CREATE TABLE `project_task_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_project_task_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `project_task_audit` */

/*Table structure for table `project_users_1_c` */

DROP TABLE IF EXISTS `project_users_1_c`;

CREATE TABLE `project_users_1_c` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `project_users_1project_ida` varchar(36) DEFAULT NULL,
  `project_users_1users_idb` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_users_1_alt` (`project_users_1project_ida`,`project_users_1users_idb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `project_users_1_c` */

/*Table structure for table `projects_accounts` */

DROP TABLE IF EXISTS `projects_accounts`;

CREATE TABLE `projects_accounts` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_acct_proj` (`project_id`),
  KEY `idx_proj_acct_acct` (`account_id`),
  KEY `projects_accounts_alt` (`project_id`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `projects_accounts` */

/*Table structure for table `projects_bugs` */

DROP TABLE IF EXISTS `projects_bugs`;

CREATE TABLE `projects_bugs` (
  `id` varchar(36) NOT NULL,
  `bug_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_bug_proj` (`project_id`),
  KEY `idx_proj_bug_bug` (`bug_id`),
  KEY `projects_bugs_alt` (`project_id`,`bug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `projects_bugs` */

/*Table structure for table `projects_cases` */

DROP TABLE IF EXISTS `projects_cases`;

CREATE TABLE `projects_cases` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_case_proj` (`project_id`),
  KEY `idx_proj_case_case` (`case_id`),
  KEY `projects_cases_alt` (`project_id`,`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `projects_cases` */

/*Table structure for table `projects_contacts` */

DROP TABLE IF EXISTS `projects_contacts`;

CREATE TABLE `projects_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_con_proj` (`project_id`),
  KEY `idx_proj_con_con` (`contact_id`),
  KEY `projects_contacts_alt` (`project_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `projects_contacts` */

/*Table structure for table `projects_opportunities` */

DROP TABLE IF EXISTS `projects_opportunities`;

CREATE TABLE `projects_opportunities` (
  `id` varchar(36) NOT NULL,
  `opportunity_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_opp_proj` (`project_id`),
  KEY `idx_proj_opp_opp` (`opportunity_id`),
  KEY `projects_opportunities_alt` (`project_id`,`opportunity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `projects_opportunities` */

/*Table structure for table `projects_products` */

DROP TABLE IF EXISTS `projects_products`;

CREATE TABLE `projects_products` (
  `id` varchar(36) NOT NULL,
  `product_id` varchar(36) DEFAULT NULL,
  `project_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_proj_prod_project` (`project_id`),
  KEY `idx_proj_prod_product` (`product_id`),
  KEY `projects_products_alt` (`project_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `projects_products` */

/*Table structure for table `prospect_list_campaigns` */

DROP TABLE IF EXISTS `prospect_list_campaigns`;

CREATE TABLE `prospect_list_campaigns` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `campaign_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_pro_id` (`prospect_list_id`),
  KEY `idx_cam_id` (`campaign_id`),
  KEY `idx_prospect_list_campaigns` (`prospect_list_id`,`campaign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `prospect_list_campaigns` */

/*Table structure for table `prospect_lists` */

DROP TABLE IF EXISTS `prospect_lists`;

CREATE TABLE `prospect_lists` (
  `assigned_user_id` char(36) DEFAULT NULL,
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `list_type` varchar(100) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `description` text,
  `domain_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_prospect_list_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `prospect_lists` */

/*Table structure for table `prospect_lists_prospects` */

DROP TABLE IF EXISTS `prospect_lists_prospects`;

CREATE TABLE `prospect_lists_prospects` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) DEFAULT NULL,
  `related_id` varchar(36) DEFAULT NULL,
  `related_type` varchar(25) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_plp_pro_id` (`prospect_list_id`),
  KEY `idx_plp_rel_id` (`related_id`,`related_type`,`prospect_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `prospect_lists_prospects` */

/*Table structure for table `prospects` */

DROP TABLE IF EXISTS `prospects`;

CREATE TABLE `prospects` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `do_not_call` tinyint(1) DEFAULT '0',
  `phone_home` varchar(100) DEFAULT NULL,
  `phone_mobile` varchar(100) DEFAULT NULL,
  `phone_work` varchar(100) DEFAULT NULL,
  `phone_other` varchar(100) DEFAULT NULL,
  `phone_fax` varchar(100) DEFAULT NULL,
  `primary_address_street` varchar(150) DEFAULT NULL,
  `primary_address_city` varchar(100) DEFAULT NULL,
  `primary_address_state` varchar(100) DEFAULT NULL,
  `primary_address_postalcode` varchar(20) DEFAULT NULL,
  `primary_address_country` varchar(255) DEFAULT NULL,
  `alt_address_street` varchar(150) DEFAULT NULL,
  `alt_address_city` varchar(100) DEFAULT NULL,
  `alt_address_state` varchar(100) DEFAULT NULL,
  `alt_address_postalcode` varchar(20) DEFAULT NULL,
  `alt_address_country` varchar(255) DEFAULT NULL,
  `assistant` varchar(75) DEFAULT NULL,
  `assistant_phone` varchar(100) DEFAULT NULL,
  `tracker_key` int(11) NOT NULL AUTO_INCREMENT,
  `birthdate` date DEFAULT NULL,
  `lead_id` char(36) DEFAULT NULL,
  `account_name` varchar(150) DEFAULT NULL,
  `campaign_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prospect_auto_tracker_key` (`tracker_key`),
  KEY `idx_prospects_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_prospecs_del_last` (`last_name`,`deleted`),
  KEY `idx_prospects_id_del` (`id`,`deleted`),
  KEY `idx_prospects_assigned` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `prospects` */

/*Table structure for table `prospects_cstm` */

DROP TABLE IF EXISTS `prospects_cstm`;

CREATE TABLE `prospects_cstm` (
  `id_c` char(36) NOT NULL,
  `jjwg_maps_lng_c` float(11,8) DEFAULT '0.00000000',
  `jjwg_maps_lat_c` float(10,8) DEFAULT '0.00000000',
  `jjwg_maps_geocode_status_c` varchar(255) DEFAULT NULL,
  `jjwg_maps_address_c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `prospects_cstm` */

/*Table structure for table `relationships` */

DROP TABLE IF EXISTS `relationships`;

CREATE TABLE `relationships` (
  `id` char(36) NOT NULL,
  `relationship_name` varchar(150) DEFAULT NULL,
  `lhs_module` varchar(100) DEFAULT NULL,
  `lhs_table` varchar(64) DEFAULT NULL,
  `lhs_key` varchar(64) DEFAULT NULL,
  `rhs_module` varchar(100) DEFAULT NULL,
  `rhs_table` varchar(64) DEFAULT NULL,
  `rhs_key` varchar(64) DEFAULT NULL,
  `join_table` varchar(64) DEFAULT NULL,
  `join_key_lhs` varchar(64) DEFAULT NULL,
  `join_key_rhs` varchar(64) DEFAULT NULL,
  `relationship_type` varchar(64) DEFAULT NULL,
  `relationship_role_column` varchar(64) DEFAULT NULL,
  `relationship_role_column_value` varchar(50) DEFAULT NULL,
  `reverse` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_rel_name` (`relationship_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `relationships` */

insert  into `relationships`(`id`,`relationship_name`,`lhs_module`,`lhs_table`,`lhs_key`,`rhs_module`,`rhs_table`,`rhs_key`,`join_table`,`join_key_lhs`,`join_key_rhs`,`relationship_type`,`relationship_role_column`,`relationship_role_column_value`,`reverse`,`deleted`) values ('10a6ac33-1533-28bd-fa6d-59486eb49675','emails_users_rel','Emails','emails','id','Users','users','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Users',0,0),('11c54c88-1d37-25ef-5503-59486e56edc1','acl_roles_actions','ACLRoles','acl_roles','id','ACLActions','acl_actions','id','acl_roles_actions','role_id','action_id','many-to-many',NULL,NULL,0,0),('11cc234c-59b5-3522-0521-59486edc4487','aod_indexevent_modified_user','Users','users','id','AOD_IndexEvent','aod_indexevent','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('11f5f417-020d-a436-b0b7-59486e064a2b','bug_emails','Bugs','bugs','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('136426e5-38cc-d33d-8c7c-59486e654cb8','lead_meetings','Leads','leads','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('13f420f8-11c7-1383-3247-59486edf06b6','securitygroups_aow_workflow','SecurityGroups','securitygroups','id','AOW_WorkFlow','aow_workflow','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOW_WorkFlow',0,0),('14c4f2b3-357a-5be6-3612-59486e000d72','cases_assigned_user','Users','users','id','Cases','cases','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('14f5841e-d70f-19a1-9449-59486e42df29','account_aos_contracts','Accounts','accounts','id','AOS_Contracts','aos_contracts','contract_account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('151f49c8-33fb-ed40-a938-59486e09bc50','securitygroups_aos_pdf_templates','SecurityGroups','securitygroups','id','AOS_PDF_Templates','aos_pdf_templates','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_PDF_Templates',0,0),('1552efa1-22e6-8746-ad8c-59486ec62cea','aor_reports_modified_user','Users','users','id','AOR_Reports','aor_reports','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('156ea55e-2421-eabf-9dfb-59486e8f5b87','favorites_created_by','Users','users','id','Favorites','favorites','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1578fe2e-ac2b-b602-a598-59486e5f0627','contact_tasks_parent','Contacts','contacts','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Contacts',0,0),('15c1daf4-600b-2813-adff-59486e0e6e13','calls_created_by','Users','users','id','Calls','calls','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1607f96b-c2a2-d56a-45ad-59486eb9cb4b','campaigns_assigned_user','Users','users','id','Campaigns','campaigns','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('176f1958-5237-a79e-5c75-59486edf9685','opportunity_currencies','Opportunities','opportunities','currency_id','Currencies','currencies','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('180fea2b-445a-2b15-734b-59486ea0ccac','projects_emails','Project','project','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('18b2099a-193f-e84b-bba7-59486e51a268','campaigns_modified_user','Users','users','id','Campaigns','campaigns','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('18ec9ecd-6605-1785-b5c3-59486e8c5d0a','aos_contracts_tasks','AOS_Contracts','aos_contracts','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','AOS_Contracts',0,0),('19110bf9-d9aa-d0ec-1dca-59486e00e64a','aos_pdf_templates_created_by','Users','users','id','AOS_PDF_Templates','aos_pdf_templates','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1917a9e9-6edd-c753-39a6-59486e3a772c','acl_roles_users','ACLRoles','acl_roles','id','Users','users','id','acl_roles_users','role_id','user_id','many-to-many',NULL,NULL,0,0),('19f0eea7-a563-6329-5e48-59486e28f6a5','emails_project_task_rel','Emails','emails','id','ProjectTask','project_task','id','emails_beans','email_id','bean_id','many-to-many','bean_module','ProjectTask',0,0),('1a10687e-df63-8a00-a499-59486ead2a66','securitygroups_hos_hospital','SecurityGroups','securitygroups','id','HOS_HOSPITAL','hos_hospital','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','HOS_HOSPITAL',0,0),('1aef1a98-b291-e35c-08a3-59486e38b07a','bug_notes','Bugs','bugs','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('1b46ef62-b0de-3894-e40f-59486ef76f1a','vis_visita_created_by','Users','users','id','VIS_VISITA','vis_visita','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1cf7d5a9-8fd5-97ea-7702-59486e221ef4','eapm_modified_user','Users','users','id','EAPM','eapm','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1d4398db-187a-38d7-9ba2-59486e6b12b4','lead_calls','Leads','leads','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('1dca6c8e-da9d-50af-496c-59486e2228ae','favorites_assigned_user','Users','users','id','Favorites','favorites','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1e7484f9-deef-750c-2b31-59486e185537','securitygroups_cases','SecurityGroups','securitygroups','id','Cases','cases','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Cases',0,0),('1efc3e91-75ba-4521-ca9d-59486ea36082','securitygroups_project_task','SecurityGroups','securitygroups','id','ProjectTask','project_task','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','ProjectTask',0,0),('1f34b8c6-5ba7-d649-cf30-59486ede9040','contact_notes_parent','Contacts','contacts','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Contacts',0,0),('1f660bd9-1f94-a57c-871c-59486e393f5a','account_aos_quotes','Accounts','accounts','id','AOS_Quotes','aos_quotes','billing_account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1f98adf2-1053-517b-ae38-59486e7b9de0','jjwg_markers_modified_user','Users','users','id','jjwg_Markers','jjwg_markers','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('1fb9c5a1-871f-2eb6-2a6a-59486eef285b','securitygroups_campaigns','SecurityGroups','securitygroups','id','Campaigns','campaigns','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Campaigns',0,0),('2097c3df-383b-4e87-226d-59486ec1bb49','aor_reports_created_by','Users','users','id','AOR_Reports','aor_reports','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('20c6871c-6090-a06a-dba1-59486e2a47a7','opportunities_campaign','Campaigns','campaigns','id','Opportunities','opportunities','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('213d0269-ff56-587c-fa12-59486e552396','tasks_modified_user','Users','users','id','Tasks','tasks','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('21459b85-d988-bf6f-ee1b-59486e778617','aod_indexevent_created_by','Users','users','id','AOD_IndexEvent','aod_indexevent','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('21fa7a97-0754-5ce7-51b3-59486e272554','email_marketing_prospect_lists','EmailMarketing','email_marketing','id','ProspectLists','prospect_lists','id','email_marketing_prospect_lists','email_marketing_id','prospect_list_id','many-to-many',NULL,NULL,0,0),('221a732a-1c41-d06c-a7ec-59486e254a77','projects_project_tasks','Project','project','id','ProjectTask','project_task','project_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('222e1b72-8a12-e26e-af8f-59486e7f5726','aos_contracts_notes','AOS_Contracts','aos_contracts','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','AOS_Contracts',0,0),('23f34359-2305-f3c2-0bc1-59486ed7cf2b','emails_projects_rel','Emails','emails','id','Project','project','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Project',0,0),('251eeafc-cd66-fd6e-b810-59486e6db5c9','bugs_release','Releases','releases','id','Bugs','bugs','found_in_release',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('25929ee1-a80d-4a9d-d546-59486eb4aa12','aos_line_item_groups_modified_user','Users','users','id','AOS_Line_Item_Groups','aos_line_item_groups','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('260220ce-2c9b-fc08-1bf6-59486e644e9e','outbound_email_modified_user','Users','users','id','OutboundEmailAccounts','outbound_email','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('26bd93c0-7f7b-5755-716d-59486e493398','lead_emails','Leads','leads','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('27876841-57cf-8843-fc7f-59486e8c3c88','eapm_created_by','Users','users','id','EAPM','eapm','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('27c0075a-c25b-af19-e91e-59486e655a96','contact_notes','Contacts','contacts','id','Notes','notes','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('27d7709e-d571-9a49-a616-59486e279fe1','case_calls','Cases','cases','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('28c15f30-7874-faa8-4092-59486e16ff01','campaign_accounts','Campaigns','campaigns','id','Accounts','accounts','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('28e91db9-9f67-67e2-0d21-59486ef7ac78','vis_visita_assigned_user','Users','users','id','VIS_VISITA','vis_visita','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('290ee722-c6d2-8487-b470-59486ef1f67b','leads_documents','Leads','leads','id','Documents','documents','id','linked_documents','parent_id','document_id','many-to-many','parent_type','Leads',0,0),('29359959-5c0b-89dd-b37d-59486e88d2c2','jjwg_markers_created_by','Users','users','id','jjwg_Markers','jjwg_markers','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2a0dfb26-21d0-961e-ba26-59486e479e1c','securitygroups_prospect_lists','SecurityGroups','securitygroups','id','ProspectLists','prospect_lists','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','ProspectLists',0,0),('2a1ad505-6f86-8e98-141c-59486e611682','tasks_created_by','Users','users','id','Tasks','tasks','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2a856047-dd94-491f-32a6-59486ebce3fe','opportunity_aos_quotes','Opportunities','opportunities','id','AOS_Quotes','aos_quotes','opportunity_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2af8c68b-c257-a148-91a6-59486e304e3f','aos_invoices_aos_product_quotes','AOS_Invoices','aos_invoices','id','AOS_Products_Quotes','aos_products_quotes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2b2179c6-1be6-e456-9ffe-59486ee91c3f','projects_assigned_user','Users','users','id','Project','project','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2b749a69-1efe-d27c-85ba-59486e63afdd','spots_modified_user','Users','users','id','Spots','spots','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2cc002f8-1040-a94e-5b65-59486ec58eb9','aor_reports_assigned_user','Users','users','id','AOR_Reports','aor_reports','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2cd7f507-b938-b63b-ba2b-59486ef95c89','emails_prospects_rel','Emails','emails','id','Prospects','prospects','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Prospects',0,0),('2ddc4ac5-ec6f-6585-14b3-59486e722147','bugs_fixed_in_release','Releases','releases','id','Bugs','bugs','fixed_in_release',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2e36754a-653d-1125-bdd4-59486ebe17dd','aos_line_item_groups_created_by','Users','users','id','AOS_Line_Item_Groups','aos_line_item_groups','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2e511ebc-94b1-23fd-cdf0-59486ec9e3ea','calls_assigned_user','Users','users','id','Calls','calls','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2eb26c09-dd4c-ffc5-8c19-59486e6fc045','aos_contracts_meetings','AOS_Contracts','aos_contracts','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','AOS_Contracts',0,0),('2ecf8f15-70c3-6377-2420-59486e58ece2','aok_knowledge_base_categories_modified_user','Users','users','id','AOK_Knowledge_Base_Categories','aok_knowledge_base_categories','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('2fccb884-dbba-bfc5-53b0-59486ea8a98a','contact_campaign_log','Contacts','contacts','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Contacts',0,0),('2ffd8425-9a78-40e0-849b-59486ec897ad','prospects_modified_user','Users','users','id','Prospects','prospects','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3034028f-fdbd-c1d6-ace1-59486ea53b69','securitygroups_assigned_user','Users','users','id','SecurityGroups','securitygroups','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('30d5907e-f886-c141-4920-59486e6b7886','eapm_assigned_user','Users','users','id','EAPM','eapm','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('30f15bf5-4a5b-a3ea-3dc8-59486e28e962','case_tasks','Cases','cases','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('30fd3211-3d95-0209-ddfc-59486eb7e9dc','outbound_email_created_by','Users','users','id','OutboundEmailAccounts','outbound_email','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3142966f-b849-fe6a-ee78-59486edf9e45','jjwg_maps_modified_user','Users','users','id','jjwg_Maps','jjwg_maps','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('32393a62-caf3-32f4-d76a-59486e173cf9','documents_accounts','Documents','documents','id','Accounts','accounts','id','documents_accounts','document_id','account_id','many-to-many',NULL,NULL,0,0),('3295cf43-16f7-8264-bf28-59486ee1adab','jjwg_markers_assigned_user','Users','users','id','jjwg_Markers','jjwg_markers','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('32a1cb7e-948d-5882-3b5a-59486e5c1708','campaign_contacts','Campaigns','campaigns','id','Contacts','contacts','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('33172af4-0943-a4cb-d61a-59486e419d92','lead_campaign_log','Leads','leads','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Leads',0,0),('336f5a12-bf19-06ca-f4bb-59486e7c43c0','tasks_assigned_user','Users','users','id','Tasks','tasks','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('33d9368b-58b4-7c73-7088-59486ea879f9','opportunity_aos_contracts','Opportunities','opportunities','id','AOS_Contracts','aos_contracts','opportunity_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('341259cb-b6ac-0e1d-290c-59486eb222bf','aos_invoices_aos_line_item_groups','AOS_Invoices','aos_invoices','id','AOS_Line_Item_Groups','aos_line_item_groups','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3435bde5-8c17-afbd-bca7-59486e7e27e8','securitygroups_users','SecurityGroups','securitygroups','id','Users','users','id','securitygroups_users','securitygroup_id','user_id','many-to-many',NULL,NULL,0,0),('354c22b9-db39-1131-f0b3-59486ed2ee65','leads_created_by','Users','users','id','Leads','leads','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('363990cf-97bf-0c10-29c5-59486ea2222a','securitygroups_calls','SecurityGroups','securitygroups','id','Calls','calls','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Calls',0,0),('364923b6-ba8f-b59d-70f1-59486e724d34','securitygroups_aor_reports','SecurityGroups','securitygroups','id','AOR_Reports','aor_reports','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOR_Reports',0,0),('368842a2-c2a9-b2c1-a30a-59486e23de33','aow_workflow_aow_conditions','AOW_WorkFlow','aow_workflow','id','AOW_Conditions','aow_conditions','aow_workflow_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('369ba624-30b8-8ca9-3e4b-59486e4ef79c','meetings_contacts','Meetings','meetings','id','Contacts','contacts','id','meetings_contacts','meeting_id','contact_id','many-to-many',NULL,NULL,0,0),('3792eff6-0d55-d4ad-bbe0-59486e013432','aos_line_item_groups_assigned_user','Users','users','id','AOS_Line_Item_Groups','aos_line_item_groups','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('38e64ac4-e770-dffb-0658-59486e002156','aok_knowledge_base_categories_created_by','Users','users','id','AOK_Knowledge_Base_Categories','aok_knowledge_base_categories','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3959c285-372a-105e-bf18-59486e6c6154','documents_contacts','Documents','documents','id','Contacts','contacts','id','documents_contacts','document_id','contact_id','many-to-many',NULL,NULL,0,0),('39c7d4b7-bd77-0deb-db19-59486e4cef75','user_direct_reports','Users','users','id','Users','users','reports_to_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3a54a3ee-040a-ba67-8486-59486e2c6720','contact_aos_quotes','Contacts','contacts','id','AOS_Quotes','aos_quotes','billing_contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3a6fb2dd-96f7-c055-fb4e-59486ebbae37','outbound_email_assigned_user','Users','users','id','OutboundEmailAccounts','outbound_email','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3aeef917-f393-861d-7ad5-59486e503142','case_notes','Cases','cases','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('3bc30b0b-aaf8-9e76-805c-59486e1d43e3','campaign_leads','Campaigns','campaigns','id','Leads','leads','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3bc32d0e-1adb-0473-bfdd-59486efa4d7f','prospects_created_by','Users','users','id','Prospects','prospects','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3be74b55-c83c-d27f-ed10-59486eb7a967','securitygroups_jjwg_markers','SecurityGroups','securitygroups','id','jjwg_Markers','jjwg_markers','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','jjwg_Markers',0,0),('3bf7cb81-8639-77bc-2a3d-59486eab3efe','spots_created_by','Users','users','id','Spots','spots','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3ca77404-3335-a8db-d4b6-59486e604fc6','projects_modified_user','Users','users','id','Project','project','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3d170b53-ef39-83aa-1e62-59486e0a48aa','securitygroups_vis_visita','SecurityGroups','securitygroups','id','VIS_VISITA','vis_visita','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','VIS_VISITA',0,0),('3d19c02e-1463-a10c-dd79-59486ea43dd9','accounts_am_projecttemplates_1','Accounts','accounts','id','AM_ProjectTemplates','am_projecttemplates','id','accounts_am_projecttemplates_1_c','accounts_am_projecttemplates_1accounts_ida','accounts_am_projecttemplates_1am_projecttemplates_idb','many-to-many',NULL,NULL,0,0),('3d528f61-3f5c-6bde-b68a-59486ec90e8f','securitygroups_tasks','SecurityGroups','securitygroups','id','Tasks','tasks','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Tasks',0,0),('3f555baa-399c-aa41-a59d-59486ece9e6f','aos_contracts_calls','AOS_Contracts','aos_contracts','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','AOS_Contracts',0,0),('3fbbc8ad-495a-3cfc-0ff2-59486e18fcb0','aor_reports_aor_fields','AOR_Reports','aor_reports','id','AOR_Fields','aor_fields','aor_report_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('3fdf9d25-a692-fe20-9027-59486edea3d8','calls_notes','Calls','calls','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Calls',0,0),('40b60d24-5241-45aa-82d9-59486e74dc2b','jjwg_maps_created_by','Users','users','id','jjwg_Maps','jjwg_maps','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('412ee30d-7a47-7542-4314-59486e18322c','groups_aos_product_quotes','AOS_Line_Item_Groups','aos_line_item_groups','id','AOS_Products_Quotes','aos_products_quotes','group_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('427a6ba5-04fb-ab0e-4970-59486ec4f94c','documents_opportunities','Documents','documents','id','Opportunities','opportunities','id','documents_opportunities','document_id','opportunity_id','many-to-many',NULL,NULL,0,0),('428256a2-7526-1725-b636-59486eaa739c','aok_knowledge_base_categories_assigned_user','Users','users','id','AOK_Knowledge_Base_Categories','aok_knowledge_base_categories','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4293eeb7-8c8d-21e3-af59-59486e22d16d','meetings_users','Meetings','meetings','id','Users','users','id','meetings_users','meeting_id','user_id','many-to-many',NULL,NULL,0,0),('42978a69-64da-96c7-4a1a-59486ed060a9','aod_indexevent_assigned_user','Users','users','id','AOD_IndexEvent','aod_indexevent','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4391dfda-88ed-6cd1-2c38-59486ea94849','users_email_addresses','Users','users','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Users',0,0),('439c8cd2-8663-966b-2e5a-59486eb532be','contact_aos_invoices','Contacts','contacts','id','AOS_Invoices','aos_invoices','billing_contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('43ea9717-417d-e336-988b-59486eb7dc9e','case_meetings','Cases','cases','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('45abf027-20eb-eeb5-92e4-59486e12ff02','spots_assigned_user','Users','users','id','Spots','spots','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('45c6d2d5-b42c-9069-dab3-59486e48fcdb','prospects_assigned_user','Users','users','id','Prospects','prospects','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('45edf818-d4b9-7618-1ae1-59486e85f092','campaign_prospects','Campaigns','campaigns','id','Prospects','prospects','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('462cd612-844d-1184-93a3-59486e117778','projects_created_by','Users','users','id','Project','project','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('46612e73-6985-ce14-3e0b-59486e6feb75','tasks_notes','Tasks','tasks','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('46e75091-cc9d-ba0e-c6f1-59486e750661','accounts_com_competidores_1','Accounts','accounts','id','COM_COMPETIDORES','com_competidores','id','accounts_com_competidores_1_c','accounts_com_competidores_1accounts_ida','accounts_com_competidores_1com_competidores_idb','many-to-many',NULL,NULL,0,0),('48f456d2-7e0c-7f4b-def2-59486e2de838','oauthkeys_modified_user','Users','users','id','OAuthKeys','oauth_consumer','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4933bc33-1881-32e7-bc2e-59486ea3998f','calls_reschedule','Calls','calls','id','Calls_Reschedule','calls_reschedule','call_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('494601c0-5714-b624-88cb-59486e8fee6c','opportunity_emails','Opportunities','opportunities','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('49475b9c-a425-1873-d602-59486ea7c26f','aor_reports_aor_conditions','AOR_Reports','aor_reports','id','AOR_Conditions','aor_conditions','aor_report_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('499c3d02-e520-f2ca-27c0-59486e0d776e','documents_cases','Documents','documents','id','Cases','cases','id','documents_cases','document_id','case_id','many-to-many',NULL,NULL,0,0),('49e36b7b-0572-2a0b-ba43-59486ed2c631','aos_contracts_aos_products_quotes','AOS_Contracts','aos_contracts','id','AOS_Products_Quotes','aos_products_quotes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4c50d31e-72d2-cef5-df6c-59486e0b2f73','meetings_leads','Meetings','meetings','id','Leads','leads','id','meetings_leads','meeting_id','lead_id','many-to-many',NULL,NULL,0,0),('4cbf0fb2-a113-02e6-e13b-59486e84b8de','pro_procedimento_modified_user','Users','users','id','PRO_PROCEDIMENTO','pro_procedimento','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4d710238-4209-7edd-be9d-59486e6f2a20','contact_aos_contracts','Contacts','contacts','id','AOS_Contracts','aos_contracts','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4d7ec507-aa36-5bb3-6ec7-59486e925148','jjwg_maps_assigned_user','Users','users','id','jjwg_Maps','jjwg_maps','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4da3d9c2-0e3e-e5f1-de61-59486ebfa99f','users_email_addresses_primary','Users','users','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('4db4876c-e334-4cf5-6611-59486e6ba29b','case_emails','Cases','cases','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('4dc0f908-abaf-3dc7-ff43-59486e0e0aa7','aow_workflow_aow_actions','AOW_WorkFlow','aow_workflow','id','AOW_Actions','aow_actions','aow_workflow_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4e0c723b-b430-ab93-efef-59486e1c39a4','aos_product_categories_modified_user','Users','users','id','AOS_Product_Categories','aos_product_categories','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4eb1a1b0-8fb7-ba31-f8e1-59486e5ee4a1','campaign_opportunities','Campaigns','campaigns','id','Opportunities','opportunities','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4ebbcdbf-7b72-d660-6674-59486ee61ab3','securitygroups_prospects','SecurityGroups','securitygroups','id','Prospects','prospects','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Prospects',0,0),('4eec744f-9145-85f9-cc6d-59486ea7d7d6','aod_index_modified_user','Users','users','id','AOD_Index','aod_index','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('4ef376bc-189c-0531-c305-59486ebeb3d6','securitygroups_spots','SecurityGroups','securitygroups','id','Spots','spots','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Spots',0,0),('501056ca-a801-606c-9f5d-59486e44801b','accounts_con_consultorios_1','Accounts','accounts','id','CON_CONSULTORIOS','con_consultorios','id','accounts_con_consultorios_1_c','accounts_con_consultorios_1accounts_ida','accounts_con_consultorios_1con_consultorios_idb','many-to-many',NULL,NULL,0,0),('516e76b7-f070-e96e-808b-59486ee1a30e','oauthkeys_created_by','Users','users','id','OAuthKeys','oauth_consumer','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('52bbd322-f0b9-f671-908c-59486efefff6','documents_bugs','Documents','documents','id','Bugs','bugs','id','documents_bugs','document_id','bug_id','many-to-many',NULL,NULL,0,0),('52cbbcdb-7871-3517-40bd-59486ef1cd54','aor_scheduled_reports_aor_reports','AOR_Reports','aor_reports','id','AOR_Scheduled_Reports','aor_scheduled_reports','aor_report_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('534a5f4e-b0e6-37ea-c198-59486ea63c15','aos_contracts_aos_line_item_groups','AOS_Contracts','aos_contracts','id','AOS_Line_Item_Groups','aos_line_item_groups','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('53e739a9-0869-171c-f5a7-59486ee37f44','aok_knowledgebase_modified_user','Users','users','id','AOK_KnowledgeBase','aok_knowledgebase','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5419857b-e205-7c5a-252a-59486e7ea8d6','fp_event_locations_modified_user','Users','users','id','FP_Event_Locations','fp_event_locations','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('55439fed-e88f-6c67-aa99-59486e011473','schedulers_created_by_rel','Users','users','id','Schedulers','schedulers','created_by',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),('554ea5fe-5ca3-6916-c49b-59486e7fe8d1','templatesectionline_modified_user','Users','users','id','TemplateSectionLine','templatesectionline','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('558d912e-2803-6f5e-7ed3-59486e96f796','opportunities_contacts','Opportunities','opportunities','id','Contacts','contacts','id','opportunities_contacts','opportunity_id','contact_id','many-to-many',NULL,NULL,0,0),('56c00f54-a076-d592-8f27-59486efb587e','contacts_aop_case_updates','Contacts','contacts','id','AOP_Case_Updates','aop_case_updates','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('56d3e0c3-9070-8973-8080-59486e7bec5e','cases_created_contact','Contacts','contacts','id','Cases','cases','contact_created_by_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('57593cef-6516-50ed-f751-59486e02cff2','accounts_bugs','Accounts','accounts','id','Bugs','bugs','id','accounts_bugs','account_id','bug_id','many-to-many',NULL,NULL,0,0),('577042c6-e4fd-6e9c-659e-59486ecd9457','pro_procedimento_created_by','Users','users','id','PRO_PROCEDIMENTO','pro_procedimento','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('57961157-e386-6a3d-4179-59486e08c609','jjwg_areas_modified_user','Users','users','id','jjwg_Areas','jjwg_areas','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('57bd0954-4c04-1096-7ba1-59486ee94388','aos_quotes_modified_user','Users','users','id','AOS_Quotes','aos_quotes','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('57cf0e93-6edd-79c5-42a8-59486ebc361e','projects_opportunities','Project','project','id','Opportunities','opportunities','id','projects_opportunities','project_id','opportunity_id','many-to-many',NULL,NULL,0,0),('58b245b3-54e7-c0e2-37ee-59486e2c28f6','campaign_email_marketing','Campaigns','campaigns','id','EmailMarketing','email_marketing','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('58c4a826-fd34-6798-45e0-59486ebaa21b','prospects_email_addresses','Prospects','prospects','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Prospects',0,0),('59ae416b-9199-983b-3b55-59486ef95a9a','aos_product_categories_created_by','Users','users','id','AOS_Product_Categories','aos_product_categories','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('59e9a3e4-1d54-ef8f-57fe-59486ec8c9a0','aok_knowledgebase_categories','AOK_KnowledgeBase','aok_knowledgebase','id','AOK_Knowledge_Base_Categories','aok_knowledge_base_categories','id','aok_knowledgebase_categories','aok_knowledgebase_id','aok_knowledge_base_categories_id','many-to-many',NULL,NULL,0,0),('5b17b67f-7c3d-807c-928c-59486e5b5921','oauthkeys_assigned_user','Users','users','id','OAuthKeys','oauth_consumer','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5d99ef94-3cf2-372e-afb8-59486efe822a','aok_knowledgebase_created_by','Users','users','id','AOK_KnowledgeBase','aok_knowledgebase','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5f0b4085-3cca-b703-6db1-59486e7d15c2','securitygroups_emailtemplates','SecurityGroups','securitygroups','id','EmailTemplates','email_templates','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','EmailTemplates',0,0),('5f1ad55e-3e60-3d8f-7aeb-59486edfeb22','templatesectionline_created_by','Users','users','id','TemplateSectionLine','templatesectionline','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5f780690-8312-2b53-574f-59486e642a4d','fp_event_locations_created_by','Users','users','id','FP_Event_Locations','fp_event_locations','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6084b96d-3d32-eeb0-5bfc-59486e732d9b','schedulers_modified_user_id_rel','Users','users','id','Schedulers','schedulers','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('60db6620-d915-3b40-4fb3-59486ee453e5','pro_procedimento_assigned_user','Users','users','id','PRO_PROCEDIMENTO','pro_procedimento','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('613a6389-1cc7-d29f-877c-59486e3be84b','campaignlog_contact','CampaignLog','campaign_log','related_id','Contacts','contacts','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('618f02ef-14f5-75a4-2270-59486eabdd4a','aos_contracts_assigned_user','Users','users','id','AOS_Contracts','aos_contracts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('61b9b8d6-d53d-4ecf-d22d-59486e558e3d','campaign_emailman','Campaigns','campaigns','id','EmailMan','emailman','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('61bca50d-07aa-fe80-f3c6-59486e29104f','accounts_modified_user','Users','users','id','Accounts','accounts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('61c5e480-87db-c704-c364-59486e66b74b','aos_quotes_created_by','Users','users','id','AOS_Quotes','aos_quotes','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('61d177a5-d665-dab6-1776-59486e1acece','accounts_contacts','Accounts','accounts','id','Contacts','contacts','id','accounts_contacts','account_id','contact_id','many-to-many',NULL,NULL,0,0),('61f2c98c-3507-0a9e-b2e6-59486e2072b7','jjwg_areas_created_by','Users','users','id','jjwg_Areas','jjwg_areas','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('62945d77-ee4e-9eb6-f3e1-59486e6490c8','securitygroups_projecttask','SecurityGroups','securitygroups','id','ProjectTask','project_task','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','ProjectTask',0,0),('62ed2ef7-039e-7aa4-0fea-59486e7eb911','aos_product_categories_assigned_user','Users','users','id','AOS_Product_Categories','aos_product_categories','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('630cdb1d-277d-e9a9-7105-59486e9094ee','am_projecttemplates_project_1','AM_ProjectTemplates','am_projecttemplates','id','Project','project','id','am_projecttemplates_project_1_c','am_projecttemplates_project_1am_projecttemplates_ida','am_projecttemplates_project_1project_idb','many-to-many',NULL,NULL,0,0),('63de11a4-3fa1-2443-d5f7-59486ec5ac02','securitygroups_jjwg_maps','SecurityGroups','securitygroups','id','jjwg_Maps','jjwg_maps','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','jjwg_Maps',0,0),('65c21748-6988-f466-98a9-59486ec08757','aor_fields_modified_user','Users','users','id','AOR_Fields','aor_fields','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('670211ff-3ac9-53ba-c5b8-59486e46b408','aok_knowledgebase_assigned_user','Users','users','id','AOK_KnowledgeBase','aok_knowledgebase','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('678a0f7f-69f8-318b-972f-59486ec9dbb8','securitygroups_meetings','SecurityGroups','securitygroups','id','Meetings','meetings','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Meetings',0,0),('67f64bea-c77c-47b2-62a9-59486e955e7a','aow_workflow_aow_processed','AOW_WorkFlow','aow_workflow','id','AOW_Processed','aow_processed','aow_workflow_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('68d9a990-92db-0a69-89d6-59486e086467','fp_event_locations_assigned_user','Users','users','id','FP_Event_Locations','fp_event_locations','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('68f641b4-d563-88b8-afbc-59486e2b01d4','emailtemplates_assigned_user','Users','users','id','EmailTemplates','email_templates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('69994e2a-e969-122c-3006-59486ef3eb4b','schedulers_jobs_rel','Schedulers','schedulers','id','SchedulersJobs','job_queue','scheduler_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('69aedf7b-37f6-638b-b49a-59486e57a0c1','accounts_created_by','Users','users','id','Accounts','accounts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6a32b0eb-b09d-db18-9741-59486edbb4b3','am_projecttemplates_contacts_1','AM_ProjectTemplates','am_projecttemplates','id','Contacts','contacts','id','am_projecttemplates_contacts_1_c','am_projecttemplates_ida','contacts_idb','many-to-many',NULL,NULL,0,0),('6af43354-3ab8-5187-616b-59486e5b26b5','aos_quotes_assigned_user','Users','users','id','AOS_Quotes','aos_quotes','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6b4d3539-6e43-2d27-d13f-59486ef062c7','jjwg_areas_assigned_user','Users','users','id','jjwg_Areas','jjwg_areas','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6b4d5ffd-e1c5-0f02-2a53-59486e7b480e','project_tasks_notes','ProjectTask','project_task','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('6b7ee17a-2ade-9db4-a679-59486e5efd12','campaignlog_lead','CampaignLog','campaign_log','related_id','Leads','leads','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6ba2713c-f9be-55d4-9a6c-59486eaceaaf','campaign_campaignlog','Campaigns','campaigns','id','CampaignLog','campaign_log','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6cb0376b-443d-80c8-3e0d-59486e7ab8ec','securitygroups_aos_product_categories','SecurityGroups','securitygroups','id','AOS_Product_Categories','aos_product_categories','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_Product_Categories',0,0),('6d4c0279-8247-eaf5-9c42-59486e84b5af','accounts_opportunities','Accounts','accounts','id','Opportunities','opportunities','id','accounts_opportunities','account_id','opportunity_id','many-to-many',NULL,NULL,0,0),('6dd3616b-b8d4-faac-b3ea-59486e6fd2a5','aod_index_created_by','Users','users','id','AOD_Index','aod_index','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6de93a10-520f-f056-8b3e-59486e80050e','securitygroups_pro_procedimento','SecurityGroups','securitygroups','id','PRO_PROCEDIMENTO','pro_procedimento','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','PRO_PROCEDIMENTO',0,0),('6e13eb04-97de-ee4a-1fc5-59486ef3acb8','aor_fields_created_by','Users','users','id','AOR_Fields','aor_fields','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6e5b5979-66c8-2c5f-db95-59486e72a902','securitygroups_emails','SecurityGroups','securitygroups','id','Emails','emails','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Emails',0,0),('6e7ecb2c-93ae-f413-e89d-59486e21c93a','jjwg_Maps_accounts','jjwg_Maps','jjwg_Maps','parent_id','Accounts','accounts','id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('6ea90333-ecbf-5f48-a64b-59486eda3141','prospects_email_addresses_primary','Prospects','prospects','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('706deeff-2fd0-93a9-5f69-59486e3911cc','securitygroups_aok_knowledgebase','SecurityGroups','securitygroups','id','AOK_KnowledgeBase','aok_knowledgebase','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOK_KnowledgeBase',0,0),('7238223a-0a9a-0907-9aa8-59486e158f02','consumer_tokens','OAuthKeys','oauth_consumer','id','OAuthTokens','oauth_tokens','consumer',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('72819ec2-a089-5c98-22d6-59486ed4d522','securitygroups_fp_event_locations','SecurityGroups','securitygroups','id','FP_Event_Locations','fp_event_locations','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','FP_Event_Locations',0,0),('72b6017b-a03c-f293-d8f5-59486e924ba7','emails_tasks_rel','Emails','emails','id','Tasks','tasks','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Tasks',0,0),('734f4246-3e3f-c54a-2a9e-59486e54df38','am_projecttemplates_users_1','AM_ProjectTemplates','am_projecttemplates','id','Users','users','id','am_projecttemplates_users_1_c','am_projecttemplates_ida','users_idb','many-to-many',NULL,NULL,0,0),('7350f22d-09f2-1889-5d7b-59486e2bf13d','accounts_assigned_user','Users','users','id','Accounts','accounts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('744bb9f1-382b-dd83-1867-59486e41c87a','project_tasks_tasks','ProjectTask','project_task','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('745f3803-c634-f411-60a0-59486ec14eca','campaignlog_created_opportunities','CampaignLog','campaign_log','related_id','Opportunities','opportunities','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('74bbdfc3-daa2-fad1-ac2b-59486e46bbb2','securitygroups_aos_quotes','SecurityGroups','securitygroups','id','AOS_Quotes','aos_quotes','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_Quotes',0,0),('74c9454b-5418-29cd-13f1-59486e5e9a3a','campaign_assigned_user','Users','users','id','Campaigns','campaigns','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('74d541b3-df83-8d60-c7cd-59486eb8bd3e','schedulersjobs_assigned_user','Users','users','id','SchedulersJobs','job_queue','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('74e684c0-4bf2-96cf-5187-59486e3857b4','securitygroups_jjwg_areas','SecurityGroups','securitygroups','id','jjwg_Areas','jjwg_areas','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','jjwg_Areas',0,0),('75eefac9-7253-f477-1cc0-59486e657ed2','sub_product_categories','AOS_Product_Categories','aos_product_categories','id','AOS_Product_Categories','aos_product_categories','parent_category_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('75fefac7-5787-40a2-6a7b-59486e006e77','com_competidores_modified_user','Users','users','id','COM_COMPETIDORES','com_competidores','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('77352db0-159c-2684-63eb-59486e7620d6','prospect_list_campaigns','ProspectLists','prospect_lists','id','Campaigns','campaigns','id','prospect_list_campaigns','prospect_list_id','campaign_id','many-to-many',NULL,NULL,0,0),('776ba79f-0e84-4803-457c-59486e4f6eb7','aod_index_assigned_user','Users','users','id','AOD_Index','aod_index','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('777aada5-a9a8-fb5d-3d9f-59486ecefcca','emails_assigned_user','Users','users','id','Emails','emails','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('77cb535d-d694-3890-ab35-59486e97530d','opportunities_modified_user','Users','users','id','Opportunities','opportunities','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('781d0202-d545-9333-6390-59486e1cf158','calls_modified_user','Users','users','id','Calls','calls','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('785b05bd-544d-0aa7-4495-59486e2cbdd9','leads_modified_user','Users','users','id','Leads','leads','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7872c3f7-d0f5-cefe-cad7-59486ea152e0','aobh_businesshours_modified_user','Users','users','id','AOBH_BusinessHours','aobh_businesshours','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('79542b47-9ac7-4fc7-0393-59486e975cd6','prospect_tasks','Prospects','prospects','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('79868c91-b310-e1ef-0d9b-59486e0e8736','calls_contacts','Calls','calls','id','Contacts','contacts','id','calls_contacts','call_id','contact_id','many-to-many',NULL,NULL,0,0),('7a24f405-1c86-a9cd-6eae-59486e7de45e','leads_assigned_user','Users','users','id','Leads','leads','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7a8094f6-992c-30ef-46fe-59486e847979','am_tasktemplates_am_projecttemplates','AM_ProjectTemplates','am_projecttemplates','id','AM_TaskTemplates','am_tasktemplates','id','am_tasktemplates_am_projecttemplates_c','am_tasktemplates_am_projecttemplatesam_projecttemplates_ida','am_tasktemplates_am_projecttemplatesam_tasktemplates_idb','many-to-many',NULL,NULL,0,0),('7ada663e-43fd-49d7-db86-59486ef07899','alerts_modified_user','Users','users','id','Alerts','alerts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7bbe2f7d-8067-fe89-9949-59486e276d3e','oauthtokens_assigned_user','Users','users','id','OAuthTokens','oauth_tokens','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7c8f1180-a7e4-c53a-bbec-59486e0f1517','securitygroups_accounts','SecurityGroups','securitygroups','id','Accounts','accounts','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Accounts',0,0),('7d004643-1094-7910-adcb-59486eca8687','aor_charts_modified_user','Users','users','id','AOR_Charts','aor_charts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7df5a17e-f03e-8c25-c1f8-59486e81a4bb','aos_quotes_aos_product_quotes','AOS_Quotes','aos_quotes','id','AOS_Products_Quotes','aos_products_quotes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7e470a06-28ce-c5a2-455f-59486ec22759','bug_calls','Bugs','bugs','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('7e4d8bb1-2068-19c2-f31c-59486ec4564a','project_tasks_meetings','ProjectTask','project_task','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('7e813a6f-5637-aa36-72a5-59486e3eec88','campaignlog_targeted_users','CampaignLog','campaign_log','target_id','Users','users','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7f000085-4de7-a018-d948-59486eaa24c8','project_users_1','Project','project','id','Users','users','id','project_users_1_c','project_users_1project_ida','project_users_1users_idb','many-to-many',NULL,NULL,0,0),('7fd30139-6d06-d50d-4640-59486e56940a','prospect_list_contacts','ProspectLists','prospect_lists','id','Contacts','contacts','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Contacts',0,0),('80f51769-4f05-b5b8-8c9a-59486ee035e3','emails_modified_user','Users','users','id','Emails','emails','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('81faa90c-6965-1240-0b16-59486e839e98','reminders_modified_user','Users','users','id','Reminders','reminders','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('825fd2d0-d5b6-0ff3-011f-59486e0ec656','com_competidores_created_by','Users','users','id','COM_COMPETIDORES','com_competidores','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('82c9eead-def2-f1c3-64b0-59486e8d9884','opportunities_created_by','Users','users','id','Opportunities','opportunities','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('83929586-77e2-8ccc-2643-59486eb2a4af','aos_contracts_documents','AOS_Contracts','aos_contracts','id','Documents','documents','id','aos_contracts_documents','aos_contracts_id','documents_id','many-to-many',NULL,NULL,0,0),('83dc5f24-6950-be2c-c268-59486e441d4f','aobh_businesshours_created_by','Users','users','id','AOBH_BusinessHours','aobh_businesshours','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('84c9ae97-cb3b-15b4-3e7d-59486e5075e7','alerts_created_by','Users','users','id','Alerts','alerts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('85e32ef9-04dd-5d43-e95f-59486e1f5391','calls_users','Calls','calls','id','Users','users','id','calls_users','call_id','user_id','many-to-many',NULL,NULL,0,0),('86614b9f-b3b6-b1a0-9e16-59486e6a76fe','aor_charts_created_by','Users','users','id','AOR_Charts','aor_charts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('86b6793d-6db5-2b31-5d76-59486e6bc664','accounts_email_addresses','Accounts','accounts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Accounts',0,0),('87499910-f742-8072-6a24-59486e0c3f7d','project_tasks_calls','ProjectTask','project_task','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('875bfc2f-a67b-6af4-e5a4-59486e3617fb','campaignlog_sent_emails','CampaignLog','campaign_log','related_id','Emails','emails','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('87bf3bdd-c775-5fb4-9a7f-59486ee75d0a','aos_quotes_aos_line_item_groups','AOS_Quotes','aos_quotes','id','AOS_Line_Item_Groups','aos_line_item_groups','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('87c2a531-9406-005a-e8c6-59486ea0c593','prospect_notes','Prospects','prospects','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('885d9b15-a772-7560-2051-59486e3b886d','jjwg_address_cache_modified_user','Users','users','id','jjwg_Address_Cache','jjwg_address_cache','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('88d99984-11e5-c0c4-4f46-59486ec90d95','jjwg_Maps_contacts','jjwg_Maps','jjwg_Maps','parent_id','Contacts','contacts','id',NULL,NULL,NULL,'one-to-many','parent_type','Contacts',0,0),('88f1841a-5eb3-840a-2861-59486eef0119','prospect_list_prospects','ProspectLists','prospect_lists','id','Prospects','prospects','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Prospects',0,0),('8a55c172-8131-8d12-367f-59486eeba61b','emails_created_by','Users','users','id','Emails','emails','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8acbf0dd-2219-69fc-a54f-59486e5b361d','aos_quotes_aos_contracts','AOS_Quotes','aos_quotes','id','AOS_Contracts','aos_contracts','id','aos_quotes_os_contracts_c','aos_quotese81e_quotes_ida','aos_quotes4dc0ntracts_idb','many-to-many',NULL,NULL,0,0),('8b440fc1-99d1-57d5-04c4-59486e477104','hos_hospital_assigned_user','Users','users','id','HOS_HOSPITAL','hos_hospital','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8ba14d1b-9c19-7232-35d8-59486edbf83a','reminders_created_by','Users','users','id','Reminders','reminders','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8ba76c64-bf3a-9b9a-b818-59486ec07613','com_competidores_assigned_user','Users','users','id','COM_COMPETIDORES','com_competidores','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8e651efb-3b7c-efe8-03b8-59486e4d88eb','alerts_assigned_user','Users','users','id','Alerts','alerts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8f95f6a4-4164-c2bf-e5d0-59486e14e821','accounts_email_addresses_primary','Accounts','accounts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('8fd2b5c0-e381-90c7-45f3-59486e0792d4','aor_charts_aor_reports','AOR_Reports','aor_reports','id','AOR_Charts','aor_charts','aor_report_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('9017479b-e31a-2129-71cc-59486ee88a47','prospect_list_leads','ProspectLists','prospect_lists','id','Leads','leads','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Leads',0,0),('9155c447-7b6a-7a8f-1e78-59486eb46670','project_tasks_emails','ProjectTask','project_task','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('918120ea-2c43-0814-8c6a-59486ec0e0ad','jjwg_Maps_leads','jjwg_Maps','jjwg_Maps','parent_id','Leads','leads','id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('91b4dd7b-63e0-fbec-32c5-59486e4101e2','prospect_meetings','Prospects','prospects','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('92143dd6-6307-2a1f-113c-59486ec6ce6b','aop_case_events_modified_user','Users','users','id','AOP_Case_Events','aop_case_events','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('92710a3a-355b-2fad-6f88-59486e76231a','lead_notes','Leads','leads','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('92c919b2-1807-29c7-76d3-59486e281abd','aos_products_modified_user','Users','users','id','AOS_Products','aos_products','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('92fd5688-7b3d-fb6c-7d73-59486ecc9c7a','jjwg_address_cache_created_by','Users','users','id','jjwg_Address_Cache','jjwg_address_cache','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('93d82f03-e637-2c54-50af-59486ea54d3a','aos_quotes_aos_invoices','AOS_Quotes','aos_quotes','id','AOS_Invoices','aos_invoices','id','aos_quotes_aos_invoices_c','aos_quotes77d9_quotes_ida','aos_quotes6b83nvoices_idb','many-to-many',NULL,NULL,0,0),('93fd288a-1360-9890-ef00-59486e0a94c5','emails_notes_rel','Emails','emails','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('948571c4-7e36-23f7-5213-59486e86c677','am_projecttemplates_modified_user','Users','users','id','AM_ProjectTemplates','am_projecttemplates','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('95251347-5c9c-9db0-cbb4-59486ebdd0b7','reminders_assigned_user','Users','users','id','Reminders','reminders','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('985ac93d-9ce2-662d-6290-59486ec2dc01','securitygroups_com_competidores','SecurityGroups','securitygroups','id','COM_COMPETIDORES','com_competidores','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','COM_COMPETIDORES',0,0),('9898e5dc-6334-943f-dc1b-59486e20f050','aow_processed_modified_user','Users','users','id','AOW_Processed','aow_processed','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('993c384a-deef-bb7e-cbc1-59486e9678df','member_accounts','Accounts','accounts','id','Accounts','accounts','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('999d33f6-fe8d-8be5-4db3-59486eaeddda','pme_procedimento_medico_modified_user','Users','users','id','PME_PROCEDIMENTO_MEDICO','pme_procedimento_medico','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('99b5002c-baba-501c-038c-59486e46f148','securitygroups_acl_roles','SecurityGroups','securitygroups','id','ACLRoles','acl_roles','id','securitygroups_acl_roles','securitygroup_id','role_id','many-to-many',NULL,NULL,0,0),('9a4bd276-832b-99bc-b4cd-59486e78556d','project_tasks_assigned_user','Users','users','id','ProjectTask','project_task','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('9aab393a-8ef6-2cd1-8bd0-59486e41558a','aop_case_events_created_by','Users','users','id','AOP_Case_Events','aop_case_events','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('9ac449b9-5e28-96f1-e8bf-59486e53ce18','inbound_email_created_by','Users','users','id','InboundEmail','inbound_email','created_by',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),('9ad45ae3-a86a-7982-1deb-59486ea82b04','prospect_calls','Prospects','prospects','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('9b11287e-8090-bc5c-24c8-59486ea92b0c','aos_quotes_project','AOS_Quotes','aos_quotes','id','Project','project','id','aos_quotes_project_c','aos_quotes1112_quotes_ida','aos_quotes7207project_idb','many-to-many',NULL,NULL,0,0),('9b17715d-4781-507e-a793-59486eb332a8','calls_leads','Calls','calls','id','Leads','leads','id','calls_leads','call_id','lead_id','many-to-many',NULL,NULL,0,0),('9b96f478-491b-ca8d-0554-59486e8f680a','campaign_modified_user','Users','users','id','Campaigns','campaigns','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('9bf581e9-2db5-2969-f9cb-59486e364c63','prospect_list_users','ProspectLists','prospect_lists','id','Users','users','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Users',0,0),('9c36ed81-6560-541a-000d-59486eec4c5b','jjwg_address_cache_assigned_user','Users','users','id','jjwg_Address_Cache','jjwg_address_cache','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('9c7d2453-2625-977f-1722-59486e9f4d6e','notes_assigned_user','Users','users','id','Notes','notes','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('9d6174e7-f9d8-9063-49fc-59486e228f4b','emails_contacts_rel','Emails','emails','id','Contacts','contacts','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Contacts',0,0),('9d841dfe-d707-f955-bb95-59486ed5d3d5','aos_products_created_by','Users','users','id','AOS_Products','aos_products','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('9ed1cd5f-3c27-531a-1d8a-59486edff97e','am_projecttemplates_created_by','Users','users','id','AM_ProjectTemplates','am_projecttemplates','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('9f2bc72c-b4bb-2211-f90b-59486ea93643','aow_actions_modified_user','Users','users','id','AOW_Actions','aow_actions','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a1ef8f0f-ae8f-5252-55fa-59486ee0b7d3','aow_processed_created_by','Users','users','id','AOW_Processed','aow_processed','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a36ba8a0-64f7-61b1-0d61-59486eb0130e','pme_procedimento_medico_created_by','Users','users','id','PME_PROCEDIMENTO_MEDICO','pme_procedimento_medico','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a4266332-bdc6-d684-04ab-59486ecee4da','aow_processed_aow_actions','AOW_Processed','aow_processed','id','AOW_Actions','aow_actions','id','aow_processed_aow_actions','aow_processed_id','aow_action_id','many-to-many',NULL,NULL,0,0),('a43f03ac-3f7b-9d90-4cb8-59486e38ad00','aop_case_events_assigned_user','Users','users','id','AOP_Case_Events','aop_case_events','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a4b80d01-9cee-8bee-9259-59486ea24016','project_tasks_modified_user','Users','users','id','ProjectTask','project_task','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a4bbbb39-a3f7-6a33-8bfd-59486e11427b','prospect_emails','Prospects','prospects','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('a510f520-5c0a-b387-ce48-59486eca3177','aos_pdf_templates_modified_user','Users','users','id','AOS_PDF_Templates','aos_pdf_templates','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a52d6a7c-e30b-1e4d-10e0-59486e26fe3c','jjwg_Maps_opportunities','jjwg_Maps','jjwg_Maps','parent_id','Opportunities','opportunities','id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('a54852b2-95a6-692f-8d46-59486e78280f','optimistic_locking',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),('a5851ffe-7f0e-4178-aa21-59486e36dc71','reminders_invitees_modified_user','Users','users','id','Reminders_Invitees','reminders_invitees','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a5d02e40-31ac-0534-7880-59486edcfcfa','inbound_email_modified_user_id','Users','users','id','InboundEmail','inbound_email','modified_user_id',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),('a5d21ee8-ae13-73ff-a86d-59486e05482d','aor_conditions_modified_user','Users','users','id','AOR_Conditions','aor_conditions','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a604e4a0-2d3e-c398-f1ba-59486e446dbf','account_cases','Accounts','accounts','id','Cases','cases','account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a60fedb6-3331-a093-f4d8-59486e0e7ff3','prospect_list_accounts','ProspectLists','prospect_lists','id','Accounts','accounts','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Accounts',0,0),('a6f777cc-17a3-07a2-d73a-59486e494962','aos_products_assigned_user','Users','users','id','AOS_Products','aos_products','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a6fff755-97c0-79a4-235c-59486e44931c','emails_accounts_rel','Emails','emails','id','Accounts','accounts','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Accounts',0,0),('a7f335f4-8691-8214-e03d-59486ebdd6cd','securitygroups_project','SecurityGroups','securitygroups','id','Project','project','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Project',0,0),('a83c2d63-81ef-3826-e9ed-59486e2f00bb','am_projecttemplates_assigned_user','Users','users','id','AM_ProjectTemplates','am_projecttemplates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a85903dd-86b7-06b0-1200-59486e48bb18','contacts_modified_user','Users','users','id','Contacts','contacts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('a98f0714-69ef-865f-82b5-59486e89e2c9','cases_bugs','Cases','cases','id','Bugs','bugs','id','cases_bugs','case_id','bug_id','many-to-many',NULL,NULL,0,0),('aaa0e986-6db4-7401-1488-59486e999a00','sugarfeed_modified_user','Users','users','id','SugarFeed','sugarfeed','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ab652c30-f577-fce7-b32b-59486ee1e027','fp_event_locations_fp_events_1','FP_Event_Locations','fp_event_locations','id','FP_events','fp_events','id','fp_event_locations_fp_events_1_c','fp_event_locations_fp_events_1fp_event_locations_ida','fp_event_locations_fp_events_1fp_events_idb','many-to-many',NULL,NULL,0,0),('ac42371b-fadf-1b6d-ecb2-59486ee8a594','aow_actions_created_by','Users','users','id','AOW_Actions','aow_actions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ac639258-887e-65a2-c6bd-59486eec0a23','documents_modified_user','Users','users','id','Documents','documents','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ac976655-7440-8a13-4f18-59486eedc54f','pme_procedimento_medico_assigned_user','Users','users','id','PME_PROCEDIMENTO_MEDICO','pme_procedimento_medico','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('acd3052f-9645-ce7c-9bb9-59486e954e4e','con_consultorios_modified_user','Users','users','id','CON_CONSULTORIOS','con_consultorios','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ad9149bb-e942-e3d4-583e-59486e1a2526','reminders_invitees_created_by','Users','users','id','Reminders_Invitees','reminders_invitees','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('adbba00b-97a4-c609-d4aa-59486e0049b0','project_tasks_created_by','Users','users','id','ProjectTask','project_task','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('adc0fc55-5afe-e4d6-2a5e-59486e07556c','prospect_campaign_log','Prospects','prospects','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Prospects',0,0),('addf4e79-13b9-2041-12d4-59486e87717b','cases_aop_case_events','Cases','cases','id','AOP_Case_Events','aop_case_events','case_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ae360d29-eeb9-1f6c-a83e-59486e3e358a','cases_created_by','Users','users','id','Cases','cases','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('af4a56d3-c984-4db1-86f1-59486e568bd9','jjwg_Maps_cases','jjwg_Maps','jjwg_Maps','parent_id','Cases','cases','id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('afac9f4a-30e9-91cd-e0f7-59486e9397e5','unified_search',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0),('afd348a4-2343-2ad3-1294-59486e4bdd36','securitygroups_leads','SecurityGroups','securitygroups','id','Leads','leads','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Leads',0,0),('b05e07de-00b2-f227-8fe8-59486edb558c','aor_conditions_created_by','Users','users','id','AOR_Conditions','aor_conditions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b082cc04-048a-a5a6-d8f3-59486ebfdeb3','securitygroups_aos_products','SecurityGroups','securitygroups','id','AOS_Products','aos_products','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_Products',0,0),('b090fa89-3568-b17b-8905-59486ee574ec','emails_leads_rel','Emails','emails','id','Leads','leads','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Leads',0,0),('b1bd5d4f-bed7-67d6-8600-59486e5a13c9','roles_users','Roles','roles','id','Users','users','id','roles_users','role_id','user_id','many-to-many',NULL,NULL,0,0),('b23b0481-9645-6076-05f5-59486e890fe4','account_aos_invoices','Accounts','accounts','id','AOS_Invoices','aos_invoices','billing_account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b2db62f0-9194-f9e1-961b-59486e162fed','account_tasks','Accounts','accounts','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('b2ff5f08-4de9-6149-c101-59486e5ecd53','projects_notes','Project','project','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('b3753b02-d8a6-6d2e-21d7-59486efc42cb','contacts_bugs','Contacts','contacts','id','Bugs','bugs','id','contacts_bugs','contact_id','bug_id','many-to-many',NULL,NULL,0,0),('b3abd4cf-7c75-db76-58e7-59486e561703','calls_reschedule_modified_user','Users','users','id','Calls_Reschedule','calls_reschedule','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b3f37504-a84a-7640-a7de-59486ea87220','bugs_modified_user','Users','users','id','Bugs','bugs','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b4692946-7cdc-de4d-c6ff-59486e779e40','fp_events_contacts','FP_events','fp_events','id','Contacts','contacts','id','fp_events_contacts_c','fp_events_contactsfp_events_ida','fp_events_contactscontacts_idb','many-to-many',NULL,NULL,0,0),('b4c70008-bae8-63f6-3912-59486ea4f4b0','sugarfeed_created_by','Users','users','id','SugarFeed','sugarfeed','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b4df0c38-c3c8-8c71-b656-59486e60b440','contacts_created_by','Users','users','id','Contacts','contacts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b50bbfe2-7f7d-f266-0704-59486e5abb76','securitygroups_notes','SecurityGroups','securitygroups','id','Notes','notes','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Notes',0,0),('b5fa32b6-31d2-610f-ec96-59486e1a5f0f','con_consultorios_created_by','Users','users','id','CON_CONSULTORIOS','con_consultorios','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b6797a5b-c139-5b63-885d-59486e32c8f9','securitygroups_pme_procedimento_medico','SecurityGroups','securitygroups','id','PME_PROCEDIMENTO_MEDICO','pme_procedimento_medico','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','PME_PROCEDIMENTO_MEDICO',0,0),('b71fa7e3-4298-8473-c646-59486ef9e92f','documents_created_by','Users','users','id','Documents','documents','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b73cf197-a2d5-6506-0323-59486edde1f9','reminders_invitees_assigned_user','Users','users','id','Reminders_Invitees','reminders_invitees','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b88f553f-8bf7-1179-20bd-59486ee4e043','leads_email_addresses','Leads','leads','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Leads',0,0),('b8a4848d-92d9-ddfc-8be9-59486ed04c26','opportunities_assigned_user','Users','users','id','Opportunities','opportunities','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b91f031f-3688-e48e-c39d-59486e7d9af3','aos_pdf_templates_assigned_user','Users','users','id','AOS_PDF_Templates','aos_pdf_templates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b92c572b-4060-2270-1930-59486e3e4305','jjwg_Maps_projects','jjwg_Maps','jjwg_Maps','parent_id','Project','project','id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('b9f31299-d743-a634-99d3-59486e07038b','product_categories','AOS_Product_Categories','aos_product_categories','id','AOS_Products','aos_products','aos_product_category_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('b9f41141-d519-189c-822b-59486edc4a29','emails_aos_contracts_rel','Emails','emails','id','AOS_Contracts','aos_contracts','id','emails_beans','email_id','bean_id','many-to-many','bean_module','AOS_Contracts',0,0),('bb8565b9-2ddb-ccb0-1824-59486e9285ea','prospectlists_assigned_user','Users','users','id','prospectlists','prospect_lists','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('bba0f0c9-5f5d-aacc-1e42-59486e2c17d5','fp_events_fp_event_locations_1','FP_events','fp_events','id','FP_Event_Locations','fp_event_locations','id','fp_events_fp_event_locations_1_c','fp_events_fp_event_locations_1fp_events_ida','fp_events_fp_event_locations_1fp_event_locations_idb','many-to-many',NULL,NULL,0,0),('bd7c58d9-0962-3224-87d2-59486e3599c0','account_notes','Accounts','accounts','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('bdb6f825-ed31-3518-f2d2-59486ec36d52','contacts_assigned_user','Users','users','id','Contacts','contacts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('be23c5e5-d6b4-63a8-c4a9-59486e3010db','notes_modified_user','Users','users','id','Notes','notes','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('be3892e3-d121-3c91-6e71-59486ef78474','calls_reschedule_created_by','Users','users','id','Calls_Reschedule','calls_reschedule','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('be44d22e-584c-0581-99a5-59486ef4d0ae','sugarfeed_assigned_user','Users','users','id','SugarFeed','sugarfeed','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('be6e9b11-6c0f-6345-9cea-59486ef31588','projects_bugs','Project','project','id','Bugs','bugs','id','projects_bugs','project_id','bug_id','many-to-many',NULL,NULL,0,0),('beeb04b8-a866-a45a-e149-59486e163c51','aor_scheduled_reports_modified_user','Users','users','id','AOR_Scheduled_Reports','aor_scheduled_reports','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('bf4dfafb-190a-af1c-5a21-59486ef9929c','con_consultorios_assigned_user','Users','users','id','CON_CONSULTORIOS','con_consultorios','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('bfe89d78-bb9b-8930-0e7f-59486ec20c1f','contacts_cases','Contacts','contacts','id','Cases','cases','id','contacts_cases','contact_id','case_id','many-to-many',NULL,NULL,0,0),('c079b65b-c6bc-13db-748d-59486e111176','documents_assigned_user','Users','users','id','Documents','documents','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c27bdc50-6c74-da7a-a137-59486e507f25','jjwg_Maps_meetings','jjwg_Maps','jjwg_Maps','parent_id','Meetings','meetings','id',NULL,NULL,NULL,'one-to-many','parent_type','Meetings',0,0),('c28db678-372e-28f1-275c-59486e6fd601','leads_email_addresses_primary','Leads','leads','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('c3b0028c-70dc-a7d3-d9ea-59486ec034bf','contact_tasks','Contacts','contacts','id','Tasks','tasks','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c3c3052b-5eb1-08fd-f29c-59486e838695','emails_meetings_rel','Emails','emails','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c3c49496-7f5c-ff0e-a4cc-59486efd230f','securitygroups_opportunities','SecurityGroups','securitygroups','id','Opportunities','opportunities','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Opportunities',0,0),('c48b2d7f-c424-ef7c-bb5d-59486e86c674','aop_case_updates_modified_user','Users','users','id','AOP_Case_Updates','aop_case_updates','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c4b18876-1d76-b2aa-6e32-59486e1dc0af','fp_events_leads_1','FP_events','fp_events','id','Leads','leads','id','fp_events_leads_1_c','fp_events_leads_1fp_events_ida','fp_events_leads_1leads_idb','many-to-many',NULL,NULL,0,0),('c533e1ad-6c37-668e-682c-59486e88d5fe','securitygroups_prospectlists','SecurityGroups','securitygroups','id','ProspectLists','prospect_lists','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','ProspectLists',0,0),('c63b497e-7ed4-ce82-3818-59486e5bc98f','account_meetings','Accounts','accounts','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('c687cd99-911c-9e0d-0456-59486e324ae6','aow_workflow_modified_user','Users','users','id','AOW_WorkFlow','aow_workflow','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c775f0db-0680-c3b0-4458-59486ee49cad','securitygroups_contacts','SecurityGroups','securitygroups','id','Contacts','contacts','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Contacts',0,0),('c8429735-b335-0c70-ffa7-59486e1bc5f3','am_tasktemplates_modified_user','Users','users','id','AM_TaskTemplates','am_tasktemplates','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c8fc5363-be6b-9a44-55db-59486ee30c3a','calls_reschedule_assigned_user','Users','users','id','Calls_Reschedule','calls_reschedule','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('c91eb914-059a-c4de-1066-59486ef25e74','securitygroups_con_consultorios','SecurityGroups','securitygroups','id','CON_CONSULTORIOS','con_consultorios','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','CON_CONSULTORIOS',0,0),('ca1d70a1-d955-f5da-1522-59486e4f3f6f','securitygroups_documents','SecurityGroups','securitygroups','id','Documents','documents','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Documents',0,0),('cb0d1871-2df2-c5ec-8715-59486e2f2f5b','fp_events_modified_user','Users','users','id','FP_events','fp_events','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('cb210892-78e7-6169-1d21-59486ee7fb2b','campaigns_created_by','Users','users','id','Campaigns','campaigns','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('cb852505-22e5-1fea-88be-59486e715b57','aor_scheduled_reports_created_by','Users','users','id','AOR_Scheduled_Reports','aor_scheduled_reports','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('cb8995fc-e994-22dc-1637-59486e510e53','lead_direct_reports','Leads','leads','id','Leads','leads','reports_to_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('cbc8133b-33b5-ae84-ac68-59486ed459c7','aos_invoices_modified_user','Users','users','id','AOS_Invoices','aos_invoices','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('cbfc03a0-f0e4-c06f-d6ef-59486ef752e2','jjwg_Maps_prospects','jjwg_Maps','jjwg_Maps','parent_id','Prospects','prospects','id',NULL,NULL,NULL,'one-to-many','parent_type','Prospects',0,0),('cc90e584-ac66-8b83-851d-59486e028c18','saved_search_assigned_user','Users','users','id','SavedSearch','saved_search','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('cd1e4f42-bf53-d9b1-8aba-59486edda2ac','bugs_created_by','Users','users','id','Bugs','bugs','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('cd6bca32-cdc9-3692-4252-59486e75cfa8','opportunity_calls','Opportunities','opportunities','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('cdcbaae1-42a9-1bc9-942d-59486ee0a1a0','contacts_users','Contacts','contacts','id','Users','users','id','contacts_users','contact_id','user_id','many-to-many',NULL,NULL,0,0),('ce3d7d7a-124c-d499-e8b6-59486e2187dc','aow_conditions_modified_user','Users','users','id','AOW_Conditions','aow_conditions','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ce474f4e-7bff-69c6-92ee-59486e809095','aop_case_updates_created_by','Users','users','id','AOP_Case_Updates','aop_case_updates','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ce4e5a0e-08d8-f850-cf60-59486e7e9f1b','projects_cases','Project','project','id','Cases','cases','id','projects_cases','project_id','case_id','many-to-many',NULL,NULL,0,0),('ce89cdb7-6e70-3e11-afdf-59486e463c0c','fp_events_prospects_1','FP_events','fp_events','id','Prospects','prospects','id','fp_events_prospects_1_c','fp_events_prospects_1fp_events_ida','fp_events_prospects_1prospects_idb','many-to-many',NULL,NULL,0,0),('d01901a4-c804-1427-cbfb-59486e8ab252','account_calls','Accounts','accounts','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('d093fd83-6e55-c264-754c-59486ec77772','contacts_email_addresses','Contacts','contacts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Contacts',0,0),('d2070ab0-79e6-5d80-9564-59486ee18158','vis_visita_modified_user','Users','users','id','VIS_VISITA','vis_visita','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d29e7b95-2b68-fa0c-6145-59486e94450f','notes_created_by','Users','users','id','Notes','notes','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d3973a31-d540-099a-96b4-59486efff4ca','am_tasktemplates_created_by','Users','users','id','AM_TaskTemplates','am_tasktemplates','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d3e43ad1-2ed3-85b3-6ce1-59486e3c2e71','document_revisions','Documents','documents','id','DocumentRevisions','document_revisions','document_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d4eb2c07-2bbe-37ae-d297-59486e293cdf','fp_events_created_by','Users','users','id','FP_events','fp_events','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d596043c-8764-0d75-e19d-59486eb9c86f','lead_tasks','Leads','leads','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('d5fb4fea-37da-84e9-5bf7-59486e7a05d0','aos_products_quotes_modified_user','Users','users','id','AOS_Products_Quotes','aos_products_quotes','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d60c7a73-fffd-3cd8-bd33-59486e8cf73c','bugs_assigned_user','Users','users','id','Bugs','bugs','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d67545c5-b3e2-15dc-4685-59486ee4ca58','opportunity_meetings','Opportunities','opportunities','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('d6dfac07-40db-cd12-7421-59486ecea13f','projects_accounts','Project','project','id','Accounts','accounts','id','projects_accounts','project_id','account_id','many-to-many',NULL,NULL,0,0),('d7b58c44-dfe5-d79e-c07c-59486ec5c5b3','aow_workflow_created_by','Users','users','id','AOW_WorkFlow','aow_workflow','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d7bd1ff8-d696-b253-68bf-59486ebb070d','aop_case_updates_assigned_user','Users','users','id','AOP_Case_Updates','aop_case_updates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('d7e3590b-3d5d-b1fe-9d1d-59486ea867fa','jjwg_maps_jjwg_areas','jjwg_Maps','jjwg_maps','id','jjwg_Areas','jjwg_areas','id','jjwg_maps_jjwg_areas_c','jjwg_maps_5304wg_maps_ida','jjwg_maps_41f2g_areas_idb','many-to-many',NULL,NULL,0,0),('d95abd64-310a-05dc-3d25-59486e35c719','account_emails','Accounts','accounts','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('d99282c6-af0c-7197-5e58-59486ed1ba16','aow_conditions_created_by','Users','users','id','AOW_Conditions','aow_conditions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('da3f086e-58e2-6a31-8572-59486e7a5128','email_template_email_marketings','EmailTemplates','email_templates','id','EmailMarketing','email_marketing','template_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('da5e5ba5-dc48-6d8f-9435-59486e95f093','contacts_email_addresses_primary','Contacts','contacts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('da622d10-f85b-7754-21d2-59486e3016ec','aos_invoices_created_by','Users','users','id','AOS_Invoices','aos_invoices','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('dabe53d1-5138-3aa9-36cd-59486ed7b0b1','emails_bugs_rel','Emails','emails','id','Bugs','bugs','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Bugs',0,0),('dca10b61-2c5b-1ce6-0ccb-59486e155759','opportunity_leads','Opportunities','opportunities','id','Leads','leads','opportunity_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('de13652e-5176-5286-a02b-59486e038137','meetings_modified_user','Users','users','id','Meetings','meetings','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('de34d3dc-6b35-6da3-4bbc-59486ef2c770','projects_tasks','Project','project','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('de66cfc2-2470-2360-bb03-59486eb1e37a','fp_events_assigned_user','Users','users','id','FP_events','fp_events','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('dea2e3a7-deb5-d54b-784c-59486ef2da44','aos_products_quotes_created_by','Users','users','id','AOS_Products_Quotes','aos_products_quotes','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e039fefd-f7d0-f1ef-33be-59486ef5cb98','securitygroups_modified_user','Users','users','id','SecurityGroups','securitygroups','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e077fb91-f6f5-2e45-7b6c-59486e7e897f','opportunity_tasks','Opportunities','opportunities','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('e0bac0eb-3098-b2fd-859f-59486ec55558','securitygroups_bugs','SecurityGroups','securitygroups','id','Bugs','bugs','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','Bugs',0,0),('e14ce583-e915-7a06-5ba7-59486e74fcf6','cases_aop_case_updates','Cases','cases','id','AOP_Case_Updates','aop_case_updates','case_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e19764ad-c47e-029e-ac8c-59486e3ded98','jjwg_maps_jjwg_markers','jjwg_Maps','jjwg_maps','id','jjwg_Markers','jjwg_markers','id','jjwg_maps_jjwg_markers_c','jjwg_maps_b229wg_maps_ida','jjwg_maps_2e31markers_idb','many-to-many',NULL,NULL,0,0),('e1c7cd2c-7840-6e1a-a3b6-59486e322485','hos_hospital_modified_user','Users','users','id','HOS_HOSPITAL','hos_hospital','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e2f55b0f-6431-f442-a762-59486e922a96','aos_invoices_assigned_user','Users','users','id','AOS_Invoices','aos_invoices','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e32680c1-6478-0364-73f8-59486e542cba','account_leads','Accounts','accounts','id','Leads','leads','account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e3abce9e-bce2-13b5-0535-59486e41fe69','contact_direct_reports','Contacts','contacts','id','Contacts','contacts','reports_to_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e3b991f0-c074-4448-b2e7-59486ea0c76c','projects_contacts','Project','project','id','Contacts','contacts','id','projects_contacts','project_id','contact_id','many-to-many',NULL,NULL,0,0),('e4f62151-8e20-827b-9e82-59486e9e02a0','aos_contracts_modified_user','Users','users','id','AOS_Contracts','aos_contracts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e5561bbd-b364-67dd-75a9-59486eda04d9','am_tasktemplates_assigned_user','Users','users','id','AM_TaskTemplates','am_tasktemplates','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e6848468-fa46-1101-970f-59486e39be17','meetings_created_by','Users','users','id','Meetings','meetings','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e6ba8654-4f3d-90db-47d4-59486efcfcf5','projects_meetings','Project','project','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('e71651f0-a48b-f7fb-1029-59486e211f74','emails_cases_rel','Emails','emails','id','Cases','cases','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Cases',0,0),('e7b9e619-485a-2f02-50a2-59486edf629f','revisions_created_by','Users','users','id','DocumentRevisions','document_revisions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e7fa23b9-0559-3459-143e-59486e6a78d0','aos_products_quotes_assigned_user','Users','users','id','AOS_Products_Quotes','aos_products_quotes','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('e85eddff-d4f8-68ad-40a0-59486ed6cdce','securitygroups_fp_events','SecurityGroups','securitygroups','id','FP_events','fp_events','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','FP_events',0,0),('e90b0f5b-bdab-390c-5328-59486e61354a','bug_tasks','Bugs','bugs','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('e98fa24a-f648-cdc2-30e5-59486ea0e1a7','securitygroups_created_by','Users','users','id','SecurityGroups','securitygroups','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('eab45ed3-a38e-b91a-d640-59486e080c5b','project_contacts_1','Project','project','id','Contacts','contacts','id','project_contacts_1_c','project_contacts_1project_ida','project_contacts_1contacts_idb','many-to-many',NULL,NULL,0,0),('eadd5e51-84a8-bcca-f07c-59486ebbd3f2','aop_case_updates_notes','AOP_Case_Updates','aop_case_updates','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','AOP_Case_Updates',0,0),('ec4cbc18-6f54-24a3-022b-59486e8d348c','cases_modified_user','Users','users','id','Cases','cases','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ec735673-31b7-1396-9423-59486e0f37e3','account_campaign_log','Accounts','accounts','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many','target_type','Accounts',0,0),('ed05d2c3-544a-3e61-01c8-59486eee8f36','campaign_campaigntrakers','Campaigns','campaigns','id','CampaignTrackers','campaign_trkrs','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ed5c3a00-c9a8-92ea-1415-59486e734b1d','contact_leads','Contacts','contacts','id','Leads','leads','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('ed6e0eb2-b53c-bb9d-8c98-59486ed20941','opportunity_notes','Opportunities','opportunities','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('ede8ff19-06dc-99b4-66fe-59486e188134','securitygroups_aos_invoices','SecurityGroups','securitygroups','id','AOS_Invoices','aos_invoices','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_Invoices',0,0),('eebd06cb-0624-5952-2278-59486e97eed9','meetings_notes','Meetings','meetings','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Meetings',0,0),('eff1c485-d0ef-2e4e-6d3a-59486ed4063f','hos_hospital_created_by','Users','users','id','HOS_HOSPITAL','hos_hospital','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('eff9f98f-a711-efbd-51cb-59486e9dd065','aos_contracts_created_by','Users','users','id','AOS_Contracts','aos_contracts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f058f686-b478-5b0b-5679-59486e6d7474','meetings_assigned_user','Users','users','id','Meetings','meetings','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f1c50237-a311-5ed5-4d99-59486e12eeee','aos_product_quotes_aos_products','AOS_Products','aos_products','id','AOS_Products_Quotes','aos_products_quotes','product_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f23381ee-b0e4-57b1-1906-59486ee854ae','emails_opportunities_rel','Emails','emails','id','Opportunities','opportunities','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Opportunities',0,0),('f2f50c28-1be7-20c0-11a4-59486e3def60','projects_calls','Project','project','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('f31af77b-ad36-f5fa-124c-59486e1e12d3','bug_meetings','Bugs','bugs','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('f3e1e992-6178-c6e9-fba0-59486e7997c4','aow_workflow_assigned_user','Users','users','id','AOW_WorkFlow','aow_workflow','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f40c8ba4-2e93-6fda-4f4e-59486ea25b26','favorites_modified_user','Users','users','id','Favorites','favorites','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('f4c702e6-e110-b41f-482a-59486eca2d3d','securitygroups_aos_contracts','SecurityGroups','securitygroups','id','AOS_Contracts','aos_contracts','id','securitygroups_records','securitygroup_id','record_id','many-to-many','module','AOS_Contracts',0,0);

/*Table structure for table `releases` */

DROP TABLE IF EXISTS `releases`;

CREATE TABLE `releases` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `list_order` int(4) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_releases` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `releases` */

/*Table structure for table `reminders` */

DROP TABLE IF EXISTS `reminders`;

CREATE TABLE `reminders` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `popup` tinyint(1) DEFAULT NULL,
  `email` tinyint(1) DEFAULT NULL,
  `email_sent` tinyint(1) DEFAULT NULL,
  `timer_popup` varchar(32) DEFAULT NULL,
  `timer_email` varchar(32) DEFAULT NULL,
  `related_event_module` varchar(32) DEFAULT NULL,
  `related_event_module_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_reminder_name` (`name`),
  KEY `idx_reminder_deleted` (`deleted`),
  KEY `idx_reminder_related_event_module` (`related_event_module`),
  KEY `idx_reminder_related_event_module_id` (`related_event_module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `reminders` */

/*Table structure for table `reminders_invitees` */

DROP TABLE IF EXISTS `reminders_invitees`;

CREATE TABLE `reminders_invitees` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `reminder_id` char(36) NOT NULL,
  `related_invitee_module` varchar(32) DEFAULT NULL,
  `related_invitee_module_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_reminder_invitee_name` (`name`),
  KEY `idx_reminder_invitee_assigned_user_id` (`assigned_user_id`),
  KEY `idx_reminder_invitee_reminder_id` (`reminder_id`),
  KEY `idx_reminder_invitee_related_invitee_module` (`related_invitee_module`),
  KEY `idx_reminder_invitee_related_invitee_module_id` (`related_invitee_module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `reminders_invitees` */

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `description` text,
  `modules` text,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_role_id_del` (`id`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `roles` */

/*Table structure for table `roles_modules` */

DROP TABLE IF EXISTS `roles_modules`;

CREATE TABLE `roles_modules` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `module_id` varchar(36) DEFAULT NULL,
  `allow` tinyint(1) DEFAULT '0',
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_module_id` (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `roles_modules` */

/*Table structure for table `roles_users` */

DROP TABLE IF EXISTS `roles_users`;

CREATE TABLE `roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_ru_role_id` (`role_id`),
  KEY `idx_ru_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `roles_users` */

/*Table structure for table `saved_search` */

DROP TABLE IF EXISTS `saved_search`;

CREATE TABLE `saved_search` (
  `id` char(36) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `search_module` varchar(150) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `contents` text,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `idx_desc` (`name`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `saved_search` */

/*Table structure for table `schedulers` */

DROP TABLE IF EXISTS `schedulers`;

CREATE TABLE `schedulers` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `date_time_start` datetime DEFAULT NULL,
  `date_time_end` datetime DEFAULT NULL,
  `job_interval` varchar(100) DEFAULT NULL,
  `time_from` time DEFAULT NULL,
  `time_to` time DEFAULT NULL,
  `last_run` datetime DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `catch_up` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_schedule` (`date_time_start`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `schedulers` */

insert  into `schedulers`(`id`,`deleted`,`date_entered`,`date_modified`,`created_by`,`modified_user_id`,`name`,`job`,`date_time_start`,`date_time_end`,`job_interval`,`time_from`,`time_to`,`last_run`,`status`,`catch_up`) values ('27fdece1-b5da-ab27-6e1f-5945a0cdd00d',0,'2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','Perform Lucene Index','function::aodIndexUnindexed','2015-01-01 19:30:01',NULL,'0::0::*::*::*',NULL,NULL,NULL,'Active',0),('40fb4aca-882c-5d96-7f0d-5945a040c5ed',0,'2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','Optimise AOD Index','function::aodOptimiseIndex','2015-01-01 17:45:01',NULL,'0::*/3::*::*::*',NULL,NULL,NULL,'Active',0),('456bf3a6-1dc0-7f34-6904-5945a07bdeb4',0,'2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','Process Workflow Tasks','function::processAOW_Workflow','2015-01-01 17:00:01',NULL,'*::*::*::*::*',NULL,NULL,NULL,'Active',1),('546cc818-a589-2e4b-de70-5945a0609797',0,'2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','Run Report Generation Scheduled Tasks','function::aorRunScheduledReports','2015-01-01 07:15:01',NULL,'*::*::*::*::*',NULL,NULL,NULL,'Active',1),('6110e190-c2d2-b092-7d28-5945a0081cc1',0,'2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','Run Email Reminder Notifications','function::sendEmailReminders','2015-01-01 06:30:01',NULL,'*::*::*::*::*',NULL,NULL,NULL,'Active',0),('689bac68-9da2-eca9-4df4-5945a0c05302',0,'2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','Prune Tracker Tables','function::trimTracker','2015-01-01 10:45:01',NULL,'0::2::1::*::*',NULL,NULL,NULL,'Active',1),('759cff89-cf80-444b-86da-5945a0837b4f',0,'2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','Clean Jobs Queue','function::cleanJobQueue','2015-01-01 15:30:01',NULL,'0::5::*::*::*',NULL,NULL,NULL,'Active',0),('7bc00456-c2e9-69cb-6c0e-5945a0c83f19',0,'2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','Prune Database on 1st of Month','function::pruneDatabase','2015-01-01 19:15:01',NULL,'0::4::1::*::*',NULL,NULL,NULL,'Inactive',0),('7dfafb6c-bec7-e5d8-3c06-5945a0990531',0,'2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','Check Inbound Mailboxes','function::pollMonitoredInboxesAOP','2015-01-01 06:00:01',NULL,'*::*::*::*::*',NULL,NULL,NULL,'Active',0),('a430140c-19e8-9650-80f7-5945a0c68175',0,'2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','Removal of documents from filesystem','function::removeDocumentsFromFS','2015-01-01 08:30:01',NULL,'0::3::1::*::*',NULL,NULL,NULL,'Active',0),('ae383db0-8291-341e-e488-5945a03a1eff',0,'2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','Run Nightly Process Bounced Campaign Emails','function::pollMonitoredInboxesForBouncedCampaignEmails','2015-01-01 08:30:01',NULL,'0::2-6::*::*::*',NULL,NULL,NULL,'Active',1),('b436a43e-5122-a1db-01c6-5945a08da53c',0,'2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','Prune SuiteCRM Feed Tables','function::trimSugarFeeds','2015-01-01 15:15:01',NULL,'0::2::1::*::*',NULL,NULL,NULL,'Active',1),('d6368e05-bba0-8151-9ec2-5945a0b01c2c',0,'2017-06-17 21:31:32','2017-06-17 21:31:32','1','1','Run Nightly Mass Email Campaigns','function::runMassEmailCampaign','2015-01-01 09:00:01',NULL,'0::2-6::*::*::*',NULL,NULL,NULL,'Active',1);

/*Table structure for table `securitygroups` */

DROP TABLE IF EXISTS `securitygroups`;

CREATE TABLE `securitygroups` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `noninheritable` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `securitygroups` */

/*Table structure for table `securitygroups_acl_roles` */

DROP TABLE IF EXISTS `securitygroups_acl_roles`;

CREATE TABLE `securitygroups_acl_roles` (
  `id` char(36) NOT NULL,
  `securitygroup_id` char(36) DEFAULT NULL,
  `role_id` char(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `securitygroups_acl_roles` */

/*Table structure for table `securitygroups_audit` */

DROP TABLE IF EXISTS `securitygroups_audit`;

CREATE TABLE `securitygroups_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_securitygroups_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `securitygroups_audit` */

/*Table structure for table `securitygroups_default` */

DROP TABLE IF EXISTS `securitygroups_default`;

CREATE TABLE `securitygroups_default` (
  `id` char(36) NOT NULL,
  `securitygroup_id` char(36) DEFAULT NULL,
  `module` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `securitygroups_default` */

/*Table structure for table `securitygroups_records` */

DROP TABLE IF EXISTS `securitygroups_records`;

CREATE TABLE `securitygroups_records` (
  `id` char(36) NOT NULL,
  `securitygroup_id` char(36) DEFAULT NULL,
  `record_id` char(36) DEFAULT NULL,
  `module` char(36) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_securitygroups_records_mod` (`module`,`deleted`,`record_id`,`securitygroup_id`),
  KEY `idx_securitygroups_records_del` (`deleted`,`record_id`,`module`,`securitygroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `securitygroups_records` */

/*Table structure for table `securitygroups_users` */

DROP TABLE IF EXISTS `securitygroups_users`;

CREATE TABLE `securitygroups_users` (
  `id` varchar(36) NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `securitygroup_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `primary_group` tinyint(1) DEFAULT NULL,
  `noninheritable` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `securitygroups_users_idxa` (`securitygroup_id`),
  KEY `securitygroups_users_idxb` (`user_id`),
  KEY `securitygroups_users_idxc` (`user_id`,`deleted`,`securitygroup_id`,`id`),
  KEY `securitygroups_users_idxd` (`user_id`,`deleted`,`securitygroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `securitygroups_users` */

/*Table structure for table `spots` */

DROP TABLE IF EXISTS `spots`;

CREATE TABLE `spots` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `config` longtext,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `spots` */

/*Table structure for table `sugarfeed` */

DROP TABLE IF EXISTS `sugarfeed`;

CREATE TABLE `sugarfeed` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `related_module` varchar(100) DEFAULT NULL,
  `related_id` char(36) DEFAULT NULL,
  `link_url` varchar(255) DEFAULT NULL,
  `link_type` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sgrfeed_date` (`date_entered`,`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sugarfeed` */

/*Table structure for table `tasks` */

DROP TABLE IF EXISTS `tasks`;

CREATE TABLE `tasks` (
  `id` char(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  `status` varchar(100) DEFAULT 'Not Started',
  `date_due_flag` tinyint(1) DEFAULT '0',
  `date_due` datetime DEFAULT NULL,
  `date_start_flag` tinyint(1) DEFAULT '0',
  `date_start` datetime DEFAULT NULL,
  `parent_type` varchar(255) DEFAULT NULL,
  `parent_id` char(36) DEFAULT NULL,
  `contact_id` char(36) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_tsk_name` (`name`),
  KEY `idx_task_con_del` (`contact_id`,`deleted`),
  KEY `idx_task_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_task_assigned` (`assigned_user_id`),
  KEY `idx_task_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tasks` */

/*Table structure for table `templatesectionline` */

DROP TABLE IF EXISTS `templatesectionline`;

CREATE TABLE `templatesectionline` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `thumbnail` varchar(255) DEFAULT NULL,
  `grp` varchar(255) DEFAULT NULL,
  `ord` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `templatesectionline` */

/*Table structure for table `tracker` */

DROP TABLE IF EXISTS `tracker`;

CREATE TABLE `tracker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monitor_id` char(36) NOT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `module_name` varchar(255) DEFAULT NULL,
  `item_id` varchar(36) DEFAULT NULL,
  `item_summary` varchar(255) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `session_id` varchar(36) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_tracker_iid` (`item_id`),
  KEY `idx_tracker_userid_vis_id` (`user_id`,`visible`,`id`),
  KEY `idx_tracker_userid_itemid_vis` (`user_id`,`item_id`,`visible`),
  KEY `idx_tracker_monitor_id` (`monitor_id`),
  KEY `idx_tracker_date_modified` (`date_modified`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `tracker` */

insert  into `tracker`(`id`,`monitor_id`,`user_id`,`module_name`,`item_id`,`item_summary`,`date_modified`,`action`,`session_id`,`visible`,`deleted`) values (2,'c5121b48-c06a-5321-1291-5945c669addb','1','Users','1','Administrator','2017-06-18 00:16:55','detailview','ruceeb1hqprcj7ev4648k4c680',1,0),(3,'2adb05aa-a2fd-03e8-754a-5948660124b0','1','PRO_PROCEDIMENTO','ae006f80-86e8-89a4-550d-594866f314ff','Procedimeto 01','2017-06-20 00:04:08','detailview','sm8e52s3d04ag0knkkq97l40b0',1,0),(4,'121f72e0-1e9c-e194-1be3-59486d3a55b3','1','PME_PROCEDIMENTO_MEDICO','47a49096-8403-136c-19a6-59486d94a1eb',NULL,'2017-06-20 00:34:47','detailview','sm8e52s3d04ag0knkkq97l40b0',1,0);

/*Table structure for table `upgrade_history` */

DROP TABLE IF EXISTS `upgrade_history`;

CREATE TABLE `upgrade_history` (
  `id` char(36) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `md5sum` varchar(32) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `version` varchar(64) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `id_name` varchar(255) DEFAULT NULL,
  `manifest` longtext,
  `date_entered` datetime DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `upgrade_history_md5_uk` (`md5sum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `upgrade_history` */

insert  into `upgrade_history`(`id`,`filename`,`md5sum`,`type`,`status`,`version`,`name`,`description`,`id_name`,`manifest`,`date_entered`,`enabled`) values ('4592faf2-cad7-d93f-eb36-5945c792523f','upload/upgrades/module/CONSULTORIOS2017_06_18_002228.zip','ecd7317edd8194afe83b9bbb42a242b0','module','installed','1497745347','CONSULTORIOS','','CONSULTORIOS','YTozOntzOjg6Im1hbmlmZXN0IjthOjEzOntpOjA7YToxOntzOjI1OiJhY2NlcHRhYmxlX3N1Z2FyX3ZlcnNpb25zIjthOjE6e2k6MDtzOjA6IiI7fX1pOjE7YToxOntzOjI0OiJhY2NlcHRhYmxlX3N1Z2FyX2ZsYXZvcnMiO2E6Mzp7aTowO3M6MjoiQ0UiO2k6MTtzOjM6IlBSTyI7aToyO3M6MzoiRU5UIjt9fXM6NjoicmVhZG1lIjtzOjA6IiI7czozOiJrZXkiO3M6MzoiQ09OIjtzOjY6ImF1dGhvciI7czowOiIiO3M6MTE6ImRlc2NyaXB0aW9uIjtzOjA6IiI7czo0OiJpY29uIjtzOjA6IiI7czoxNjoiaXNfdW5pbnN0YWxsYWJsZSI7YjoxO3M6NDoibmFtZSI7czoxMjoiQ09OU1VMVE9SSU9TIjtzOjE0OiJwdWJsaXNoZWRfZGF0ZSI7czoxOToiMjAxNy0wNi0xOCAwMDoyMjoyNyI7czo0OiJ0eXBlIjtzOjY6Im1vZHVsZSI7czo3OiJ2ZXJzaW9uIjtpOjE0OTc3NDUzNDc7czoxMzoicmVtb3ZlX3RhYmxlcyI7czo2OiJwcm9tcHQiO31zOjExOiJpbnN0YWxsZGVmcyI7YTo3OntzOjI6ImlkIjtzOjEyOiJDT05TVUxUT1JJT1MiO3M6NToiYmVhbnMiO2E6MTp7aTowO2E6NDp7czo2OiJtb2R1bGUiO3M6MTY6IkNPTl9DT05TVUxUT1JJT1MiO3M6NToiY2xhc3MiO3M6MTY6IkNPTl9DT05TVUxUT1JJT1MiO3M6NDoicGF0aCI7czo0NToibW9kdWxlcy9DT05fQ09OU1VMVE9SSU9TL0NPTl9DT05TVUxUT1JJT1MucGhwIjtzOjM6InRhYiI7YjoxO319czoxMDoibGF5b3V0ZGVmcyI7YTowOnt9czoxMzoicmVsYXRpb25zaGlwcyI7YTowOnt9czo5OiJpbWFnZV9kaXIiO3M6MTY6IjxiYXNlcGF0aD4vaWNvbnMiO3M6NDoiY29weSI7YToxOntpOjA7YToyOntzOjQ6ImZyb20iO3M6NDg6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvQ09OX0NPTlNVTFRPUklPUyI7czoyOiJ0byI7czoyNDoibW9kdWxlcy9DT05fQ09OU1VMVE9SSU9TIjt9fXM6ODoibGFuZ3VhZ2UiO2E6Mjp7aTowO2E6Mzp7czo0OiJmcm9tIjtzOjU5OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9sYW5ndWFnZS9hcHBsaWNhdGlvbi9lbl91cy5sYW5nLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTE6ImFwcGxpY2F0aW9uIjtzOjg6Imxhbmd1YWdlIjtzOjU6ImVuX3VzIjt9aToxO2E6Mzp7czo0OiJmcm9tIjtzOjU5OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9sYW5ndWFnZS9hcHBsaWNhdGlvbi9wdF9CUi5sYW5nLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTE6ImFwcGxpY2F0aW9uIjtzOjg6Imxhbmd1YWdlIjtzOjU6InB0X0JSIjt9fX1zOjE2OiJ1cGdyYWRlX21hbmlmZXN0IjtzOjA6IiI7fQ==','2017-06-18 00:22:27',1),('5ba76b95-db2e-3223-fc1d-5945d22f2aa3','upload/upgrades/module/VISITA2017_06_18_010529.zip','555193a64abce9c85c3a55145af4156d','module','installed','1497747929','VISITA','','VISITA','YTozOntzOjg6Im1hbmlmZXN0IjthOjEzOntpOjA7YToxOntzOjI1OiJhY2NlcHRhYmxlX3N1Z2FyX3ZlcnNpb25zIjthOjE6e2k6MDtzOjA6IiI7fX1pOjE7YToxOntzOjI0OiJhY2NlcHRhYmxlX3N1Z2FyX2ZsYXZvcnMiO2E6Mzp7aTowO3M6MjoiQ0UiO2k6MTtzOjM6IlBSTyI7aToyO3M6MzoiRU5UIjt9fXM6NjoicmVhZG1lIjtzOjA6IiI7czozOiJrZXkiO3M6MzoiVklTIjtzOjY6ImF1dGhvciI7czowOiIiO3M6MTE6ImRlc2NyaXB0aW9uIjtzOjA6IiI7czo0OiJpY29uIjtzOjA6IiI7czoxNjoiaXNfdW5pbnN0YWxsYWJsZSI7YjoxO3M6NDoibmFtZSI7czo2OiJWSVNJVEEiO3M6MTQ6InB1Ymxpc2hlZF9kYXRlIjtzOjE5OiIyMDE3LTA2LTE4IDAxOjA1OjI5IjtzOjQ6InR5cGUiO3M6NjoibW9kdWxlIjtzOjc6InZlcnNpb24iO2k6MTQ5Nzc0NzkyOTtzOjEzOiJyZW1vdmVfdGFibGVzIjtzOjY6InByb21wdCI7fXM6MTE6Imluc3RhbGxkZWZzIjthOjc6e3M6MjoiaWQiO3M6NjoiVklTSVRBIjtzOjU6ImJlYW5zIjthOjE6e2k6MDthOjQ6e3M6NjoibW9kdWxlIjtzOjEwOiJWSVNfVklTSVRBIjtzOjU6ImNsYXNzIjtzOjEwOiJWSVNfVklTSVRBIjtzOjQ6InBhdGgiO3M6MzM6Im1vZHVsZXMvVklTX1ZJU0lUQS9WSVNfVklTSVRBLnBocCI7czozOiJ0YWIiO2I6MTt9fXM6MTA6ImxheW91dGRlZnMiO2E6MDp7fXM6MTM6InJlbGF0aW9uc2hpcHMiO2E6MDp7fXM6OToiaW1hZ2VfZGlyIjtzOjE2OiI8YmFzZXBhdGg+L2ljb25zIjtzOjQ6ImNvcHkiO2E6MTp7aTowO2E6Mjp7czo0OiJmcm9tIjtzOjQyOiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9tb2R1bGVzL1ZJU19WSVNJVEEiO3M6MjoidG8iO3M6MTg6Im1vZHVsZXMvVklTX1ZJU0lUQSI7fX1zOjg6Imxhbmd1YWdlIjthOjI6e2k6MDthOjM6e3M6NDoiZnJvbSI7czo1OToiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbGFuZ3VhZ2UvYXBwbGljYXRpb24vZW5fdXMubGFuZy5waHAiO3M6OToidG9fbW9kdWxlIjtzOjExOiJhcHBsaWNhdGlvbiI7czo4OiJsYW5ndWFnZSI7czo1OiJlbl91cyI7fWk6MTthOjM6e3M6NDoiZnJvbSI7czo1OToiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbGFuZ3VhZ2UvYXBwbGljYXRpb24vcHRfQlIubGFuZy5waHAiO3M6OToidG9fbW9kdWxlIjtzOjExOiJhcHBsaWNhdGlvbiI7czo4OiJsYW5ndWFnZSI7czo1OiJwdF9CUiI7fX19czoxNjoidXBncmFkZV9tYW5pZmVzdCI7czowOiIiO30=','2017-06-18 01:05:29',1),('62d781b7-4656-c033-9409-59486a9089a5','upload/upgrades/module/PROCEDIMENTO_MEDICO2017_06_20_001959.zip','1d38dd06fe995370bf1189688791601d','module','installed','1497917999','PROCEDIMENTO_MEDICO','','PROCEDIMENTO_MEDICO','YTozOntzOjg6Im1hbmlmZXN0IjthOjEzOntpOjA7YToxOntzOjI1OiJhY2NlcHRhYmxlX3N1Z2FyX3ZlcnNpb25zIjthOjE6e2k6MDtzOjA6IiI7fX1pOjE7YToxOntzOjI0OiJhY2NlcHRhYmxlX3N1Z2FyX2ZsYXZvcnMiO2E6Mzp7aTowO3M6MjoiQ0UiO2k6MTtzOjM6IlBSTyI7aToyO3M6MzoiRU5UIjt9fXM6NjoicmVhZG1lIjtzOjA6IiI7czozOiJrZXkiO3M6MzoiUE1FIjtzOjY6ImF1dGhvciI7czo2OiJhdml0aXMiO3M6MTE6ImRlc2NyaXB0aW9uIjtzOjA6IiI7czo0OiJpY29uIjtzOjA6IiI7czoxNjoiaXNfdW5pbnN0YWxsYWJsZSI7YjoxO3M6NDoibmFtZSI7czoxOToiUFJPQ0VESU1FTlRPX01FRElDTyI7czoxNDoicHVibGlzaGVkX2RhdGUiO3M6MTk6IjIwMTctMDYtMjAgMDA6MTk6NTciO3M6NDoidHlwZSI7czo2OiJtb2R1bGUiO3M6NzoidmVyc2lvbiI7aToxNDk3OTE3OTk5O3M6MTM6InJlbW92ZV90YWJsZXMiO3M6NjoicHJvbXB0Ijt9czoxMToiaW5zdGFsbGRlZnMiO2E6Nzp7czoyOiJpZCI7czoxOToiUFJPQ0VESU1FTlRPX01FRElDTyI7czo1OiJiZWFucyI7YToxOntpOjA7YTo0OntzOjY6Im1vZHVsZSI7czoyMzoiUE1FX1BST0NFRElNRU5UT19NRURJQ08iO3M6NToiY2xhc3MiO3M6MjM6IlBNRV9QUk9DRURJTUVOVE9fTUVESUNPIjtzOjQ6InBhdGgiO3M6NTk6Im1vZHVsZXMvUE1FX1BST0NFRElNRU5UT19NRURJQ08vUE1FX1BST0NFRElNRU5UT19NRURJQ08ucGhwIjtzOjM6InRhYiI7YjoxO319czoxMDoibGF5b3V0ZGVmcyI7YTowOnt9czoxMzoicmVsYXRpb25zaGlwcyI7YTowOnt9czo5OiJpbWFnZV9kaXIiO3M6MTY6IjxiYXNlcGF0aD4vaWNvbnMiO3M6NDoiY29weSI7YToxOntpOjA7YToyOntzOjQ6ImZyb20iO3M6NTU6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvUE1FX1BST0NFRElNRU5UT19NRURJQ08iO3M6MjoidG8iO3M6MzE6Im1vZHVsZXMvUE1FX1BST0NFRElNRU5UT19NRURJQ08iO319czo4OiJsYW5ndWFnZSI7YToyOntpOjA7YTozOntzOjQ6ImZyb20iO3M6NTk6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL2xhbmd1YWdlL2FwcGxpY2F0aW9uL2VuX3VzLmxhbmcucGhwIjtzOjk6InRvX21vZHVsZSI7czoxMToiYXBwbGljYXRpb24iO3M6ODoibGFuZ3VhZ2UiO3M6NToiZW5fdXMiO31pOjE7YTozOntzOjQ6ImZyb20iO3M6NTk6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL2xhbmd1YWdlL2FwcGxpY2F0aW9uL3B0X0JSLmxhbmcucGhwIjtzOjk6InRvX21vZHVsZSI7czoxMToiYXBwbGljYXRpb24iO3M6ODoibGFuZ3VhZ2UiO3M6NToicHRfQlIiO319fXM6MTY6InVwZ3JhZGVfbWFuaWZlc3QiO3M6MDoiIjt9','2017-06-20 00:19:57',1),('7e2add2a-6363-49bb-2dbb-5945f4be9d3c','upload/upgrades/module/PROCEDIMENTO2017_06_18_033213.zip','6fd7b9774d7984c1c362b31a999b130d','module','installed','1497756732','PROCEDIMENTO','','PROCEDIMENTO','YTozOntzOjg6Im1hbmlmZXN0IjthOjEzOntpOjA7YToxOntzOjI1OiJhY2NlcHRhYmxlX3N1Z2FyX3ZlcnNpb25zIjthOjE6e2k6MDtzOjA6IiI7fX1pOjE7YToxOntzOjI0OiJhY2NlcHRhYmxlX3N1Z2FyX2ZsYXZvcnMiO2E6Mzp7aTowO3M6MjoiQ0UiO2k6MTtzOjM6IlBSTyI7aToyO3M6MzoiRU5UIjt9fXM6NjoicmVhZG1lIjtzOjA6IiI7czozOiJrZXkiO3M6MzoiUFJPIjtzOjY6ImF1dGhvciI7czo2OiJhdml0aXMiO3M6MTE6ImRlc2NyaXB0aW9uIjtzOjA6IiI7czo0OiJpY29uIjtzOjA6IiI7czoxNjoiaXNfdW5pbnN0YWxsYWJsZSI7YjoxO3M6NDoibmFtZSI7czoxMjoiUFJPQ0VESU1FTlRPIjtzOjE0OiJwdWJsaXNoZWRfZGF0ZSI7czoxOToiMjAxNy0wNi0xOCAwMzozMjoxMSI7czo0OiJ0eXBlIjtzOjY6Im1vZHVsZSI7czo3OiJ2ZXJzaW9uIjtpOjE0OTc3NTY3MzI7czoxMzoicmVtb3ZlX3RhYmxlcyI7czo2OiJwcm9tcHQiO31zOjExOiJpbnN0YWxsZGVmcyI7YTo3OntzOjI6ImlkIjtzOjEyOiJQUk9DRURJTUVOVE8iO3M6NToiYmVhbnMiO2E6MTp7aTowO2E6NDp7czo2OiJtb2R1bGUiO3M6MTY6IlBST19QUk9DRURJTUVOVE8iO3M6NToiY2xhc3MiO3M6MTY6IlBST19QUk9DRURJTUVOVE8iO3M6NDoicGF0aCI7czo0NToibW9kdWxlcy9QUk9fUFJPQ0VESU1FTlRPL1BST19QUk9DRURJTUVOVE8ucGhwIjtzOjM6InRhYiI7YjoxO319czoxMDoibGF5b3V0ZGVmcyI7YTowOnt9czoxMzoicmVsYXRpb25zaGlwcyI7YTowOnt9czo5OiJpbWFnZV9kaXIiO3M6MTY6IjxiYXNlcGF0aD4vaWNvbnMiO3M6NDoiY29weSI7YToxOntpOjA7YToyOntzOjQ6ImZyb20iO3M6NDg6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvUFJPX1BST0NFRElNRU5UTyI7czoyOiJ0byI7czoyNDoibW9kdWxlcy9QUk9fUFJPQ0VESU1FTlRPIjt9fXM6ODoibGFuZ3VhZ2UiO2E6Mjp7aTowO2E6Mzp7czo0OiJmcm9tIjtzOjU5OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9sYW5ndWFnZS9hcHBsaWNhdGlvbi9lbl91cy5sYW5nLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTE6ImFwcGxpY2F0aW9uIjtzOjg6Imxhbmd1YWdlIjtzOjU6ImVuX3VzIjt9aToxO2E6Mzp7czo0OiJmcm9tIjtzOjU5OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9sYW5ndWFnZS9hcHBsaWNhdGlvbi9wdF9CUi5sYW5nLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTE6ImFwcGxpY2F0aW9uIjtzOjg6Imxhbmd1YWdlIjtzOjU6InB0X0JSIjt9fX1zOjE2OiJ1cGdyYWRlX21hbmlmZXN0IjtzOjA6IiI7fQ==','2017-06-18 03:32:11',1),('aab76a34-fb95-fb02-18ed-5945f6ae0ccf','upload/upgrades/module/HOSPITAL2017_06_18_033857.zip','0adb263a87192bf57841c0f2d03a52b1','module','installed','1497757136','HOSPITAL','','HOSPITAL','YTozOntzOjg6Im1hbmlmZXN0IjthOjEzOntpOjA7YToxOntzOjI1OiJhY2NlcHRhYmxlX3N1Z2FyX3ZlcnNpb25zIjthOjE6e2k6MDtzOjA6IiI7fX1pOjE7YToxOntzOjI0OiJhY2NlcHRhYmxlX3N1Z2FyX2ZsYXZvcnMiO2E6Mzp7aTowO3M6MjoiQ0UiO2k6MTtzOjM6IlBSTyI7aToyO3M6MzoiRU5UIjt9fXM6NjoicmVhZG1lIjtzOjA6IiI7czozOiJrZXkiO3M6MzoiSE9TIjtzOjY6ImF1dGhvciI7czo2OiJhdml0aXMiO3M6MTE6ImRlc2NyaXB0aW9uIjtzOjA6IiI7czo0OiJpY29uIjtzOjA6IiI7czoxNjoiaXNfdW5pbnN0YWxsYWJsZSI7YjoxO3M6NDoibmFtZSI7czo4OiJIT1NQSVRBTCI7czoxNDoicHVibGlzaGVkX2RhdGUiO3M6MTk6IjIwMTctMDYtMTggMDM6Mzg6NTYiO3M6NDoidHlwZSI7czo2OiJtb2R1bGUiO3M6NzoidmVyc2lvbiI7aToxNDk3NzU3MTM2O3M6MTM6InJlbW92ZV90YWJsZXMiO3M6NjoicHJvbXB0Ijt9czoxMToiaW5zdGFsbGRlZnMiO2E6Nzp7czoyOiJpZCI7czo4OiJIT1NQSVRBTCI7czo1OiJiZWFucyI7YToxOntpOjA7YTo0OntzOjY6Im1vZHVsZSI7czoxMjoiSE9TX0hPU1BJVEFMIjtzOjU6ImNsYXNzIjtzOjEyOiJIT1NfSE9TUElUQUwiO3M6NDoicGF0aCI7czozNzoibW9kdWxlcy9IT1NfSE9TUElUQUwvSE9TX0hPU1BJVEFMLnBocCI7czozOiJ0YWIiO2I6MTt9fXM6MTA6ImxheW91dGRlZnMiO2E6MDp7fXM6MTM6InJlbGF0aW9uc2hpcHMiO2E6MDp7fXM6OToiaW1hZ2VfZGlyIjtzOjE2OiI8YmFzZXBhdGg+L2ljb25zIjtzOjQ6ImNvcHkiO2E6MTp7aTowO2E6Mjp7czo0OiJmcm9tIjtzOjQ0OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9tb2R1bGVzL0hPU19IT1NQSVRBTCI7czoyOiJ0byI7czoyMDoibW9kdWxlcy9IT1NfSE9TUElUQUwiO319czo4OiJsYW5ndWFnZSI7YToyOntpOjA7YTozOntzOjQ6ImZyb20iO3M6NTk6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL2xhbmd1YWdlL2FwcGxpY2F0aW9uL2VuX3VzLmxhbmcucGhwIjtzOjk6InRvX21vZHVsZSI7czoxMToiYXBwbGljYXRpb24iO3M6ODoibGFuZ3VhZ2UiO3M6NToiZW5fdXMiO31pOjE7YTozOntzOjQ6ImZyb20iO3M6NTk6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL2xhbmd1YWdlL2FwcGxpY2F0aW9uL3B0X0JSLmxhbmcucGhwIjtzOjk6InRvX21vZHVsZSI7czoxMToiYXBwbGljYXRpb24iO3M6ODoibGFuZ3VhZ2UiO3M6NToicHRfQlIiO319fXM6MTY6InVwZ3JhZGVfbWFuaWZlc3QiO3M6MDoiIjt9','2017-06-18 03:38:56',1),('c1b00c88-69f2-79f7-4a0e-5945a2a384c5','upload/upgrades/langpack/pt-BR (2).zip','44f9784532041b9e6cda8350dee85f2f','langpack','installed','7.8.3.0','','','','YTozOntzOjg6Im1hbmlmZXN0IjthOjk6e3M6NDoibmFtZSI7czoxOToiUG9ydHVndWVzZSAoQnJhemlsKSI7czoxMToiZGVzY3JpcHRpb24iO3M6NTY6IlRyYWR1w6fDo286IHd3dy5jcm93ZGluLmNvbS9wcm9qZWN0L3N1aXRlY3JtdHJhbnNsYXRpb25zIjtzOjQ6InR5cGUiO3M6ODoibGFuZ3BhY2siO3M6MTY6ImlzX3VuaW5zdGFsbGFibGUiO3M6MzoiWWVzIjtzOjI1OiJhY2NlcHRhYmxlX3N1Z2FyX3ZlcnNpb25zIjthOjA6e31zOjI0OiJhY2NlcHRhYmxlX3N1Z2FyX2ZsYXZvcnMiO2E6MTp7aTowO3M6MjoiQ0UiO31zOjY6ImF1dGhvciI7czoxODoiU3VpdGVDUk0gQ29tbXVuaXR5IjtzOjc6InZlcnNpb24iO3M6NzoiNy44LjMuMCI7czoxNDoicHVibGlzaGVkX2RhdGUiO3M6MTA6IjIwMTctMDQtMTIiO31zOjExOiJpbnN0YWxsZGVmcyI7YTozOntzOjI6ImlkIjtzOjU6InB0X0JSIjtzOjk6ImltYWdlX2RpciI7czoxNzoiPGJhc2VwYXRoPi9pbWFnZXMiO3M6NDoiY29weSI7YTozOntpOjA7YToyOntzOjQ6ImZyb20iO3M6MTg6IjxiYXNlcGF0aD4vaW5jbHVkZSI7czoyOiJ0byI7czo3OiJpbmNsdWRlIjt9aToxO2E6Mjp7czo0OiJmcm9tIjtzOjE4OiI8YmFzZXBhdGg+L21vZHVsZXMiO3M6MjoidG8iO3M6NzoibW9kdWxlcyI7fWk6MjthOjI6e3M6NDoiZnJvbSI7czoxODoiPGJhc2VwYXRoPi9pbnN0YWxsIjtzOjI6InRvIjtzOjc6Imluc3RhbGwiO319fXM6MTY6InVwZ3JhZGVfbWFuaWZlc3QiO3M6MDoiIjt9','2017-06-17 21:41:35',1),('daf0a6be-142f-ea09-8380-5945c242d921','upload/upgrades/module/COMPETIDORES2017_06_17_235919.zip','afec0571aa22fe890ac47614fdbbff48','module','installed','1497743959','COMPETIDORES','','COMPETIDORES','YTozOntzOjg6Im1hbmlmZXN0IjthOjEzOntpOjA7YToxOntzOjI1OiJhY2NlcHRhYmxlX3N1Z2FyX3ZlcnNpb25zIjthOjE6e2k6MDtzOjA6IiI7fX1pOjE7YToxOntzOjI0OiJhY2NlcHRhYmxlX3N1Z2FyX2ZsYXZvcnMiO2E6Mzp7aTowO3M6MjoiQ0UiO2k6MTtzOjM6IlBSTyI7aToyO3M6MzoiRU5UIjt9fXM6NjoicmVhZG1lIjtzOjA6IiI7czozOiJrZXkiO3M6MzoiQ09NIjtzOjY6ImF1dGhvciI7czowOiIiO3M6MTE6ImRlc2NyaXB0aW9uIjtzOjA6IiI7czo0OiJpY29uIjtzOjA6IiI7czoxNjoiaXNfdW5pbnN0YWxsYWJsZSI7YjoxO3M6NDoibmFtZSI7czoxMjoiQ09NUEVUSURPUkVTIjtzOjE0OiJwdWJsaXNoZWRfZGF0ZSI7czoxOToiMjAxNy0wNi0xNyAyMzo1OToxOCI7czo0OiJ0eXBlIjtzOjY6Im1vZHVsZSI7czo3OiJ2ZXJzaW9uIjtpOjE0OTc3NDM5NTk7czoxMzoicmVtb3ZlX3RhYmxlcyI7czo2OiJwcm9tcHQiO31zOjExOiJpbnN0YWxsZGVmcyI7YTo3OntzOjI6ImlkIjtzOjEyOiJDT01QRVRJRE9SRVMiO3M6NToiYmVhbnMiO2E6MTp7aTowO2E6NDp7czo2OiJtb2R1bGUiO3M6MTY6IkNPTV9DT01QRVRJRE9SRVMiO3M6NToiY2xhc3MiO3M6MTY6IkNPTV9DT01QRVRJRE9SRVMiO3M6NDoicGF0aCI7czo0NToibW9kdWxlcy9DT01fQ09NUEVUSURPUkVTL0NPTV9DT01QRVRJRE9SRVMucGhwIjtzOjM6InRhYiI7YjoxO319czoxMDoibGF5b3V0ZGVmcyI7YTowOnt9czoxMzoicmVsYXRpb25zaGlwcyI7YTowOnt9czo5OiJpbWFnZV9kaXIiO3M6MTY6IjxiYXNlcGF0aD4vaWNvbnMiO3M6NDoiY29weSI7YToxOntpOjA7YToyOntzOjQ6ImZyb20iO3M6NDg6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvQ09NX0NPTVBFVElET1JFUyI7czoyOiJ0byI7czoyNDoibW9kdWxlcy9DT01fQ09NUEVUSURPUkVTIjt9fXM6ODoibGFuZ3VhZ2UiO2E6Mjp7aTowO2E6Mzp7czo0OiJmcm9tIjtzOjU5OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9sYW5ndWFnZS9hcHBsaWNhdGlvbi9lbl91cy5sYW5nLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTE6ImFwcGxpY2F0aW9uIjtzOjg6Imxhbmd1YWdlIjtzOjU6ImVuX3VzIjt9aToxO2E6Mzp7czo0OiJmcm9tIjtzOjU5OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9sYW5ndWFnZS9hcHBsaWNhdGlvbi9wdF9CUi5sYW5nLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTE6ImFwcGxpY2F0aW9uIjtzOjg6Imxhbmd1YWdlIjtzOjU6InB0X0JSIjt9fX1zOjE2OiJ1cGdyYWRlX21hbmlmZXN0IjtzOjA6IiI7fQ==','2017-06-17 23:59:18',1);

/*Table structure for table `user_preferences` */

DROP TABLE IF EXISTS `user_preferences`;

CREATE TABLE `user_preferences` (
  `id` char(36) NOT NULL,
  `category` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `contents` longtext,
  PRIMARY KEY (`id`),
  KEY `idx_userprefnamecat` (`assigned_user_id`,`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_preferences` */

insert  into `user_preferences`(`id`,`category`,`deleted`,`date_entered`,`date_modified`,`assigned_user_id`,`contents`) values ('1fcf8a33-f2c5-5108-9af5-5945a0da065a','global',0,'2017-06-17 21:31:32','2017-06-20 00:22:26','1','YTo0MDp7czoyMDoiY2FsZW5kYXJfcHVibGlzaF9rZXkiO3M6MzY6IjFmOTNmZjQ1LTNmYTctNDllOS03YzJkLTU5NDVhMDc0MzBmMyI7czo4OiJ0aW1lem9uZSI7czozOiJVVEMiO3M6MTI6Im1haWxtZXJnZV9vbiI7czoyOiJvbiI7czoxNjoic3dhcF9sYXN0X3ZpZXdlZCI7czowOiIiO3M6MTQ6InN3YXBfc2hvcnRjdXRzIjtzOjA6IiI7czoxOToibmF2aWdhdGlvbl9wYXJhZGlnbSI7czoyOiJnbSI7czoxMzoic3VicGFuZWxfdGFicyI7czowOiIiO3M6MTA6InVzZXJfdGhlbWUiO3M6NjoiU3VpdGVQIjtzOjE0OiJtb2R1bGVfZmF2aWNvbiI7czowOiIiO3M6OToiaGlkZV90YWJzIjthOjA6e31zOjExOiJyZW1vdmVfdGFicyI7YTowOnt9czo3OiJub19vcHBzIjtzOjM6Im9mZiI7czoxMzoicmVtaW5kZXJfdGltZSI7aToxODAwO3M6MTk6ImVtYWlsX3JlbWluZGVyX3RpbWUiO2k6MzYwMDtzOjE2OiJyZW1pbmRlcl9jaGVja2VkIjtzOjE6IjEiO3M6MjI6ImVtYWlsX3JlbWluZGVyX2NoZWNrZWQiO047czoyOiJ1dCI7czoxOiIxIjtzOjU6ImRhdGVmIjtzOjU6Im0vZC9ZIjtzOjE1OiJtYWlsX3NtdHBzZXJ2ZXIiO3M6MDoiIjtzOjEzOiJtYWlsX3NtdHBwb3J0IjtzOjI6IjI1IjtzOjEzOiJtYWlsX3NtdHB1c2VyIjtzOjA6IiI7czoxMzoibWFpbF9zbXRwcGFzcyI7czowOiIiO3M6MjY6ImRlZmF1bHRfbG9jYWxlX25hbWVfZm9ybWF0IjtzOjU6InMgZiBsIjtzOjE2OiJleHBvcnRfZGVsaW1pdGVyIjtzOjE6IiwiO3M6MjI6ImRlZmF1bHRfZXhwb3J0X2NoYXJzZXQiO3M6NToiVVRGLTgiO3M6MTQ6InVzZV9yZWFsX25hbWVzIjtzOjI6Im9uIjtzOjE3OiJtYWlsX3NtdHBhdXRoX3JlcSI7czoxOiIxIjtzOjEyOiJtYWlsX3NtdHBzc2wiO2k6MTtzOjE3OiJlbWFpbF9zaG93X2NvdW50cyI7aTowO3M6MjE6ImRlZmF1bHRfZW1haWxfY2hhcnNldCI7czo1OiJVVEYtOCI7czoxOToidGhlbWVfY3VycmVudF9ncm91cCI7czo1OiJUb2RvcyI7czo5OiJBY2NvdW50c1EiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxMjoiYmFzaWNfc2VhcmNoIjt9czo2OiJMZWFkc1EiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxMjoiYmFzaWNfc2VhcmNoIjt9czo2OiJVc2Vyc1EiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxMjoiYmFzaWNfc2VhcmNoIjt9czoxNzoiQ09NX0NPTVBFVElET1JFU1EiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxMjoiYmFzaWNfc2VhcmNoIjt9czoxNzoiQ09OX0NPTlNVTFRPUklPU1EiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxMjoiYmFzaWNfc2VhcmNoIjt9czoxMToiVklTX1ZJU0lUQVEiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxMjoiYmFzaWNfc2VhcmNoIjt9czoxMDoiUHJvc3BlY3RzUSI7YToxOntzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjEyOiJiYXNpY19zZWFyY2giO31zOjE3OiJQUk9fUFJPQ0VESU1FTlRPUSI7YToxOntzOjEzOiJzZWFyY2hGb3JtVGFiIjtzOjE1OiJhZHZhbmNlZF9zZWFyY2giO31zOjI0OiJQTUVfUFJPQ0VESU1FTlRPX01FRElDT1EiO2E6MTp7czoxMzoic2VhcmNoRm9ybVRhYiI7czoxMjoiYmFzaWNfc2VhcmNoIjt9fQ=='),('23ce8bc1-d5e1-16e3-49ba-5945a1ada5a7','Home',0,'2017-06-17 21:37:28','2017-06-17 21:37:28','1','YToyOntzOjg6ImRhc2hsZXRzIjthOjY6e3M6MzY6IjE1ODRjMzFiLTJiNjUtMzg5Mi0xMmVjLTU5NDVhMWVlYmI2NyI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNjoiU3VnYXJGZWVkRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6OToiU3VnYXJGZWVkIjtzOjExOiJmb3JjZUNvbHVtbiI7aToxO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NDoibW9kdWxlcy9TdWdhckZlZWQvRGFzaGxldHMvU3VnYXJGZWVkRGFzaGxldC9TdWdhckZlZWREYXNobGV0LnBocCI7fXM6MzY6IjE2MzQ3Mzg0LTNlZDAtNmIxMS0xM2NiLTU5NDVhMTNiMjk4NCI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNDoiTXlDYWxsc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjU6IkNhbGxzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo1NjoibW9kdWxlcy9DYWxscy9EYXNobGV0cy9NeUNhbGxzRGFzaGxldC9NeUNhbGxzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjE2ZjgzYTgxLTBhMzgtZTMyNC0xZTEyLTU5NDVhMTI1NTgyOCI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoxNzoiTXlNZWV0aW5nc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjg6Ik1lZXRpbmdzIjtzOjExOiJmb3JjZUNvbHVtbiI7aTowO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NToibW9kdWxlcy9NZWV0aW5ncy9EYXNobGV0cy9NeU1lZXRpbmdzRGFzaGxldC9NeU1lZXRpbmdzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjE3ODZkOTZhLWVhMTAtZjAxZS1lOGU0LTU5NDVhMTIzN2UxOCI7YTo1OntzOjk6ImNsYXNzTmFtZSI7czoyMjoiTXlPcHBvcnR1bml0aWVzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6MTM6Ik9wcG9ydHVuaXRpZXMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjgwOiJtb2R1bGVzL09wcG9ydHVuaXRpZXMvRGFzaGxldHMvTXlPcHBvcnR1bml0aWVzRGFzaGxldC9NeU9wcG9ydHVuaXRpZXNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiMTg0NGRhYjMtZDM1MS1hNWMxLWU5MmItNTk0NWExMDg1N2QwIjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE3OiJNeUFjY291bnRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6ODoiQWNjb3VudHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjY1OiJtb2R1bGVzL0FjY291bnRzL0Rhc2hsZXRzL015QWNjb3VudHNEYXNobGV0L015QWNjb3VudHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiMThlODE4NmItODgwNS05MmY2LWIyM2MtNTk0NWExNWY3MmViIjthOjU6e3M6OToiY2xhc3NOYW1lIjtzOjE0OiJNeUxlYWRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6NToiTGVhZHMiO3M6MTE6ImZvcmNlQ29sdW1uIjtpOjA7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjU2OiJtb2R1bGVzL0xlYWRzL0Rhc2hsZXRzL015TGVhZHNEYXNobGV0L015TGVhZHNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319fXM6NToicGFnZXMiO2E6MTp7aTowO2E6Mzp7czo3OiJjb2x1bW5zIjthOjI6e2k6MDthOjI6e3M6NToid2lkdGgiO3M6MzoiNjAlIjtzOjg6ImRhc2hsZXRzIjthOjU6e2k6MDtzOjM2OiIxNjM0NzM4NC0zZWQwLTZiMTEtMTNjYi01OTQ1YTEzYjI5ODQiO2k6MTtzOjM2OiIxNmY4M2E4MS0wYTM4LWUzMjQtMWUxMi01OTQ1YTEyNTU4MjgiO2k6MjtzOjM2OiIxNzg2ZDk2YS1lYTEwLWYwMWUtZThlNC01OTQ1YTEyMzdlMTgiO2k6MztzOjM2OiIxODQ0ZGFiMy1kMzUxLWE1YzEtZTkyYi01OTQ1YTEwODU3ZDAiO2k6NDtzOjM2OiIxOGU4MTg2Yi04ODA1LTkyZjYtYjIzYy01OTQ1YTE1ZjcyZWIiO319aToxO2E6Mjp7czo1OiJ3aWR0aCI7czozOiI0MCUiO3M6ODoiZGFzaGxldHMiO2E6MTp7aTowO3M6MzY6IjE1ODRjMzFiLTJiNjUtMzg5Mi0xMmVjLTU5NDVhMWVlYmI2NyI7fX19czoxMDoibnVtQ29sdW1ucyI7czoxOiIzIjtzOjE0OiJwYWdlVGl0bGVMYWJlbCI7czoyMDoiTEJMX0hPTUVfUEFHRV8xX05BTUUiO319fQ=='),('245b41ed-ecca-f7de-72ea-5948596aa23e','Prospects2_PROSPECT',0,'2017-06-19 23:07:16','2017-06-19 23:07:16','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('325991c7-2cb8-1499-a569-5945a15d3fe4','Home2_CALL',0,'2017-06-17 21:37:28','2017-06-17 21:37:28','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('3e222c9b-8f5c-4b92-472a-5945a138bea4','Home2_MEETING',0,'2017-06-17 21:37:28','2017-06-17 21:37:28','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('44084b14-8a1c-1799-1887-5945c6cdd2a5','COM_COMPETIDORES2_COM_COMPETIDORES',0,'2017-06-18 00:17:49','2017-06-18 00:17:49','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('49c34d61-1031-21a4-a90a-59486a9c9b82','PME_PROCEDIMENTO_MEDICO2_PME_PROCEDIMENTO_MEDICO',0,'2017-06-20 00:22:26','2017-06-20 00:22:26','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('4b42b306-e4f1-6bfc-defb-5945a68af48a','Leads2_LEAD',0,'2017-06-17 21:59:31','2017-06-17 21:59:31','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('50ef31b3-f43b-7d65-c35b-5945a184fd9e','Home2_OPPORTUNITY',0,'2017-06-17 21:37:28','2017-06-17 21:37:28','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('579a732e-37d3-a13c-a5fa-5945a18c68f2','Home2_LEAD_18e8186b-8805-92f6-b23c-5945a15f72eb',0,'2017-06-17 21:37:36','2017-06-17 21:37:36','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('59c6b1a8-c331-f5d3-5255-5945c899703e','CON_CONSULTORIOS2_CON_CONSULTORIOS',0,'2017-06-18 00:23:57','2017-06-18 00:23:57','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('63fabe38-5e91-4c4e-a671-5945a1c4e7e5','Home2_ACCOUNT',0,'2017-06-17 21:37:28','2017-06-17 21:37:28','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('6afe8fb2-3d10-9c71-d081-594865f9b24b','PRO_PROCEDIMENTO2_PRO_PROCEDIMENTO',0,'2017-06-20 00:01:47','2017-06-20 00:01:47','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('7722bd93-e181-29e4-cfd8-5945a15ef49e','Home2_LEAD',0,'2017-06-17 21:37:28','2017-06-17 21:37:28','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('77244ac5-52d1-a82b-0706-5945c1aca4fe','Users2_USER',0,'2017-06-17 23:55:21','2017-06-17 23:55:21','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('7c30bdd5-3417-1137-f93c-5945a008a4a8','ETag',0,'2017-06-17 21:31:32','2017-06-20 00:19:57','1','YToxOntzOjEyOiJtYWluTWVudUVUYWciO2k6Nzt9'),('83c53b1e-7adb-b290-ed82-5945d2310941','VIS_VISITA2_VIS_VISITA',0,'2017-06-18 01:07:38','2017-06-18 01:07:38','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('89da5c7a-c743-efaa-f322-5945a1aa420b','Home2_SUGARFEED',0,'2017-06-17 21:37:28','2017-06-17 21:37:28','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ=='),('d04679e0-c646-dda9-45d2-5945a6701346','Accounts2_ACCOUNT',0,'2017-06-17 21:58:57','2017-06-17 21:58:57','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMjoiZGF0ZV9lbnRlcmVkIjtzOjk6InNvcnRPcmRlciI7czo0OiJERVNDIjt9fQ==');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `user_name` varchar(60) DEFAULT NULL,
  `user_hash` varchar(255) DEFAULT NULL,
  `system_generated_password` tinyint(1) DEFAULT NULL,
  `pwd_last_changed` datetime DEFAULT NULL,
  `authenticate_id` varchar(100) DEFAULT NULL,
  `sugar_login` tinyint(1) DEFAULT '1',
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `external_auth_only` tinyint(1) DEFAULT '0',
  `receive_notifications` tinyint(1) DEFAULT '1',
  `description` text,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `department` varchar(50) DEFAULT NULL,
  `phone_home` varchar(50) DEFAULT NULL,
  `phone_mobile` varchar(50) DEFAULT NULL,
  `phone_work` varchar(50) DEFAULT NULL,
  `phone_other` varchar(50) DEFAULT NULL,
  `phone_fax` varchar(50) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `address_street` varchar(150) DEFAULT NULL,
  `address_city` varchar(100) DEFAULT NULL,
  `address_state` varchar(100) DEFAULT NULL,
  `address_country` varchar(100) DEFAULT NULL,
  `address_postalcode` varchar(20) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `portal_only` tinyint(1) DEFAULT '0',
  `show_on_employees` tinyint(1) DEFAULT '1',
  `employee_status` varchar(100) DEFAULT NULL,
  `messenger_id` varchar(100) DEFAULT NULL,
  `messenger_type` varchar(100) DEFAULT NULL,
  `reports_to_id` char(36) DEFAULT NULL,
  `is_group` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_name` (`user_name`,`is_group`,`status`,`last_name`,`first_name`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`user_name`,`user_hash`,`system_generated_password`,`pwd_last_changed`,`authenticate_id`,`sugar_login`,`first_name`,`last_name`,`is_admin`,`external_auth_only`,`receive_notifications`,`description`,`date_entered`,`date_modified`,`modified_user_id`,`created_by`,`title`,`photo`,`department`,`phone_home`,`phone_mobile`,`phone_work`,`phone_other`,`phone_fax`,`status`,`address_street`,`address_city`,`address_state`,`address_country`,`address_postalcode`,`deleted`,`portal_only`,`show_on_employees`,`employee_status`,`messenger_id`,`messenger_type`,`reports_to_id`,`is_group`) values ('1','admin','$1$Y24.BG..$x6Jo4II0rhyusegvSJNnf0',0,NULL,NULL,1,NULL,'Administrator',1,0,1,NULL,'2017-06-17 21:31:32','2017-06-17 21:31:32','1','','Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL,NULL,NULL,NULL,0,0,1,'Active',NULL,NULL,'',0);

/*Table structure for table `users_feeds` */

DROP TABLE IF EXISTS `users_feeds`;

CREATE TABLE `users_feeds` (
  `user_id` varchar(36) DEFAULT NULL,
  `feed_id` varchar(36) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  KEY `idx_ud_user_id` (`user_id`,`feed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `users_feeds` */

/*Table structure for table `users_last_import` */

DROP TABLE IF EXISTS `users_last_import`;

CREATE TABLE `users_last_import` (
  `id` char(36) NOT NULL,
  `assigned_user_id` char(36) DEFAULT NULL,
  `import_module` varchar(36) DEFAULT NULL,
  `bean_type` varchar(36) DEFAULT NULL,
  `bean_id` char(36) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`assigned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `users_last_import` */

/*Table structure for table `users_password_link` */

DROP TABLE IF EXISTS `users_password_link`;

CREATE TABLE `users_password_link` (
  `id` char(36) NOT NULL,
  `username` varchar(36) DEFAULT NULL,
  `date_generated` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `users_password_link` */

/*Table structure for table `users_signatures` */

DROP TABLE IF EXISTS `users_signatures`;

CREATE TABLE `users_signatures` (
  `id` char(36) NOT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `signature` text,
  `signature_html` text,
  PRIMARY KEY (`id`),
  KEY `idx_usersig_uid` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `users_signatures` */

/*Table structure for table `vcals` */

DROP TABLE IF EXISTS `vcals`;

CREATE TABLE `vcals` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_id` char(36) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `source` varchar(100) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  KEY `idx_vcal` (`type`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `vcals` */

/*Table structure for table `vis_visita` */

DROP TABLE IF EXISTS `vis_visita`;

CREATE TABLE `vis_visita` (
  `id` char(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_entered` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `modified_user_id` char(36) DEFAULT NULL,
  `created_by` char(36) DEFAULT NULL,
  `description` text,
  `deleted` tinyint(1) DEFAULT '0',
  `assigned_user_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `vis_visita` */

/*Table structure for table `vis_visita_audit` */

DROP TABLE IF EXISTS `vis_visita_audit`;

CREATE TABLE `vis_visita_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` varchar(36) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `data_type` varchar(100) DEFAULT NULL,
  `before_value_string` varchar(255) DEFAULT NULL,
  `after_value_string` varchar(255) DEFAULT NULL,
  `before_value_text` text,
  `after_value_text` text,
  PRIMARY KEY (`id`),
  KEY `idx_vis_visita_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `vis_visita_audit` */

/*Table structure for table `vis_visita_cstm` */

DROP TABLE IF EXISTS `vis_visita_cstm`;

CREATE TABLE `vis_visita_cstm` (
  `id_c` char(36) NOT NULL,
  `data_hora_visita_c` datetime DEFAULT NULL,
  `data_real_visita_c` datetime DEFAULT NULL,
  `con_consultorios_id_c` char(36) DEFAULT NULL,
  `account_id_c` char(36) DEFAULT NULL,
  `status_aprovacao_c` varchar(100) DEFAULT NULL,
  `status_realizacao_c` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `vis_visita_cstm` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
